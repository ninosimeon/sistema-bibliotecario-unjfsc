<?php

class Acceso {

    function chrome() {
        $ins = & get_instance();
        if (!$ins->agent->is_browser('Chrome')) {
            show_error('Para acceder a esta area necesitas GOOGLE CHROME');
        }
    }

    function menu(){
        $ci = & get_instance();
        $ci->load->library('session');
        $ci->load->helper('url');
        $persona = $ci->session->userdata('logeado');
        $imprime = '';
        $tipos = 'LECTOR'.'INVITADO'.'AUXILIAR'.'BIBLIOTECARIO'.'DIRECTOR'.'ADMIN';
        switch ($persona['perfil_usuario']) {
            case 'ADMIN':
                $imprime .= heading('OTROS MÓDULOS', 4, "class='ui-widget-header ui-corner-top'");
                $imprime .= '<div class="ui-widget-content ui-corner-bottom">';
                $imprime .= anchor('visualizarcatalogo',"<button class='seleccion_another'>LECTOR</button><br>");
                $imprime .= anchor('usuarios',"<button class='seleccion_another'>AUXILIAR</button><br>");
                $imprime .= anchor('bibliografico',"<button class='seleccion_another'>BIBLIOTECARIO</button><br>");
                $imprime .= anchor('reporte',"<button class='seleccion_another'>DIRECTOR</button>");                
                $imprime .= '</div>';
                break;                        
        }
        return $imprime;
    }

    function controlar() {
        $ci = & get_instance();
        $ci->load->library('session');
        $perfil = $ci->session->userdata('logeado');
        if (!$perfil) {
            redirect('validarusuario', 'refresh');
        } else {
            switch ($perfil['perfil_usuario']) {
                case 'LECTOR':
                    if (!preg_match('/' . $ci->uri->segment(1) . '/', 'visualizarcatalogoo')) {
                        show_error('No estas autorizado para estar aqui');
                    }
                    break;
                case 'INVITADO':
                    if (!preg_match('/' . $ci->uri->segment(1) . '/', 'visualizarcatalogoo')) {
                        show_error('No estas autorizado para estar aqui');
                    }
                    break;
                /* case 'AUXILIAR':
                  if ($ci->uri->segment(1) != 'prestamo_reserva') {
                  show_error('No estas autorizado para estar aqui');
                  };
                  break; */
                case 'BIBLIOTECARIO':
                    if ($ci->uri->segment(1) != 'bibliografico') {
                        show_error('No estas autorizado para estar aqui');
                    };
                    break;
                default :
                    break;
            }
        }
    }

}

?>
