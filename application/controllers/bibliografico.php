<?php

class Bibliografico extends CI_Controller {

    var $sesion;

    function __construct() {
        parent::__construct();
        $this->load->model('Mbibliografico');
        $this->sesion = $this->session->userdata('logeado');
        $this->acceso->controlar();
        $this->load->model('Mevento');
        $this->acceso->chrome();
    }

    function index() {
        $imprime['menu'] = $this->acceso->menu();
        $this->load->view('bibliotecario/material_bibliografico/material_bibliografico', $imprime);
    }

    function extrae_inciales($cadena) {
        $cadena_une = explode(' ', $cadena);
        $cadena_final = '';
        foreach ($cadena_une as $value) {
            $cadena_final.= $value[0];
        }
        return $cadena_final;
    }

    function probemos() {
        $recupera_sesion = $this->sesion;
        var_dump($recupera_sesion);
        die;
    }

    function subir_ebook() {
        $config = array();
        $recupera_sesion = $this->sesion;
        //if ($this->input->post()) {
        $isbn = 'EBOOK' . mt_rand(100000000, 999999999);
        $titulo = $this->input->post('titulo');
        $categoria = $this->input->post('categoria');
        $cod_tematica = $this->input->post('tematica');
        $autor = $this->input->post('autor');
        $publicacion = $this->input->post('fecha_publicacion');
        $edicion = $this->input->post('edicion');
        $volumen = $this->input->post('volumen');
        $cod_terminal = $recupera_sesion['cod_terminal'];
        $resolucion = $this->input->post('resolucion');
        $autor_fragmentado = $this->extrae_inciales($autor);
        $titulo_fragmentado = $this->extrae_inciales($titulo);
        $publicacion_fragmentado = substr($publicacion, 0, 4);
        if (strlen($volumen) > 0) {
            $signatura = substr($recupera_sesion['nom_terminal'], 0, 3) . ' ' . $cod_tematica . ' ' . $autor_fragmentado . ' ' . $titulo_fragmentado . ' ' . $publicacion_fragmentado . ' Vol.' . $volumen . ' Ed.' . $edicion;
        } else {
            $signatura = substr($recupera_sesion['nom_terminal'], 0, 3) . ' ' . $cod_tematica . ' ' . $autor_fragmentado . ' ' . $titulo_fragmentado . ' ' . $publicacion_fragmentado . ' Ed.' . $edicion;
        }
        //}
        $config['upload_path'] = './public/pdf/';
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = '9000';
        $config['file_name'] = $isbn;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('file_pdf')) {
            /* $error = array('error' => $this->upload->display_errors());
              $this->load->view('subida/upload_form', $error); */
            show_error($this->upload->display_errors());
        } else {            
            $url_pdf = "public/pdf/" . $config['file_name'] . ".pdf";           
            $this->Mbibliografico->ingresa_ebook($isbn, $titulo, $categoria, $cod_tematica, $autor, $publicacion, $edicion, $signatura, $cod_terminal, $resolucion, $volumen);
            /*$this->db->where('ISBN-ISSN-codigo', $isbn);
            $this->db->update('material_bibliografico', array('contenido' => $url_pdf));*/
            $this->output->set_content_type('pdf')->set_output(file_get_contents(base_url($url_pdf)));
        }
    }

    function ebook() {
        /* var_dump($this->session->all_userdata());
          die; */
        $imprime = array();
        if ($this->input->post('verifica_dni')) {
            $query = $this->db->get_where('persona', array('numero' => $this->input->post('verifica_dni')));
            if ($query->num_rows() == 1) {
                $pendiente = $this->Mevento->verificaSolicitudAnterior($this->input->post('verifica_dni'));
                if ($pendiente) {
                    $nombre = 'pendiente';
                } else {
                    foreach ($query->result() as $value) {
                        $nombre = $value->apellidos . ' ' . $value->nombres;
                    }
                }
            } else {
                $nombre = 'fail';
            }
            echo json_encode($nombre);
        } else {
            $imprime['categoria'] = $this->db->get('categoria');
            $imprime['tema'] = $this->db->get('tema');
            $imprime['menu'] = $this->acceso->menu();
            $this->load->view('bibliotecario/material_bibliografico/ebook_view', $imprime);
        }
    }

    function add_sin_isbn() {
        $jbusca = array();
        $data = array();
        if ($this->input->post('buscador_frases')) {
            $query = $this->Mbibliografico->buscador_alternativo($this->input->post('buscador_frases'));
            if ($query->num_rows() > 0) {
                $i = 0;
                foreach ($query->result() as $value) {
                    $jbusca[$i] = array('value' => $value->isbn, 'label' => $value->busca);
                    $i = $i + 1;
                }
                $jbusca[] = array('value' => 'YFT', 'label' => '¿NO ES EL MISMO?. Tendras que registrarlo :D');
            } else {
                $jbusca[] = array('value' => 'YFT', 'label' => 'NO ENCONTRADO!!. Tendras que registrarlo =)');
            }
            echo json_encode($jbusca);
        } else {
            $data['menu'] = $this->acceso->menu();
            $data['categoria'] = $this->db->get('categoria');
            $this->load->view('bibliotecario/material_bibliografico/agregar_material_sin_isbn', $data);
        }
    }

    function agregar_material() {
        $jBusca = array();
        $jTema = array();
        $data = array();
        if ($this->input->post('v_isbn')) {
            $query = $this->Mbibliografico->verifica_existencia($this->input->post('v_isbn'));
            if ($query) {
                echo 'ok';
            }
        } else if ($this->input->post('aleatorio')) {
            $query = $this->db->get_where('material_bibliografico', array('ISBN-ISSN-codigo' => $this->input->post('aleatorio')));
            if ($query->num_rows > 0) {
                $jrespuesta = 'fail';
            } else {
                $jrespuesta = 'ok';
            }
            echo json_encode($jrespuesta);
        } else if ($this->input->post('new_categoria')) {
            $query = $this->Mbibliografico->verifica_categoria($this->input->post('new_categoria'));
            if ($query) {
                echo 'ok';
            }
        } else if ($this->input->post('new_material')) {
            $this->Mbibliografico->ingresa_material($this->input->post('isbn'), $this->input->post('titulo'), $this->input->post('categoria'), $this->input->post('tematica'), $this->input->post('autores'), $this->input->post('editorial'), $this->input->post('fecha'), $this->input->post('edicion'), $this->input->post('volumen'));
        } else if ($this->input->post('busca_tema')) {
            $query = $this->db->get_where('tematica', array('codCategoria' => $this->input->post('busca_tema')));
            $i = 0;
            foreach ($query->result() as $value) {
                $jBusca[$i]['valor'] = $value->codTematica;
                $jBusca[$i]['texto'] = $value->descripcion;
                $i = $i + 1;
            }
            echo json_encode($jBusca);
        } else if ($this->input->post('recarga_tema')) {
            $query = $this->db->get('tema');
            $i = 0;
            foreach ($query->result() as $value) {
                $jTema[$i]['valor'] = $value->codCategoria;
                $jTema[$i]['texto'] = $value->descripcion;
                $i = $i + 1;
            }
            echo json_encode($jTema);
        } else {
            $data['categoria'] = $this->db->get('categoria');
            $data['menu'] = $this->acceso->menu();
            //$data['tema'] = $this->db->get('tema');
            $this->load->view('bibliotecario/material_bibliografico/agregar_material_html5', $data);
        }
    }

    function agregar_item() {
        $jMuestra = array();
        $data = array();
        if ($this->input->post('busca_isbn')) {
            $query = $this->Mbibliografico->verifica_existencia($this->input->post('busca_isbn'));
            if (!$query) {
                $consulta = $this->Mbibliografico->describe_material($this->input->post('busca_isbn'));
                foreach ($consulta->result() as $value) {
                    $jDescribe['titulo'] = $value->titulo;
                    $jDescribe['autor'] = $value->autores;
                    $jDescribe['edicion'] = $value->edicion;
                    echo json_encode($jDescribe);
                }
            }
        } else if ($this->input->post('isbn')) {
            /* INICIA DETERMINA SIGNATURA
             */
            $ubica = explode('-', $this->sesion['cod_terminal']);
            $ubicacion = substr($ubica[1], 0, 3);
            $query = $this->db->get_where('material_bibliografico', array('ISBN-ISSN-codigo' => $this->input->post('isbn')));
            $volumen = FALSE;
            foreach ($query->result() as $value) {
                $tematica = $value->codTematica;
                $aut = $value->autores;
                $tit = $value->titulo;
                if (strlen($value->volumen) > 0) {
                    $volumen = 'v.' . $value->volumen;
                }
                $date = $value->fecPublicacion;
            }
            $autor = strtoupper(substr($aut, 0, 3)); //Mayus
            $titulo = strtolower(substr($tit, 0, 3)); //minus
            $fecha = substr($date, 0, 4);
            //determina numero de copia
            $query_copia = $this->db->get_where('item_bibliografico', array('codTerminal' => $this->sesion['cod_terminal'], 'ISBN-ISSN-codigo' => $this->input->post('isbn')));
            $copia = $query_copia->num_rows() + 1;
            $ncopia = "c." . $copia; //numeros con numeros, letras con letras C:
            if ($volumen) {
                $signatura = $ubicacion . ' ' . $tematica . ' ' . $autor . ' ' . $titulo . ' ' . $fecha . ' ' . $volumen . ' ' . $ncopia;
            } else {
                $signatura = $ubicacion . ' ' . $tematica . ' ' . $autor . ' ' . $titulo . ' ' . $fecha . ' ' . $ncopia;
            }
            /* FIN DETERMINA SIGNATURA
             */
            $this->Mbibliografico->ingresa_item($this->input->post('isbn'), $this->input->post('soporte'), $this->input->post('prestamo'), $this->input->post('origen'), $this->input->post('observaciones'), $signatura, $this->sesion['cod_terminal'], $this->input->post('resolucion'));
            $query_fin = $this->db->get_where('item_bibliografico', array('signatura' => $signatura));
            foreach ($query_fin->result() as $value) {
                $jMuestra['signatura'] = $signatura;
                $jMuestra['fechaIngreso'] = $value->fechaIngreso;
                $jMuestra['horaIngreso'] = $value->horaIngreso;
            }
            echo json_encode($jMuestra);
        } else {
            $soporte = $this->db->get('soporte');
            $prestamo = $this->db->get('modalidad_prestamo');
            $origen = $this->db->get('origen');
            $data['soporte'] = $soporte;
            $data['prestamo'] = $prestamo;
            $data['origen'] = $origen;
            $data['menu'] = $this->acceso->menu();
            $this->load->view('bibliotecario/item_bibliografico/agregar_item', $data);
        }
    }

}

/* Fin del archivo bibliografico.php */
