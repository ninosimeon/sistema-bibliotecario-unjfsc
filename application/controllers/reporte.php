<?php

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 * @property CI_Table $table
 * @property CI_Session $session
 * @property CI_FTP $ftp
 * ....
 */
class Reporte extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Mreporte');
        $this->load->model('Mvalidarusuario');
        $this->load->library('cezpdf');
        $this->load->helper('pdf_helper');
        $this->acceso->controlar();
        //$this->acceso->chrome();
    }

    /*
     * INICIO MATERIAL_DETALLE
     */

    function jqgrid_evento_material() {
        $signatura = $this->input->post('id');
        $query = $this->Mreporte->relacion_solicitantes($signatura);
        $i = 0;
        foreach ($query->result() as $value) {
            $jQgrid->rows[$i]['cell'] = array((int) $value->DNI, $value->NOMBRES, date('m - d', strtotime($value->fecha)), $value->hora, $value->FACULTAD, (int) $value->numero);
            $i = $i + 1;
        }
        $this->output->set_content_type('json')->set_output(json_encode($jQgrid));
    }

    function jqgrid_material() {
        #Recuperamos que isbn buscar ;)
        $recupera_sesion = $this->session->userdata('jqgrid');
        //var_dump($recupera_sesion);die;        

        $limit = $this->input->post('rows');

        $page = $this->input->post('page', TRUE);
        $sidx = $this->input->post('sidx', TRUE);
        $sord = $this->input->post('sord', TRUE);
        if ($this->input->post('_search')) {
            $campo = $this->input->post('searchField');
            $operador = $this->input->post('searchOper');
            switch ($operador) {
                case 'eq':
                    $operador = '=';
                    break;
                case 'ne':
                    $operador = '<>';
                    break;
                case 'lt':
                    $operador = '<';
                    break;
                case 'le':
                    $operador = '<=';
                    break;
                case 'gt':
                    $operador = '>';
                    break;
                case 'ge':
                    $operador = '>=';
                    break;
            }
            $elemento = $this->input->post('searchString');
            $first = $campo . ' ' . $operador;
        }
        if (!$sidx) {
            $sidx = 1;
        }
        $canti = $this->Mreporte->relacion_signatura($recupera_sesion['isbn'], $recupera_sesion['terminal'], $sidx, $sord, NULL, NULL, $first, $elemento);
        $count = $canti->num_rows();
        if ($count > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages) {
            $page = $total_pages;
        }
        if ($this->input->post('page')) {
            $start = $limit * $page - $limit;
        }
        $query = $this->Mreporte->relacion_signatura($recupera_sesion['isbn'], $recupera_sesion['terminal'], $sidx, $sord, $start, $limit, $first, $elemento);
        $jQgrid->page = $page;
        $jQgrid->total = $total_pages;
        $jQgrid->records = $count;
        $i = 0;
        //var_dump($query->result());die;
        foreach ($query->result() as $value) {
            $jQgrid->rows[$i]['id'] = $value->signatura;
            $jQgrid->rows[$i]['cell'] = array($value->signatura, $value->fecha_ingreso, $value->hora_ingreso);
            $i = $i + 1;
        }
        $this->output->set_content_type('json')->set_output(json_encode($jQgrid));
    }

    function material_detalle() {
        $imprime = array();
        if ($this->input->post('relacion_first')) {
            $relacion = $this->Mreporte->muestra_terminales($this->input->post('isbn'));
            foreach ($relacion->result() as $value) {
                $jEnvia[] = array('nom_terminal' => $value->terminal, 'cod_terminal' => $value->cod_terminal, 'cantidad' => $value->cantidad);
            }
            $this->output->set_content_type('json')->set_output(json_encode($jEnvia));
        } else if ($this->input->post('relacion_item')) {
            $query = $this->db->get_where('view_item', array('isbn' => $this->input->post('isbn'), 'cod_terminal' => $this->input->post('cod_terminal')));
            $this->session->set_userdata('jqgrid', array('isbn' => $this->input->post('isbn'), 'terminal' => $this->input->post('cod_terminal')));
            foreach ($query->result() as $value) {
                $jSabe[] = array('signatura' => $value->signatura, 'autor' => $value->autores, 'publicacion' => substr($value->fecPublicacion, 0, 4), 'titulo' => $value->titulo, 'edicion' => $value->edicion, 'volumen' => $value->volumen, 'soporte' => $value->soporte);
            }
            $this->output->set_content_type('json')->set_output(json_encode($jSabe));
        } else if ($this->input->post('carga_signatura')) {
            $consulta = $this->Mreporte->detalle_signatura($this->input->post('signatura_lanza'));
            foreach ($consulta->result() as $value) {
                $jSignatura = array('fecha' => $value->fecha, 'cantidad' => $value->cantidad);
            }
            $this->output->set_content_type('json')->set_output(json_encode($jSignatura));
        } else {
            $sesion = $this->session->userdata('logeado');
            $imprime ['persona'] = array('cargo' => $sesion ['perfil_usuario'], 'nombres' => $sesion ['apellidos_nombres'], 'terminal' => $sesion ['nom_terminal'], 'id_terminal' => $sesion ['cod_terminal']);
            $this->load->view('reportes/detallado/material', $imprime);
        }
    }

    /*
     * FIN MATERIAL_DETALLE
     */

    function recupera_sesion() {
        $sesion_actual = $this->session->userdata('logeado');
        $imprime_sesion = '<strong>' . $sesion_actual['perfil_usuario'] . '</strong>, ' . $sesion_actual['apellidos_nombres'];
        return $imprime_sesion;
    }

    function index() {
        $jItem = array();
        $data = array();
        if ($this->input->post('relacion_items')) {
            $query = $this->db->get('view_terminal_cantidad');
            foreach ($query->result() as $value) {
                $jItem[] = array($value->terminal, (int) $value->cantidad);
            }
            echo json_encode($jItem);
        } else {
            $data['menu'] = $this->acceso->menu();
            $data['sesion'] = $this->recupera_sesion();
            $this->load->view('reportes/inicio_v3', $data);
        }
    }

    function sancion() {
        $data = array();
        $data['menu'] = $this->acceso->menu();
        $data['sesion'] = $this->recupera_sesion();
        $this->load->view('reportes/sancion/inicio', $data);
    }

    function jqgrid_sancion_v2() {
        $limit = $this->input->post('rows');
        $page = $this->input->post('page', TRUE);
        $sidx = $this->input->post('sidx', TRUE);
        $sord = $this->input->post('sord', TRUE);
        if ($this->input->post('_search')) {
            $campo = $this->input->post('searchField');
            $operador = $this->input->post('searchOper');
            switch ($operador) {
                case 'eq':
                    $operador = '=';
                    break;
                case 'ne':
                    $operador = '<>';
                    break;
                case 'lt':
                    $operador = '<';
                    break;
                case 'le':
                    $operador = '<=';
                    break;
                case 'gt':
                    $operador = '>';
                    break;
                case 'ge':
                    $operador = '>=';
                    break;
            }
            $elemento = $this->input->post('searchString');
            $first = $campo . ' ' . $operador;
        }
        if (!$sidx) {
            $sidx = 1;
        }
        /* $query = $this->Mreporte->grafica_sancionados();
          $suma_cantidad = 0;
          foreach ($query->result() as $value) {
          $suma_cantidad = (int) $value->cantidad + $suma_cantidad;
          }
          $count = $suma_cantidad; */
        $canti = $this->Mreporte->detallado_sancionados_view($sidx, $sord, NULL, NULL, $first, $elemento);
        $count = $canti->num_rows();
        if ($count > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages) {
            $page = $total_pages;
        }
        if ($this->input->post('page')) {
            $start = $limit * $page - $limit;
        }

        $query = $this->Mreporte->detallado_sancionados_view($sidx, $sord, $start, $limit, $first, $elemento);

        $jQgrid->page = $page;
        $jQgrid->total = $total_pages;
        $jQgrid->records = $count;
        $i = 0;
        foreach ($query->result() as $value) {
            $jQgrid->rows[$i]['id'] = $value->nevento;
            $jQgrid->rows[$i]['cell'] = array((int) $value->dni, $value->nombres, $value->fechaInicio, $value->horaInicio, $value->fechaFin, $value->horaFin, $value->terminal, $value->descripcion);
            $i = $i + 1;
        }
        echo json_encode($jQgrid);
    }

    function jqgrid_sancion() {
        $limit = $this->input->post('rows');
        $page = $this->input->post('page', TRUE);
        $sidx = $this->input->post('sidx', TRUE);
        $sord = $this->input->post('sord', TRUE);
        if (!$sidx)
            $sidx = 1;
        $query = $this->Mreporte->grafica_sancionados();
        $suma_cantidad = 0;
        foreach ($query->result() as $value) {
            $suma_cantidad = (int) $value->cantidad + $suma_cantidad;
        }
        $count = $suma_cantidad;
        if ($count > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages) {
            $page = $total_pages;
        }
        if ($this->input->post('page')) {
            $start = $limit * $page - $limit;
        }
        $query = $this->Mreporte->detallado_sancionados($sidx, $sord, $start, $limit, $parametro = NULL);
        $jQgrid->page = $page;
        $jQgrid->total = $total_pages;
        $jQgrid->records = $count;
        $i = 0;
        foreach ($query->result() as $value) {
            $jQgrid->rows[$i]['id'] = $value->nevento;
            $jQgrid->rows[$i]['cell'] = array((int) $value->dni, $value->nombres, $value->fechaInicio, $value->horaInicio, $value->fechaFin, $value->horaFin, $value->terminal, $value->descripcion);
            $i = $i + 1;
        }
        echo json_encode($jQgrid);
    }

    function pdf_operaciones(){
        /* SESIÓN DE REPORTE */
        $sesion = $this->session->userdata('reporte');
        $fecha_1 = $sesion['fecha_1'];
        $fecha_2 = $sesion['fecha_2'];
        $terminal = $sesion['terminal'];
        $libro = $sesion['libro'];
        $persona = $sesion['persona'];
        /* SESIÓN DE PERSONA */
        $terricola = $this->session->userdata('logeado');
        $nombres = $terricola['apellidos_nombres'];
        $pdf = new Cezpdf('a4', 'portrait');
        $pdf->selectFont(APPPATH . 'libraries/fonts/Helvetica.afm');
        $pdf->ezText('<u><b>REPORTE OPERACIONES</b></u>', 13, array('justification' => 'center'));
        $pdf->ezText('', 9);
        $pdf->ezText('Peticionado por <b>' . $nombres . '</b>');   
        $pdf->ezText('');    
        # CONSULTA GENERAL
        $query = $this->Mreporte->detallado_operaciones_primero($fecha_1, $fecha_2, $terminal, $libro, $persona);
        /*var_dump($query->result());die;*/
        foreach ($query->result() as $value) {
            $humano[] = array('col_1' => "<b>LECTOR</b> <b>FECHA:</b> $value->fecha <b>HORA:</b> $value->hora
<b>$value->DNI</b> $value->NOMBRES");
            $libro =  $this->Mreporte->detallado_operaciones_externo($value->numero);
            /*var_dump($libro->result());die;*/
            foreach ($libro->result() as $dato) {
                $humano[] = array('col_1' => "<b>ISBN: </b> $dato->isbn <b>SIGNATURA:</b> $dato->signatura
<b>AUTOR: </b>$dato->autor
<b>TÍTULO: </b> $dato->titulo");            
            }            
            $prestamo = $this->Mreporte->operaciones_subgrid_prestamo($value->numero);
            foreach ($prestamo->result() as $key) {
                $humano[] = array('col_1' => "<b>PRESTAMO</b> <b>FECHA: </b>$key->fecha <b>HORA: </b>$key->hora
<b>$key->dni</b> $key->nombres");
            }
            $devolucion = $this->Mreporte->operaciones_subgrid_devolucion($value->numero);
            foreach ($devolucion->result() as $valor) {
                $humano[] = array('col_1' => "<b>DEVOLUCIÓN</b> <b>FECHA: </b>$valor->fecha <b>HORA: </b>$valor->hora
<b>$valor->dni</b> $valor->nombres");
            }
            $pdf->ezTable($humano, '', '', array('shaded' => 1, 'showHeadings' => 0, 'fontSize' => 9));
            $pdf->ezText('');
            unset($humano);
        }
        $pdf->ezStream(array('Content-Disposition' => 'reporte_operacion.pdf'));
    }

    /*function pdf_operaciones() {
        $recupera_sesion = $this->session->userdata('reporte');
        $sesion = $this->session->userdata('logeado');
        $pdf = new Cezpdf('a4', 'landscape');
        $pdf->selectFont(APPPATH . 'libraries/fonts/Helvetica.afm');
        //prep_pdf('landscape');
        $pdf->ezText('<u><b>REPORTE OPERACIONES</b></u>', 13, array('justification' => 'center'));
        $pdf->ezText('', 9);
        $pdf->ezText('Peticionado por <b>' . $sesion['apellidos_nombres'] . '</b>');
        $pdf->ezText('');
        $pdf->ezText('<b><u>RESUMEN</u></b>');
        $pdf->ezText('');
        $query = $this->Mreporte->listado_anio();
        foreach ($query->result() as $value) {
            $anio[] = $value->anio;
        }
        for ($index = 0; count($anio); $index++) {
            $db_resumen = array('col_1' => '<b>AÑO</b>', 'col_2' => $anio[$index]);
            $query_detalle_resumen = $this->Mreporte->grafico_operaciones($anio[$index]);
            foreach ($query_detalle_resumen->result() as $value) {
                $db_resumen = array('col_1' => $value->terminal, 'col_2' => $value->cantidad);
            }
        }
        $pdf->ezTable($db_resumen, '', '', array('shaded' => 1, 'showHeadings' => 0, 'fontSize' => 9));
        $pdf->ezText('');
        $this->db->group_by('terminal');
        $query_terminal = $this->db->get('view_publicacion_ordenado');
        foreach ($query_terminal->result() as $value) {
            $pdf->ezText("<b>TERMINAL $value->terminal</b>");
            $query_terminal_detallado = $this->Mreporte->pdf_detallado($value->codTerminal);
            $db_detallado[] = array('autores' => '<b>AUTOR</b>', 'titulo' => '<b>TÍTULO</b>', 'signatura' => '<b>SIGNATURA</b>', 'isbn' => '<b>ISBN</b>', 'anio' => '<b>AÑO</b>');
            $pdf->ezText('');
            foreach ($query_terminal_detallado->result() as $value) {
                $db_detallado[] = array('autores' => $value->autores, 'titulo' => $value->titulo, 'signatura' => $value->signatura, 'isbn' => $value->isbn, 'anio' => $value->anio);
            }
            $pdf->ezTable($db_detallado, '', '', array('shaded' => 1, 'showHeadings' => 0, 'fontSize' => 9, 'maxWidth' => 770));
            $pdf->ezText('');
            unset($db_detallado);
        }
        $pdf->ezStream(array('Content-Disposition' => 'items_publicacion.pdf'));
    }*/

    function operaciones() {
        if ($this->input->post('reporte')) {
            $guarda['fecha_1'] = $this->input->post('fecha_1');
            $guarda['fecha_2'] = $this->input->post('fecha_2');
            $guarda['terminal'] = $this->input->post('terminal');
            $guarda['libro'] = $this->input->post('libro');
            $guarda['persona'] = $this->input->post('persona');
            $this->session->set_userdata('reporte', $guarda);
        } else {
            $data['menu'] = $this->acceso->menu();
            $data['relacion_anio'] = $this->Mreporte->listado_anio();
            $data['sesion'] = $this->recupera_sesion();
            $this->load->view('reportes/operaciones/inicio', $data);
        }
    }

    /* function operaciones() {
      if ($this->input->post('anio')) {
      $sesion_guarda['anio'] = $this->input->post('anio');
      $this->session->set_userdata('reporte', $sesion_guarda);
      $query = $this->Mreporte->grafico_operaciones($this->input->post('anio'));
      foreach ($query->result() as $value) {
      $jGrafico->terminal[] = $value->terminal;
      $jGrafico->cantidad[] = (int) $value->cantidad;
      }
      echo json_encode($jGrafico);
      } else {
      $data['menu'] = $this->acceso->menu();
      $data['relacion_anio'] = $this->Mreporte->listado_anio();
      $data['sesion'] = $this->recupera_sesion();
      $this->load->view('reportes/operaciones/inicio', $data);
      }
      } */

    function subgrid_operaciones() {
        $id = $this->input->post('id');
        $query = $this->Mreporte->operaciones_subgrid_prestamo($id);
        $i = 0;
        foreach ($query->result() as $value) {
            $jQgrid->rows[$i]['cell'] = array((int) $value->dni, $value->nombres, date('m - d', strtotime($value->fecha)), $value->hora, 'PRESTAMO', $value->numero);
            $i = $i + 1;
        }
        $consulta = $this->Mreporte->operaciones_subgrid_devolucion($id);
        foreach ($consulta->result() as $value) {
            $jQgrid->rows[$i]['cell'] = array((int) $value->dni, $value->nombres, date('m - d', strtotime($value->fecha)), $value->hora, 'DEVOLUCIÓN', $value->numero);
            $i = $i + 1;
        }
        echo json_encode($jQgrid);
    }

    function jqgrid_operaciones_libro($evento) {
        $limit = $this->input->post('rows');
        $page = $this->input->post('page', TRUE);
        $sidx = $this->input->post('sidx', TRUE);
        $sord = $this->input->post('sord', TRUE);
        if (!$sidx)
            $sidx = 1;
        $count = 1;
        if ($count > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages) {
            $page = $total_pages;
        }
        if ($this->input->post('page')) {
            $start = $limit * $page - $limit;
        }
        #NO SE  AGREGA LOS DEMAS PARÁMETROS YA QUE EL RESULTADO SIEMPRE SERA 1 
        $query = $this->Mreporte->detallado_operaciones_externo($evento);
        $jQgrid->page = $page;
        $jQgrid->total = $total_pages;
        $jQgrid->records = $count;
        $i = 0;
        foreach ($query->result() as $value) {
            $jQgrid->rows[$i]['id'] = $value->signatura;
            $jQgrid->rows[$i]['cell'] = array($value->autor, $value->titulo, $value->prestamo, $value->terminal, $value->signatura, $value->isbn);
            $i = $i + 1;
        }
        echo json_encode($jQgrid);
    }

    #Aca inicia el jQgrid

    function jqgrid_operaciones() {
        /* $recupera_sesion = $this->session->userdata('reporte');
          $anio = $recupera_sesion['anio']; */
        /* INICIO RECUPERA SESION */
        $sesion = $this->session->userdata('reporte');
        $fecha_1 = $sesion['fecha_1'];
        $fecha_2 = $sesion['fecha_2'];
        $terminal = $sesion['terminal'];
        $libro = $sesion['libro'];
        $persona = $sesion['persona'];
        /*var_dump($sesion);
        die;*/
        /* FIN RECUPERA SESION */
        $limit = $this->input->post('rows');
        $page = $this->input->post('page', TRUE);
        $sidx = $this->input->post('sidx', TRUE);
        $sord = $this->input->post('sord', TRUE);
        if ($this->input->post('_search')) {
            $campo = $this->input->post('searchField');
            $operador = $this->input->post('searchOper');
            switch ($operador) {
                case 'eq':
                    $operador = '=';
                    break;
                case 'ne':
                    $operador = '<>';
                    break;
                case 'lt':
                    $operador = '<';
                    break;
                case 'le':
                    $operador = '<=';
                    break;
                case 'gt':
                    $operador = '>';
                    break;
                case 'ge':
                    $operador = '>=';
                    break;
            }
            $elemento = $this->input->post('searchString');
            $first = $campo . ' ' . $operador;
        }

        if (!$sidx) {
            $sidx = 1;
        }
        //$instancia_query , un artificio para determinal la cantidad segun año

        /* $instancia_query = $this->Mreporte->cantidad_operaciones($anio);
          foreach ($instancia_query->result() as $value) {
          $count = (int) $value->cantidad;
          } */

        $canti = $this->Mreporte->detallado_operaciones_primero($fecha_1, $fecha_2, $terminal, $libro, $persona, $sidx, $sord, NULL, NULL, $first, $elemento);
        if ($canti->num_rows()) {
            $count = $canti->num_rows();
            if ($count > 0) {
                $total_pages = ceil($count / $limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages) {
                $page = $total_pages;
            }
            if ($this->input->post('page')) {
                $start = $limit * $page - $limit;
            }
            //consulta a analizar        
            $query = $this->Mreporte->detallado_operaciones_primero($fecha_1, $fecha_2, $terminal, $libro, $persona, $sidx, $sord, $start, $limit, $first, $elemento);
            $jQgrid->page = $page;
            $jQgrid->total = $total_pages;
            $jQgrid->records = $count;
            $i = 0;
            foreach ($query->result() as $value) {
                $jQgrid->rows[$i]['id'] = (int) $value->numero;
                $jQgrid->rows[$i]['cell'] = array((int) $value->DNI, $value->NOMBRES, date('m - d', strtotime($value->fecha)), $value->hora, $value->FACULTAD, (int) $value->numero, $value->signatura);
                $i = $i + 1;
            }
        } else {
            $jQgrid->rows[0]['id'] = "";
            $jQgrid->rows[0]['cell'] = array('', '', '', '', '', '', '');
        }
        $this->output->set_content_type('json')->set_output(json_encode($jQgrid));
    }

    function pdf_publicacion() {
        $sesion = $this->session->userdata('logeado');
        $pdf = new Cezpdf('a4', 'landscape');
        $pdf->selectFont(APPPATH . 'libraries/fonts/Helvetica.afm');
        //prep_pdf('landscape');
        $pdf->ezText('<u><b>REPORTE ÍTEMS PUBLICACIÓN</b></u>', 13, array('justification' => 'center'));
        $pdf->ezText('', 9);
        $pdf->ezText('Peticionado por <b>' . $sesion['apellidos_nombres'] . '</b>');
        $pdf->ezText('');
        $pdf->ezText('<b><u>RESUMEN</u></b>');
        $pdf->ezText('');
        $pdf->ezColumnsStart(array('num' => 3));
        $query = $this->db->get('view_publicacion_ordenado');
        $db_resumen[] = array('terminal' => '<b>TERMINAL</b>', 'anio' => '<b>AÑO</b>', 'cantidad' => '<b>CANTIDAD</b>');
        foreach ($query->result() as $value) {
            $db_resumen[] = array('terminal' => $value->terminal, 'anio' => $value->anio, 'cantidad' => $value->cantidad);
        }
        $pdf->ezTable($db_resumen, '', '', array('shaded' => 1, 'showHeadings' => 0, 'fontSize' => 9));
        $pdf->ezText('');
        $pdf->ezColumnsStop();
        $this->db->group_by('terminal');
        $query_terminal = $this->db->get('view_publicacion_ordenado');

        foreach ($query_terminal->result() as $value) {
            $pdf->ezText("<b>TERMINAL $value->terminal</b>");
            $query_terminal_detallado = $this->Mreporte->pdf_detallado($value->codTerminal);
            $db_detallado[] = array('autores' => '<b>AUTOR</b>', 'titulo' => '<b>TÍTULO</b>', 'signatura' => '<b>SIGNATURA</b>', 'isbn' => '<b>ISBN</b>', 'anio' => '<b>AÑO</b>');
            $pdf->ezText('');
            foreach ($query_terminal_detallado->result() as $value) {
                $db_detallado[] = array('autores' => $value->autores, 'titulo' => $value->titulo, 'signatura' => $value->signatura, 'isbn' => $value->isbn, 'anio' => $value->anio);
            }
            $pdf->ezTable($db_detallado, '', '', array('cols' => array('autores' => array('width' => 220)), 'shaded' => 1, 'showHeadings' => 0, 'fontSize' => 9, 'maxWidth' => 770));
            $pdf->ezText('');
            unset($db_detallado);
        }

        $pdf->ezStream(array('Content-Disposition' => 'items_publicacion.pdf'));
    }

    function jqgrid_publicacion() {
        if ($this->input->post('jqgrid_terminal')) {
            $sesion_guarda['terminal'] = $this->input->post('jqgrid_terminal');
            $this->session->set_userdata('reporte', $sesion_guarda);
        } else {
            $recupera_sesion = $this->session->userdata('reporte');
            $terminal = $recupera_sesion['terminal'];
            $limit = $this->input->post('rows');
            $page = $this->input->post('page', TRUE);
            $sidx = $this->input->post('sidx', TRUE);
            $sord = $this->input->post('sord', TRUE);
            if (!$sidx)
                $sidx = 1;
            $cantidad = $this->Mreporte->items_publicacion_terminal($terminal);
            $sumate = 0;
            foreach ($cantidad->result() as $value) {
                $sumate = (int) $value->cantidad + $sumate;
            }
            $count = $sumate;
            if ($count > 0) {
                $total_pages = ceil($count / $limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages) {
                $page = $total_pages;
            }
            if ($this->input->post('page')) {
                $start = $limit * $page - $limit;
            }
            $query = $this->Mreporte->jqpublicacion_superdetallado($terminal, $sidx, $sord, $start, $limit);
            $jQgrid->page = $page;
            $jQgrid->total = $total_pages;
            $jQgrid->records = $count;
            $i = 0;
            foreach ($query->result() as $value) {
                $jQgrid->rows[$i]['id'] = $value->signatura;
                $jQgrid->rows[$i]['cell'] = array($value->autores, $value->titulo, $value->signatura, $value->isbn, (int) $value->anio);
                $i = $i + 1;
            }
            echo json_encode($jQgrid);
        }
    }

    function item_publicacion() {
        if ($this->input->post('grafico_terminal')) {
            $query_grafico = $this->Mreporte->items_publicacion_terminal($this->input->post('grafico_terminal'));
            foreach ($query_grafico->result() as $value) {
                $jGrafico['anio'][] = (int) $value->anio;
                $jGrafico['cantidad'][] = (int) $value->cantidad;
            }
            echo json_encode($jGrafico);
        } else {
            $data['menu'] = $this->acceso->menu();
            $data['sesion'] = $this->recupera_sesion();
            $data['relacion_terminal'] = $this->db->get('terminal');
            $this->load->view('reportes/items_publicacion/inicio_v2', $data);
        }
    }

    function jqgrid_ingreso() {
        if ($this->input->post('jqgrid_anio')) {
            $sesion_guarda['anio'] = $this->input->post('jqgrid_anio');
            $sesion_guarda['terminal'] = $this->input->post('jqgrid_terminal');
            //$sesion_guarda['reporte'] = array('anio' => $this->input->post('jqgrid_anio'), 'terminal' => $this->input->post('jqgrid_terminal'));
            $this->session->set_userdata('reporte', $sesion_guarda);
        } else {
            $recupera_sesion = $this->session->userdata('reporte');
            $parametro = $recupera_sesion['anio'];
            $terminal = $recupera_sesion['terminal'];
            //if ($this->input->post('rows')) {
            $limit = $this->input->post('rows');
            /* } else {
              $limit = 10;
              } */

            //if ($this->input->post('page')) {
            $page = $this->input->post('page', TRUE);
            /* } else {
              $page = 1;
              $start = 0;
              } */
            $sidx = $this->input->post('sidx', TRUE);
            $sord = $this->input->post('sord', TRUE);

            if (!$sidx)
                $sidx = 1;
            $cantidad = $this->Mreporte->ingreso_superdetallado($parametro, $terminal);
            $count = (int) $cantidad->num_rows();
            if ($count > 0) {
                $total_pages = ceil($count / $limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages) {
                $page = $total_pages;
            }
            if ($this->input->post('page')) {
                $start = $limit * $page - $limit;
            }

            $query = $this->Mreporte->jqingreso_superdetallado($parametro, $terminal, $sidx, $sord, $start, $limit);
            $jQgrid->page = $page;
            $jQgrid->total = $total_pages;
            $jQgrid->records = $count;
            $i = 0;
            foreach ($query->result() as $value) {
                $jQgrid->rows[$i]['id'] = $value->signatura;
                $jQgrid->rows[$i]['cell'] = array($value->autores, $value->titulo, $value->signatura, $value->isbn, date('m - d', strtotime($value->ingreso)));
                $i = $i + 1;
            }
            echo json_encode($jQgrid);
        }
    }

    function pdf_ingreso() {
        $sesion = $this->session->userdata('logeado');
        $pdf = new Cezpdf('a4', 'landscape');
        $pdf->selectFont(APPPATH . 'libraries/fonts/Helvetica.afm');
        //prep_pdf('landscape');
        $pdf->ezText('<u><b>REPORTE ÍTEMS INGRESO</b></u>', 13, array('justification' => 'center'));
        $pdf->ezText('', 9);
        $pdf->ezText('Peticionado por <b>' . $sesion['apellidos_nombres'] . '</b>');
        $pdf->ezText('');
        $pdf->ezText('<b><u>RESUMEN POR AÑO</u></b>');
        //$this->cezpdf->ezText('');
        $query = $this->db->get('view_anio_ingreso'); //consulta año / cantidad
        foreach ($query->result() as $value) {
            $db_dato[] = array('desc' => '<b><u>' . $value->anio . '</u></b>', 'cifras' => '<b><u>' . $value->cantidad . '</u></b>');
            $consulta = $this->Mreporte->items_ingreso_terminal((int) $value->anio);
            foreach ($consulta->result() as $valor) { //consulta nombre cantidad
                $db_dato[] = array('desc' => $valor->nomTerminal, 'cifras' => $valor->cantidad);
            }
        }
        $pdf->ezTable($db_dato, '', '', array('shaded' => 0, 'showHeadings' => 0));
        $pdf->ezText('');
        $query_anio_terminal = $this->Mreporte->semi_detalle();
        foreach ($query_anio_terminal->result() as $detalle) {
            $pdf->ezText("<b>AÑO $detalle->anio, TERMINAL '$detalle->nomTerminal'</b>");
            $pdf->ezText('');
            $consulta_superdetallado = $this->Mreporte->ingreso_superdetallado((int) $detalle->anio, $detalle->terminal);
            foreach ($consulta_superdetallado->result() as $superdetallado) {
                $db_detallado[] = array('<b>AUTOR</b>' => $superdetallado->autores, '<b>TITULO</b>' => $superdetallado->titulo, '<b>SIGNATURA</b>' => $superdetallado->signatura, '<b>ISBN</b>' => $superdetallado->isbn);
            }
            $pdf->ezTable($db_detallado, '', '', array('shaded' => 1, 'showHeadings' => 1, 'maxWidth' => 770, 'fontSize' => 9, 'showLines' => 2));
            unset($db_detallado);
            $pdf->ezText('');
        }
        $pdf->ezText('');
        $pdf->ezStream(array('Content-Disposition' => 'items_ingreso.pdf'));
    }

    function item_ingreso() {
        if ($this->input->post('anio_seleccionado')) {
            $query = $this->Mreporte->items_ingreso_terminal($this->input->post('anio_seleccionado'));
            foreach ($query->result() as $value) {
                $dato[] = array($value->nomTerminal, $value->codTerminal);
            }
            echo json_encode($dato);
        } else if ($this->input->post('grafico_terminal')) {
            $query = $this->Mreporte->fecha_ingreso($this->input->post('grafico_terminal'), $this->input->post('grafico_anio'));
            foreach ($query->result() as $value) {
                switch ($value->mes) {
                    case 1:
                        $value->mes = 'Enero';
                        break;
                    case 2:
                        $value->mes = 'Febrero';
                        break;
                    case 3:
                        $value->mes = 'Marzo';
                        break;
                    case 4:
                        $value->mes = 'Abril';
                        break;
                    case 5:
                        $value->mes = 'Mayo';
                        break;
                    case 6:
                        $value->mes = 'Junio';
                        break;
                    case 7:
                        $value->mes = 'Julio';
                        break;
                    case 8:
                        $value->mes = 'Agosto';
                        break;
                    case 9:
                        $value->mes = 'Setiembre';
                        break;
                    case 10:
                        $value->mes = 'Octubre';
                        break;
                    case 11:
                        $value->mes = 'Noviembre';
                        break;
                    case 12:
                        $value->mes = 'Diciembre';
                        break;
                }
                $mes[] = $value->mes;
                $cantidad[] = (int) $value->cantidad;
                //$jIngreso[] = array($value->mes, (int) $value->cantidad);
            }
            $dato->mes = $mes;
            $dato->cantidad = $cantidad;
            echo json_encode($dato);
        } else {
            $data['anios'] = $this->db->get('view_anio_ingreso');
            $data['introduccion'] = $this->Mreporte->items_ingreso();
            $data['sesion'] = $this->recupera_sesion();
            $data['menu'] = $this->acceso->menu();
            $this->load->view('reportes/items_ingreso/inicio', $data);
        }
    }

    public function genera_pdf() {
        prep_pdf();
        $this->cezpdf->ezText('<b>Cliente No.:</b> 12');
        $this->cezpdf->ezText('<b>Cliente:</b> Abraham Zenteno Sanchez');
        $this->cezpdf->ezText('<b>Tienda:</b>  Plaza Dorada');
        $this->cezpdf->ezText('<b>Fecha y hora de impresion:</b> ' . date('Y-m-d') . ', ' . date('H:i') . ' hrs.');
        $this->cezpdf->ezText('');
        $db_data[] = array('eye' => 'O.D.', 'ESF' => '+9.75', 'CIL' => '-1.25', 'EJE' => '3', 'ADD' => '+2.50', 'REF' => 'D.I. 4 mm');
        $db_data[] = array('eye' => 'O.I.', 'ESF' => '+9.20', 'CIL' => '-1.00', 'EJE' => '3', 'ADD' => '+4.50', 'REF' => 'D.I. 3 mm');

        $col_names = array(
            'eye' => '',
            'ESF' => 'ESF.',
            'CIL' => 'CIL.',
            'EJE' => 'EJE',
            'ADD' => 'ADD',
            'REF' => ''
        );

        $this->cezpdf->ezTable($db_data, $col_names, 'Graduacion registrada el 3 de Diciembre del 2009', array('width' => 550));

        $this->cezpdf->ezStream(array('Content-Disposition' => 'nama_file.pdf'));
    }

    /* function operaciones() {
      if ($this->input->post('terminal')) {
      $this->session->set_userdata('terminal_reporte', $this->input->post('terminal'));
      } else {
      $data['terminal'] = $this->db->get('terminal');
      $data['items'] = $this->db->get('item_bibliografico');
      $data['materiales'] = $this->db->get('material_bibliografico');
      $data['sindevolver'] = $this->db->get_where('evento_prestamo', array('estado' => 'SIN DEVOLVER'));
      $this->load->view('reportes/operaciones', $data);
      }
      } */

    function fecha_publicacion() {
        $parametro = $this->session->userdata('parametro');
        $terminal = $this->session->userdata('reporte');
        if ($this->input->post('rows')) {
            $limit = $this->input->post('rows');
        } else {
            $limit = 10;
        }

        if ($this->input->post('page')) {
            $page = $this->input->post('page', TRUE);
        } else {
            $page = 1;
            $start = 0;
        }
        $sidx = $this->input->post('sidx', TRUE);
        $sord = $this->input->post('sord', TRUE);

        if (!$sidx)
            $sidx = 1;
        $cantidad = $this->Mreporte->jqcant_fechapublicacion($terminal['terminal'], $parametro);
        foreach ($cantidad->result() as $value) {
            $count = (int) $value->cantidad;
        }
        if ($count > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages) {
            $page = $total_pages;
        }
        if ($this->input->post('page')) {
            $start = $limit * $page - $limit;
        }

        $query = $this->Mreporte->jquery_fechapublicacion($terminal['terminal'], $parametro, $sidx, $sord, $start, $limit);
        $jQgrid->page = $page;
        $jQgrid->total = $total_pages;
        $jQgrid->records = $count;
        $i = 0;
        foreach ($query->result() as $value) {
            $jQgrid->rows[$i]['id'] = $value->signatura;
            $jQgrid->rows[$i]['cell'] = array($value->autores, $value->titulo, $value->signatura, $value->isbn);
            $i = $i + 1;
        }
        echo json_encode($jQgrid);
    }

    function estadistico($var = NULL) {
        /* var_dump($this->session->all_userdata());
          die; */
        if ($this->input->post('terminales_item')) {
            $query = $this->db->get('view_terminal_cantidad');
            foreach ($query->result() as $value) {
                $jItem[] = array($value->terminal, (int) $value->cantidad);
            }
            echo json_encode($jItem);
        } else if ($this->input->post('terminal')) {
            $this->session->unset_userdata('reporte');
            $this->session->set_userdata('reporte', array('terminal' => $this->input->post('terminal'), 'nombre_terminal' => $this->Mvalidarusuario->determinaTerminal($this->input->post('terminal'))));
            $terminal = $this->session->userdata('reporte');
            $jTerminal['terminal'] = $terminal['nombre_terminal'];
            echo json_encode($jTerminal);
        } else if ($this->input->post('terminal_ingre')) {
            $anio = $this->session->userdata('parametro');
            $terminal = $this->session->userdata('reporte');
            $query = $this->Mreporte->fecha_ingreso($terminal['terminal'], $anio);
            foreach ($query->result() as $value) {
                switch ($value->mes) {
                    case 1:
                        $value->mes = 'Enero';
                        break;
                    case 2:
                        $value->mes = 'Febrero';
                        break;
                    case 3:
                        $value->mes = 'Marzo';
                        break;
                    case 4:
                        $value->mes = 'Abril';
                        break;
                    case 5:
                        $value->mes = 'Mayo';
                        break;
                    case 6:
                        $value->mes = 'Junio';
                        break;
                    case 7:
                        $value->mes = 'Julio';
                        break;
                    case 8:
                        $value->mes = 'Agosto';
                        break;
                    case 9:
                        $value->mes = 'Setiembre';
                        break;
                    case 10:
                        $value->mes = 'Octubre';
                        break;
                    case 11:
                        $value->mes = 'Noviembre';
                        break;
                    case 12:
                        $value->mes = 'Diciembre';
                        break;
                }
                $jIngreso[] = array((string) $value->mes, (int) $value->cantidad);
            }
            echo json_encode($jIngreso);
        } else if ($this->input->post('terminal_ingreso_column')) {
            $this->session->set_userdata('parametro', $this->input->post('terminal_ingreso_column'));
        } else if ($this->input->post('terminal_ingreso')) {
            $terminal = $this->session->userdata('reporte');
            $query = $this->Mreporte->fechaingreso($terminal['terminal']);
            foreach ($query->result() as $value) {
                $jIngreso[] = array($value->anio, (int) $value->cantidad);
            }
            echo json_encode($jIngreso);
        } else if ($this->input->post('terminal_pub')) {
            $terminal = $this->session->userdata('reporte');
            $query = $this->Mreporte->fechapublicacion($terminal['terminal']);
            foreach ($query->result() as $value) {
                $jPublicacion[] = array((int) $value->anio, (int) $value->cantidad);
            }
            echo json_encode($jPublicacion);
        } else if ($this->input->post('jqgrid')) {
            $sesion = $this->session->userdata('reporte');
            switch ($this->input->post('jqtipo')) {
                case 'fecha_publicacion':
                    $this->session->set_userdata('parametro', $this->input->post('jqparametro'));
                    $this->fecha_publicacion();
                    break;
                case 'fecha_ingreso':
                    $this->session->set_userdata('parametro', $this->input->post('jqparametro'));
                    $this->fecha_ingreso();
                    break;
                default:
                    break;
            }
        } else {
            $data['terminal'] = $this->db->get('terminal');
            $data['relacion'] = $this->db->get('view_terminal_cantidad');
            $this->load->view('reportes/estadistico/inicio_v2', $data);
            //$this->load->view('reportes/estadistico/inicio_v3');
        }
    }

    function operaciones_terminal($consulta = NULL) {
        if ($consulta == 'concluidos') {
            if ($this->input->post('signatura')) {
                $this->session->set_userdata('terminal_reporte_consulta', $this->input->post('signatura'));
            }
            $consu = $this->Mreporte->paginar_concluido($this->session->userdata('terminal_reporte_consulta'));
            if ($consu->num_rows() > 0) {
                $por_pagina = 5;
                $config['base_url'] = site_url('reporte/operaciones_terminal/concluidos');
                $config['total_rows'] = $consu->num_rows();
                $config['per_page'] = $por_pagina;
                $config['uri_segment'] = 4;
                $config['num_links'] = 3;
                $this->pagination->initialize($config);
                $dato['item'] = $this->db->get_where('view_reporte_operacion', array('signatura' => $this->session->userdata('terminal_reporte_consulta')));
                $dato['relacion'] = $this->Mreporte->paginar_concluido($this->session->userdata('terminal_reporte_consulta'), $por_pagina, $this->uri->segment(4));
                $this->load->view('reportes/operaciones_concluido', $dato);
            } else {
                echo 'fail';
            }
        } else if ($consulta == 'noconcluidos_analisis') {
            $this->session->set_userdata('terminal_reporte_consulta', $this->input->post('signatura'));
            $file1 = $this->Mreporte->paginar_inconcluso_solicitud($this->session->userdata('terminal_reporte_consulta'));
            $file2 = $this->Mreporte->paginar_inconcluso_prestamo($this->session->userdata('terminal_reporte_consulta'));
            $calculo = 0;
            if (($file1->num_rows() + $file2->num_rows()) > 0) {
                $jAnalisis['estado'] = 'ok';
            } else {
                $jAnalisis['estado'] = 'fail';
            } if ($file1->num_rows() > 0) {
                $calculo = $calculo + 1;
            } if ($file2->num_rows() > 0) {
                $calculo = $calculo + 2;
            }
            $jAnalisis['calculo'] = $calculo;
            echo json_encode($jAnalisis);
        } else if ($consulta == 'noconcluidos_solicitudes') {
            $total = $this->Mreporte->paginar_inconcluso_solicitud($this->session->userdata('terminal_reporte_consulta'));
            if ($total->num_rows() > 0) {
                $por_pagina = 6;
                $config['base_url'] = site_url('reporte/operaciones_terminal/noconcluidos_solicitudes');
                $config['total_rows'] = $total->num_rows();
                $config['per_page'] = $por_pagina;
                $config['uri_segment'] = 4;
                $config['num_links'] = 3;
                $this->pagination->initialize($config);
                $dato['item'] = $this->db->get_where('view_reporte_operacion', array('signatura' => $this->session->userdata('terminal_reporte_consulta')));
                $dato['relacion'] = $this->Mreporte->paginar_inconcluso_solicitud($this->session->userdata('terminal_reporte_consulta'), $por_pagina, $this->uri->segment(4));
                $this->load->view('reportes/operaciones_inconcluso_solicitud', $dato);
            } else {
                show_error('Error en la consulta');
            }
        } else if ($consulta == 'noconcluidos_prestamos') {
            $total = $this->Mreporte->paginar_inconcluso_prestamo($this->session->userdata('terminal_reporte_consulta'));
            if ($total->num_rows() > 0) {
                $por_pagina = 4;
                $config['base_url'] = site_url('reporte/operaciones_terminal/noconcluidos_prestamos');
                $config['total_rows'] = $total->num_rows();
                $config['per_page'] = $por_pagina;
                $config['uri_segment'] = 4;
                $config['num_links'] = 3;
                $this->pagination->initialize($config);
                $dato['item'] = $this->db->get_where('view_reporte_operacion', array('signatura' => $this->session->userdata('terminal_reporte_consulta')));
                $dato['relacion'] = $this->Mreporte->paginar_inconcluso_prestamo($this->session->userdata('terminal_reporte_consulta'), $por_pagina, $this->uri->segment(4));
                $this->load->view('reportes/operaciones_inconcluso_prestamo', $dato);
            } else {
                show_error('Error en la consulta');
            }
        } else {
            $por_pagina = 2;
            $total = $this->db->get_where('view_reporte_operacion', array('terminal' => $this->session->userdata('terminal_reporte')));
            $query = $this->Mreporte->paginar_operaciones($this->session->userdata('terminal_reporte'), $por_pagina, $this->uri->segment(3));
            $config['base_url'] = site_url('reporte/operaciones_terminal');
            $config['total_rows'] = $total->num_rows();
            $config['per_page'] = $por_pagina;
            $config['uri_segment'] = 3;
            $config['num_links'] = 5;
            $this->pagination->initialize($config);
            $carga['resultadoBusqueda'] = $query->result();
            $data['relacion_operaciones'] = $query;
            $data['terminal'] = $this->db->get('terminal');
            $data['terminal_act'] = $this->db->get_where('terminal', array('codTerminal' => $this->session->userdata('terminal_reporte')));
            $data['items'] = $this->db->get_where('item_bibliografico', array('codTerminal' => $this->session->userdata('terminal_reporte')));
            $data['materiales'] = $this->Mreporte->consulta_materiales($this->session->userdata('terminal_reporte'));
            $data['sindevolver'] = $this->Mreporte->consulta_sindevolver($this->session->userdata('terminal_reporte'));
            $this->load->view('reportes/operaciones_terminal', $data);
        }
    }

}

/* Fin del archivo reporte.php */
