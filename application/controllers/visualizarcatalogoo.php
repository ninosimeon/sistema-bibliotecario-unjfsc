<?php

class Visualizarcatalogoo extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Mvisualizarcatalogo');
        $this->load->model('Mevento');
        $this->acceso->controlar();
    }

    function index() {
        $carga['resultadoBusqueda'] = '';
        $sesion = $this->session->userdata('logeado');
        $carga['tematica'] = $this->Mvisualizarcatalogo->cargatematica();
        $carga['categoria'] = $this->Mvisualizarcatalogo->cargacategoria();
        $carga['div_procesamiento'] = '';
        $carga['div_determinante'] = 'busqueda';
        $carga['all_terminales'] = $this->Mvisualizarcatalogo->todoterminal();
        $carga['resultadoNovedades'] = '';
        if ($this->session->userdata('busquedaCatalogo')) {
            $busqueda = $this->session->userdata('busquedaCatalogo');
            $por_pagina = 6;
            $query = $this->Mvisualizarcatalogo->busqueda($sesion['ciclo'], $busqueda[0], $busqueda[1], $busqueda[2], $busqueda[3], $busqueda[4], $busqueda[5], $busqueda[6], $por_pagina, $this->uri->segment(3));
            $total = $this->Mvisualizarcatalogo->busqueda($sesion['ciclo'], $busqueda[0], $busqueda[1], $busqueda[2], $busqueda[3], $busqueda[4], $busqueda[5], $busqueda[6]);
            if ($query) {
                $config['base_url'] = site_url('visualizarcatalogoo/busquedacatalogo');
                $config['total_rows'] = $total->num_rows();
                $config['per_page'] = $por_pagina;
                $config['uri_segment'] = 3;
                $this->pagination->initialize($config);
                $carga['resultadoBusqueda'] = $query->result();
                //Aca metes codigo para el manejo de JS (ocultamiento)
            } else {
                $carga['resultadoBusqueda'] = 'fail';
            }
        } else if ($this->session->userdata('solicitudMaterial')) {
            $solicitud = $this->session->userdata('solicitudMaterial');
            $carga['cant_disponible'] = $this->Mvisualizarcatalogo->calculadisponibles($solicitud['terminal'], $solicitud['isbn'], $sesion['ciclo']);
            $carga['cant_nodisponible'] = $this->Mvisualizarcatalogo->calculaocupados($solicitud['terminal'], $solicitud['isbn'], $sesion['ciclo']);
            $carga['resultadoBusqueda'] = $this->Mvisualizarcatalogo->listarItems($solicitud['terminal'], $solicitud['isbn'], $sesion['ciclo']);
            $carga['div_determinante'] = 'solicitud';
            $carga['div_procesamiento'] = "$('.procesa').hide();";
        } else if ($this->session->userdata('novedades')) {
            $novedoso = $this->session->userdata('novedades');
            $carga['resultadoBusqueda'] = 'LMAO';
            $carga['div_determinante'] = 'novedades';
            $carga['div_procesamiento'] = "$('.procesa').hide();";
            if (isset($novedoso['novedades'])) {//detecta sesion con el post :D
                $carga['resultadoNovedades'] = $novedoso['novedades'];
            }
        }
        $this->load->view('visualizar_catalogo/visualizar_catalogo', $carga);
    }

    function novedades() {
        $datos = array();
        $this->session->unset_userdata('solicitudMaterial');
        $this->session->unset_userdata('busquedaCatalogo');
        $sesion = $this->session->userdata('logeado');
        if ($this->input->post()) {
            $query = $this->Mvisualizarcatalogo->determinanovedades($sesion ['ciclo'], $this->input->post('select_term'));
            $datos ['novedades'] = $query;
        }
        $datos ['estado_novedad'] = 'ok';
        $this->session->set_userdata('novedades', $datos);
        $this->index();
    }

    function regresaCatalogo() {
        if ($this->input->post()) {
            $this->session->unset_userdata('solicitudMaterial');
            $this->session->unset_userdata('novedades');
            $this->session->unset_userdata('busquedaCatalogo');
        }
        $this->index();
    }

    function busquedacatalogo() {
        if ($this->input->post()) {
            $parametroBusqueda[] = $this->input->post('terminal');
            $parametroBusqueda[] = $this->input->post('input_isb');
            $parametroBusqueda[] = $this->input->post('input_cat');
            $parametroBusqueda[] = $this->input->post('input_tem');
            $parametroBusqueda[] = $this->input->post('input_tit');
            $parametroBusqueda[] = $this->input->post('input_aut');
            $parametroBusqueda[] = $this->input->post('input_edi');
            $this->session->unset_userdata('solicitudMaterial');
            $this->session->unset_userdata('novedades');
            $this->session->set_userdata('busquedaCatalogo', $parametroBusqueda);
        }
        $this->index();
    }

    function procesaSolicitud() {
        if ($this->input->post()) {
            $parametroSolicitud['terminal'] = $this->Mvisualizarcatalogo->determinaterminal($this->input->post('item'));
            $parametroSolicitud['isbn'] = $this->Mvisualizarcatalogo->determinaisbn($this->input->post('item'));
            $this->session->unset_userdata('busquedaCatalogo');
            $this->session->unset_userdata('novedades');
            $this->session->set_userdata('solicitudMaterial', $parametroSolicitud);
        }
        $this->index();
    }

    function solicitud_reserva() {
        $sesion = $this->session->userdata('logeado');
        if ($this->input->post('averigua')) {
            $query = $this->db->get_where('view_busqueda', array('signatura' => $this->input->post('averigua'), 'estado' => 'DISPONIBLE'));
            if ($query->num_rows() > 0) {
                $data['estado'] = 'DISPONIBLE';
            } else {
                $datos_item = $this->Mvisualizarcatalogo->determina_datos($this->input->post('averigua'));
                foreach ($datos_item->result() as $value) {
                    $data['hora'] = $value->fechaFin;
                    $data['fecha'] = $value->horaFin;
                }
                $datos_persona = $this->Mvisualizarcatalogo->determina_persona($this->input->post('averigua'));
                $i = 0;
                foreach ($datos_persona->result() as $value) {
                    $data['dni'][$i] = $value->usuario;
                    $i = $i + 1;
                }
                //$data['estado'] = 'NO DISPONIBLE';
            }
            echo json_encode($data);
        } else if ($this->input->post('solicitud')) {
            if ($this->Mevento->verificaSolicitudAnterior($sesion['cuenta'])) {
                echo 'pendiente';
            } else {
                $this->Mevento->solicitud_reserva($sesion['cuenta'], $this->input->post('solicitud'), $this->input->post('tipo_solicitud'));
            }
        } else {
            show_error('Estas aqui por equivocación ¬¬[!]. ¡RETROCEDE!');
        }
    }

}

?>
