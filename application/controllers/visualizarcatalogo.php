<?php

class Visualizarcatalogo extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Mvisualizarcatalogo');
        $this->load->model('Mevento');
        $this->load->model('Mvalidarusuario');
        $this->acceso->controlar();
        if ($this->agent->is_browser('MSIE', 'Internet Explorer')) {
            redirect('visualizarcatalogoo', 'refresh');
        }
    }

    function consulta_isbn() {
        if ($this->input->post('isbn_consulta')) {
            $query = $this->db->get_where('view_busqueda', array('ISBN' => $this->input->post('isbn_consulta')));
            foreach ($query->result() as $value) {
                if (isset($value->indice)) {
                    $jISBN = nl2br($value->indice);
                } else {
                    $jISBN = 'fail';
                }
            }

            echo json_encode($jISBN);
        }
    }

    function index() {
        $imprime = array();
        $config = array();
        $sesion = $this->session->userdata('logeado');
        $imprime ['persona'] = array('cargo' => $sesion ['perfil_usuario'], 'nombres' => $sesion ['apellidos_nombres'], 'terminal' => $sesion ['nom_terminal'], 'id_terminal' => $sesion ['cod_terminal']);
        $imprime ['resultados'] = '';
        if ($this->session->userdata('busquedaCatalogo')) {
            $recupera_busqueda = $this->session->userdata('busquedaCatalogo');
            $por_pagina = 10;
            if ($recupera_busqueda[0] == 'simple') {
                $query = $this->Mvisualizarcatalogo->busqueda_simple($recupera_busqueda [1], $por_pagina, $this->uri->segment(3));
                $total = $this->Mvisualizarcatalogo->busqueda_simple($recupera_busqueda [1]);
                if ($query) {
                    $config ['base_url'] = site_url('visualizarcatalogo/busqueda_simple');
                    $config ['total_rows'] = $total->num_rows();
                    $config ['per_page'] = $por_pagina;
                    $config ['uri_segment'] = 3;
                    $this->pagination->initialize($config);
                    $imprime ['resultados'] = $query->result();
                } else {
                    $imprime ['resultados'] = 'fail';
                }
            } else if ($recupera_busqueda[0] == 'avanzado') {
                $query = $this->Mvisualizarcatalogo->busqueda_avanzada($recupera_busqueda [1], $recupera_busqueda[2], $recupera_busqueda[3], $recupera_busqueda[4], $por_pagina, $this->uri->segment(3));
                $total = $this->Mvisualizarcatalogo->busqueda_avanzada($recupera_busqueda [1], $recupera_busqueda[2], $recupera_busqueda[3], $recupera_busqueda[4]);
                if ($query) {
                    $config ['base_url'] = site_url('visualizarcatalogo/busqueda_avanzada');
                    $config ['total_rows'] = $total->num_rows();
                    $config ['per_page'] = $por_pagina;
                    $config ['uri_segment'] = 3;
                    $this->pagination->initialize($config);
                    $imprime ['resultados'] = $query->result();
                } else {
                    $imprime ['resultados'] = 'fail';
                }
            }
        } 
        $this->load->view('visualizar_catalogo/buscadorHTML5', $imprime);
    }

    /*
     * busqueda_avanzada()
     * Realiza la busqueda con varios INPUT TEXT
     */

    function busqueda_avanzada() {
        if ($this->input->post()) {
            $parametroBusqueda = array();
            $parametroBusqueda [] = 'avanzado';
            $parametroBusqueda [] = $this->input->post('input_autor');
            $parametroBusqueda [] = $this->input->post('input_isbn');
            $parametroBusqueda [] = $this->input->post('input_titulo');
            $parametroBusqueda [] = $this->input->post('input_contenido');
            $this->session->set_userdata('busquedaCatalogo', $parametroBusqueda);
        }
        $this->index();
    }

    /*
     * busqueda_simple()
     * Realiza una busqueda simple, SOLO EN 1 INPUT TEXT. 
     */

    function busqueda_simple() {
        if ($this->input->post()) {
            $parametroBusqueda = array(); //Por buenas practicas se inicializa la variable
            $parametroBusqueda [] = 'simple';
            $parametroBusqueda [] = $this->input->post('simple');
            $this->session->set_userdata('busquedaCatalogo', $parametroBusqueda);
        }
        $this->index();
    }

    function busquedacatalogo() {
        if ($this->input->post()) {
            $parametroBusqueda = array();
            $parametroBusqueda [] = $this->input->post('terminal');
            $parametroBusqueda [] = $this->input->post('input_isb');
            $parametroBusqueda [] = $this->input->post('input_cat');
            $parametroBusqueda [] = $this->input->post('input_tem');
            $parametroBusqueda [] = $this->input->post('input_tit');
            $parametroBusqueda [] = $this->input->post('input_aut');
            $parametroBusqueda [] = $this->input->post('input_edi');
            $this->session->set_userdata('busquedaCatalogo', $parametroBusqueda);
        }
        $this->index();
    }

    /*
     * Inicia el trance
     */

    function procesa_solicitud() {
        $imprime = array();
        $determinante = 0;
        $a = 0;
        $b = 0;
        $this->session->unset_userdata('busquedaCatalogo');
        $sesion = $this->session->userdata('logeado');
        $imprime ['persona'] = array('cargo' => $sesion ['perfil_usuario'], 'nombres' => $sesion ['apellidos_nombres'], 'terminal' => $sesion ['nom_terminal'], 'id_terminal' => $sesion ['cod_terminal'], 'dni' => $sesion ['cuenta']);
        $query = $this->Mvisualizarcatalogo->describe_libros($this->input->post('valor_isbn'), $this->input->post('ubicacion'));
        foreach ($query->result() as $value) {
            if ($value->modPrestamo == 'INTERNO') {
                $a = 1;
            }
            if ($value->modPrestamo == 'EXTERNO') {
                $b = 2;
            }
            $imprime ['libro'] = array('titulo' => $value->titulo, 'autor' => $value->autores, 'publicacion' => $value->fecPublicacion, 'edicion' => $value->edicion, 'volumen' => $value->volumen, 'ubicacion' => $value->nomTerminal);
        }
        $determinante = $a + $b;
        $parametros_interno = array('type' => 'button', 'content' => 'INTERNO', 'id' => 'btn_interno', 'value' => 'INTERNO');
        $parametros_externo = array('type' => 'button', 'content' => 'EXTERNO', 'id' => 'btn_externo', 'value' => 'EXTERNO');
        $imprime ['boton_interno'] = '';
        $imprime ['boton_externo'] = '';
        switch ($determinante) {
            case 1 :
                $imprime ['boton_interno'] = form_button($parametros_interno);
                break;
            case 2 :
                $imprime ['boton_externo'] = form_button($parametros_externo);
                break;
            case 3 :
                $imprime ['boton_interno'] = form_button($parametros_interno);
                $imprime ['boton_externo'] = form_button($parametros_externo);
                break;
        }
        $imprime ['isbn'] = $this->input->post('valor_isbn');
        $imprime ['ubicacion'] = $this->input->post('ubicacion');
        $imprime ['cant_disponible'] = $this->Mvisualizarcatalogo->calculadisponibles($imprime ['ubicacion'], $imprime ['isbn'], $sesion ['ciclo']);
        $imprime ['cant_nodisponible'] = $this->Mvisualizarcatalogo->calculaocupados($imprime ['ubicacion'], $imprime ['isbn'], $sesion ['ciclo']);
        if ($imprime ['cant_disponible'] > 0) {
            $parametros = array('type' => 'button', 'style' => 'height: 55px;', 'content' => 'SOLICITAR', 'onclick' => 'inicia_solicitud()');
            $imprime ['boton_disponible'] = form_button($parametros);
        } else {
            $imprime ['boton_disponible'] = '';
        }
        if ($imprime ['cant_nodisponible'] > 0) {
            $parametros = array('type' => 'button', 'style' => 'height: 55px;', 'content' => 'RESERVAR', 'onclick' => 'inicia_reserva()');
            $imprime ['boton_nodisponible'] = form_button($parametros);
        } else {
            $imprime ['boton_nodisponible'] = '';
        }
        $this->load->view('visualizar_catalogo/procesa_solicitud', $imprime);
    }

    /*
     * Fin del trance
     */

    function solicitud_reserva() {
        $data = array();
        $item = '';
        $sesion = $this->session->userdata('logeado');
        if ($this->input->post('solicitud_reserva')) {
            if ($this->Mevento->verificaSolicitudAnterior($sesion ['cuenta'])) {
                $data ['estado'] = 'pendiente';
            } else if (!$this->Mvalidarusuario->sancionado($sesion['cuenta'])) {
                $data ['estado'] = 'sancion';
            } else {
                /* recibe peticiones ajax */
                if ($this->input->post('tipo_solicitud') == 'INTERNO') {
                    $data ['estado'] = 'INTERNO';
                    $determina_item = $this->db->get_where('view_busqueda', array('ISBN' => $this->input->post('isbn'), 'codTerminal' => $this->input->post('terminal'), 'estado' => 'DISPONIBLE'), 1);
                    foreach ($determina_item->result() as $value) {
                        $item = $value->signatura;
                    }
                    $this->Mevento->solicitud_reserva($sesion ['cuenta'], $item, 'SOLICITUD');
                } else if ($this->input->post('tipo_solicitud') == 'EXTERNO') {
                    $data ['estado'] = 'EXTERNO';
                    $determina_item = $this->db->get_where('view_busqueda', array('ISBN' => $this->input->post('isbn'), 'codTerminal' => $this->input->post('terminal'), 'estado' => 'DISPONIBLE', 'modPrestamo' => 'EXTERNO'), 1);
                    foreach ($determina_item->result() as $value) {
                        $item = $value->signatura;
                    }
                    $this->Mevento->solicitud_reserva($sesion ['cuenta'], $item, 'SOLICITUD');
                } else if ($this->input->post('tipo_solicitud') == 'RESERVA') {
                    $data ['estado'] = 'RESERVA';
                    $determina_item = $this->db->get_where('view_busqueda', array('ISBN' => $this->input->post('isbn'), 'codTerminal' => $this->input->post('terminal'), 'estado' => 'NO DISPONIBLE'), 1);
                    foreach ($determina_item->result() as $value) {
                        $item = $value->signatura;
                    }
                    $this->Mevento->solicitud_reserva($sesion ['cuenta'], $item, 'RESERVA');
                    $datos_item = $this->Mvisualizarcatalogo->determina_datos($item);
                    foreach ($datos_item->result() as $value) {
                        $data['fecha'] = $value->fechaFin;
                        $data['hora'] = $value->horaFin;
                    }
                }
            }
            echo json_encode($data);
        }
    }

    function solicitud_reserva_XXXXXX() {
        $data = array();
        $sesion = $this->session->userdata('logeado');
        if ($this->input->post('averigua')) {
            $query = $this->db->get_where('view_busqueda', array('signatura' => $this->input->post('averigua'), 'estado' => 'DISPONIBLE'));
            if ($query->num_rows() > 0) {
                $data ['estado'] = 'DISPONIBLE';
            } else {
                $datos_item = $this->Mvisualizarcatalogo->determina_datos($this->input->post('averigua'));
                foreach ($datos_item->result() as $value) {
                    $data ['hora'] = $value->fechaFin;
                    $data ['fecha'] = $value->horaFin;
                }
                $datos_persona = $this->Mvisualizarcatalogo->determina_persona($this->input->post('averigua'));
                $i = 0;
                foreach ($datos_persona->result() as $value) {
                    $data ['dni'] [$i] = $value->usuario;
                    $i = $i + 1;
                }
                // $data['estado'] = 'NO DISPONIBLE';
            }
            echo json_encode($data);
        } else if ($this->input->post('solicitud')) {
            if ($this->Mevento->verificaSolicitudAnterior($sesion ['cuenta'])) {
                echo 'pendiente';
            } else {
                $this->Mevento->solicitud_reserva($sesion ['cuenta'], $this->input->post('solicitud'), $this->input->post('tipo_solicitud'));
            }
        } else {
            show_error('Estas aqui por equivocación ¬¬[!]. ¡RETROCEDE!');
        }
    }

}

/* Fin del archivo visualizarcatalogo.php */
