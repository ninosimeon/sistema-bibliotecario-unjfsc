<?php

class Variado extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Mvalidarusuario');
    }

    function cerrar_sesion() {
        $this->session->sess_destroy();
        redirect('validarusuario', 'refresh');
    }

    /*
     * LA FUNCION A CONTINUACIÓN(password()) RESETEA LAS CONTRASEÑAS SIENDO ESTAS LA MISMA QUE SU USUARIO (DNI)
     */

    function password() {
        $dni = array();
        $query_1 = $this->db->get('usuario');
        foreach ($query_1->result() as $value) {
            $dni[] = $value->cuenta;
        }
        for ($index = 0; count($dni); $index++) {
            $this->db->where('cuenta', $dni[$index]);
            $this->db->update('usuario', array('clave' => $this->encrypt->encode($dni[$index])));
        }
    }

    function panel() {
        $sesion = $this->session->userdata('logeado');
        if ($sesion['perfil_usuario'] == 'INVITADO') {
            redirect('validarusuario', 'refresh');
        }
        $imprime = array();
        $jRespuesta = array();
        /* var_dump($this->session->all_userdata());
          die; */
        $sesion = $this->session->userdata('logeado');
        if ($this->input->post('password')) {
            $query = $this->Mvalidarusuario->validar($sesion['cuenta'], $this->input->post('inicial'));
            if ($query) {
                $jRespuesta['estado'] = 'ok';
                $this->db->where('cuenta', $sesion['cuenta']);
                $this->db->update('usuario', array('clave' => $this->encrypt->encode($this->input->post('nuevo'))));
            } else {
                $jRespuesta['estado'] = 'fail';
            }
            echo json_encode($jRespuesta);
        } else if ($this->input->post('direccion')) {
            $this->db->where('numero', $sesion['cuenta']);
            $this->db->update('persona', array('dirFisica' => $this->input->post('direccion')));
        } else if ($this->input->post('mail')) {
            $this->db->where('numero', $sesion['cuenta']);
            $this->db->update('persona', array('email' => $this->input->post('mail')));
        } else if ($this->input->post('telefono')) {
            $this->db->where('numero', $sesion['cuenta']);
            $this->db->update('persona', array('dirFisica' => $this->input->post('telefono')));
        } else {
            $persona = $this->db->get_where('persona', array('numero' => $sesion['cuenta']));
            foreach ($persona->result() as $value) {
                $imprime['panel_persona'] = array('nombres' => $value->apellidos . ', ' . $value->nombres, 'ubicacion' => $sesion['nom_terminal'], 'fecha' => $value->fechaNac, 'direccion' => $value->dirFisica, 'mail' => $value->email, 'telefono' => $value->telefono);
            }
            $imprime ['persona'] = array('cargo' => $sesion ['perfil_usuario'], 'nombres' => $sesion ['apellidos_nombres'], 'terminal' => $sesion ['nom_terminal'], 'id_terminal' => $sesion ['cod_terminal']);
            $imprime ['resultados'] = '';
            $this->load->view('variado/panel_usuario', $imprime);
        }
    }

    function ver_item() {
        $jItem = array();
        if ($this->input->post('item')) {
            $query = $this->db->get_where('view_item', array('signatura' => $this->input->post('item')));
            foreach ($query->result() as $value) {
                $jItem['isbn'] = $value->isbn;
                $jItem['titulo'] = $value->titulo;
                $jItem['autor'] = $value->autores;
                $jItem['editorial'] = $value->editorial;
                $jItem['tematica'] = $value->tematica;
                $jItem['fecha'] = $value->fecPublicacion;
                $jItem['terminal'] = $value->nomTerminal;
            }
            echo json_encode($jItem);
        }
    }

}

?>
