<?php $sesion = $this->session->userdata('logeado'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>      
        <meta charset="utf-8" />
        <meta name="description" content="Sistema Web Bibliotecario UNJFSC" />  
        <title>..::Sistema Bibliotecario::..</title>
        <link href="<?php echo base_url('public/css/temaBibliotecaBibliotecario.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('public/css/fresh_theme.css'); ?>" rel="stylesheet" type="text/css" />
        <link rel="icon" href="<?php echo base_url('public/img/favicon.ico'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/ui.jqgrid.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.ui.sunny.css'); ?>"/>        
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.alerts.css'); ?>"/>        
        <script src="<?php echo base_url('public/lib/jquery.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.datepicker-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.sunny.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/grid.locale-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.jqGrid.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.alerts.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.alphanumeric.js'); ?>" type="text/javascript" ></script>
        <script>           
            $().ready(function() { 
                $('button').button()
                $('.seleccion').button({
                    icons:{
                        primary: "ui-icon-arrowreturnthick-1-e"
                    }
                })      
                $('.seleccion_another').button({
                    icons:{
                        primary: "ui-icon ui-icon-circle-triangle-e"
                    }
                })               
                $('#subida').hide();
                $('#msg_existe').dialog({
                    autoOpen: false,
                    show: "blind",
                    hide: "explode",
                    buttons:{
                        "¿Agregar item?": function(){
                            document.location.href= '<?php echo site_url('bibliografico/agregar_item'); ?>';
                        },                       
                        "¿Subir índice?": function(){
                            iniciar_subida();
                            $(this).dialog('close');                            
                        }
                    },
                    resizable: false,
                    modal: true,
                    title: 'MATERIAL EXISTENTE ¬¬ ¡ALERTA!'
                });
                $('.material').hide();
                $('#agregar_material').show();
                recargar_tematica();
                $('#input_tematica').change(function(){                   
                    $.post('<?php echo site_url('bibliografico/agregar_material'); ?>',{busca_tema: $('#input_tematica').val()},function(r){
                        $('.material').hide();
                        $('#selecciona_tematica').show();                          
                        for (i = 0; r.length; i++) {
                            $('#tematica_especifico').append($('<option></option>').attr('value',r[i].valor).text(r[i].texto));
                        }                       
                        
                    },'json');
                    $('#input_tematica').empty();
                    $('#input_tematica').attr('id','tematica_definido');   
                    //jAlert('<select name="tematica_especifico" id="tematica_especifico"></select>', 'SELECCIONE EL TEMA');
                });
                $('#input_isbn').keyup(function(r){
                    if (r.keyCode == 13) {
                        $.post('<?php echo site_url('bibliografico/agregar_material'); ?>',{v_isbn: $('#input_isbn').val()},function(e){
                            if (e == 'ok') {
                                $('.input_text').attr('disabled',false);
                                $('.input_btn').attr('disabled',false);
                                $('#input_isbn').attr('readonly',true);
                            }else{                                
                                $('#input_isbn').attr('readonly',true);
                                $('#msg_existe h1').empty();
                                $('#msg_existe h1').append($('#input_isbn').val());
                                $('#msg_existe').dialog('open');
                                /*jAlert('ISBN EXISTENTE, pasa a ITEM BIBLIOGRAFICO ¬¬ <br /><h1>'+$('#input_isbn').val()+'</h1>','¡WARNING!',function(ev){
                                    if (ev) {
                                        document.location.href= '<?php echo site_url('bibliografico/agregar_item'); ?>';
                                    }
                                });*/
                            }
                        });
                    }
                });
                $('#input_edicion').numeric();
                $('#input_volumen').numeric();
                $('#btn_limpiar').click(function(){
                    document.location.href= '<?php echo site_url('bibliografico/agregar_material'); ?>';
                });                
                $('#btn_salir').click(function(){
                    document.location.href= '<?php echo site_url('bibliografico'); ?>';
                });
                $('.input_text').attr('disabled',true);
                $('.input_btn').attr('disabled',true);                
                $('#buscador').hide();  
                $('#input_fecha').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'yy-mm-dd',
                    option: $.datepicker.regional['es'],
                    yearRange: '1910:2020'
                });
            });   

            function agrega_material(){                                                 
                $.post('<?php echo site_url('bibliografico/agregar_material'); ?>',{new_material: 'ok',isbn: $('#input_isbn').val(),titulo: $('#input_titulo').val(),categoria: $('#input_categoria').val(),tematica: $('#tematica_definido').val(),autores: $('#input_autores').val(),editorial: $('#input_editorial').val(), fecha: $('#input_fecha').val(),edicion: $('#input_edicion').val(), volumen: $('#input_volumen').val()},function(r){
                    if (r == 'ok') {
                        var valor_isbn = $('#input_isbn').val();
                        jConfirm('SE AGREGO CON ÉXITO, ¿Deseas agregar el ítem?<br /><h1>'+valor_isbn+'</h1>', '¡CORRECTO!', function(e){
                            if (e) {                                         
                                document.location.href= '<?php echo site_url('bibliografico/agregar_item'); ?>';
                            }                            
                        });
                        $('.input_btn').attr('disabled',true); 
                    }else{
                        jAlert('NO SE PUDO AGREGAR','¡PROBLEMAS!');
                    }
                });
                return false;   
            }
            
            function iniciar_subida(){
                $.post('<?php echo site_url('subir/verifica'); ?>',{isbn: $('#input_isbn').val()},function(r){
                    if (r) {
                        $('#agregar').hide();                       
                        $('#subida').empty();
                        $('#subida').append("<p>Ya fue agregado el índice =(</p>");
                        $('#subida').show();
                    } else {
                        $('#agregar').hide();
                        $('#subida').show();
                    }
                },'json');                                
            }
            
            function agrega_material_v1(){
                $.post('<?php echo site_url('bibliografico/agregar_material'); ?>',{new_material: 'ok',isbn: $('#input_isbn').val(),titulo: $('#input_titulo').val(),categoria: $('#input_categoria').val(),tematica: $('#tematica_definido').val(),autores: $('#input_autores').val(),editorial: $('#input_editorial').val(), fecha: $('#input_fecha').val(),edicion: $('#input_edicion').val(), volumen: $('#input_volumen').val()},function(r){
                    if (r == 'ok') {
                        jConfirm('SE AGREGO CON ÉXITO, ¿Deseas agregar el ítem?', '¡CORRECTO!', function(e){
                            if (e) {                                         
                                document.location.href= '<?php echo site_url('bibliografico/agregar_item'); ?>';
                            }
                        });
                    }else{
                        jAlert('NO SE PUDO AGREGAR','¡PROBLEMAS!');
                    }
                });
            }
            function agrega_categoria(){
                jPrompt('INGRESE NUEVA CATEGORIA: ','','CATEGORIA',function(r){
                    if (r) {
                        $.post('<?php echo site_url('bibliografico/agregar_material'); ?>',{new_categoria: r},function(e){
                            if (e == 'ok') {
                                $('#input_categoria').append($('<option></option>').attr('value',r).text(r));
                            }else{
                                jAlert('CATEGORIA DUPLICADA','¡PELIGRO!');
                            }
                        });
                    }
                });                
            }
            function definido(){                
                //jAlert($('select[name="tematica_especifico"] option:selected').text());                
                $('.material').hide();
                $('#agregar_material').show();
                $('#tematica_definido').append($('<option></option>').attr('value',$('select[name="tematica_especifico"] option:selected').val()).text($('select[name="tematica_especifico"] option:selected').text()));
            }
            function recargar_tematica(){
                $('#tematica_definido').attr('id','input_tematica');
                $('#input_tematica').empty();
                $('#tematica_especifico').empty();
                $.post('<?php echo site_url('bibliografico/agregar_material'); ?>',{recarga_tema: 'ok'},function(r){  
                    $('#input_tematica').append($('<option></option>').attr('value','XXX').text('Seleccione'));
                    for (i = 0;r.length; i++) {
                        $('#input_tematica').append($('<option></option>').attr('value',r[i].valor).text(r[i].texto));
                    }
                },'json');
            }
        </script>
    </head>
    <body>    
        <div id="msg_existe">
            <p>El ISBN <h1 style="text-align: center"></h1></p>
    </div>
    <div id="contenido" class="ui-widget">
        <div id="buscador">Material: 
            <input type="text" name="usuarioBusca" id="usuarioBusca" />
            <input type="button" name="ir" id="ir" value="Ir" onclick="buscar($('#usuarioBusca').val())" /><div id="resultado" style="color: #000">
                <p><b>No encontrado! =(</b></p>
            </div></div>
        <div id="titulo"><strong>AGREGAR MATERIAL BIBLIOGRÁFICO</strong></div>
        <div id="cabezera"><img src="<?php echo base_url(); ?>public/img/bannerAdministrativo.png" width="800" height="67" alt="banner" /></div>
        <div id="menu" class="">
            <div>
                <h4 class="ui-widget-header ui-corner-top">MATERIAL bibliográfico</h4>
                <div class="ui-widget-content">                 
                    <?php echo anchor('bibliografico/agregar_material', "<button class='seleccion'>Agregar</button>"); ?><br>
                    <?php echo anchor('bibliografico/deshabilitar_material', "<button class='seleccion'>Deshabilitar</button>"); ?>
                </div>
                <h4 class="ui-widget-header">ITEM bibliográfico</h4>
                <div class="ui-widget-content ui-corner-bottom">
                    <?php echo anchor('bibliografico/agregar_item', "<button class='seleccion'>Agregar</button>"); ?><br>
                    <?php echo anchor('bibliografico/deshabilitar_item', "<button class='seleccion'>Deshabilitar</button>"); ?>
                </div>                                  
            </div>
            <div id="otros_menu" class="" style="margin-top: 10px;">
                <?php echo $menu; ?>                                 
            </div>
            <div id="terminal" class="ui-corner-all ui-widget-content">
                TERMINAL:<br> 
                <b><?php echo $sesion['nom_terminal']; ?></b>
            </div>
        </div>
        <footer id="pieDePagina" class="ui-state-default">
            <div style="float: left;">
                Ciudad Universitaria - Av. Mercedes Indacochea N° 609<br />
                Teléfono: 232-1338, Huacho - Perú
            </div>
            <div style="float: right">Desarrollado por: Nino D. Simeón Huaccho</div>                    
            <div style="clear: both;"></div>
        </footer>
        <div id="logeado" class="ui-widget-header">         
            <b><?php echo $sesion ['perfil_usuario']; ?>,</b> <?php echo $sesion ['apellidos_nombres']; ?> 
            <nav style="margin-right: 10px;float: right;">
                <a href="<?php echo site_url('variado/panel'); ?>">Panel de usuario</a> | 
                <a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar Sesión</a>
            </nav>
        </div>
        <div id="terminal">TERMINAL:<br />
            <strong><?php echo $sesion['nom_terminal']; ?></strong></div>
        <div id="contenido_contenido">
            <div id="selecciona_tematica" class="material">
                <h3><strong>SELECCIONE LA TEMÁTICA</strong></h3>
                <p>
                    <select name="tematica_especifico" id="tematica_especifico">
                    </select>
                </p>
                <p>
                    <input type="button" name="btn_confirma_tematica" id="btn_confirma_tematica" value="OK" onclick="definido()"/>
                </p>
            </div>
            <div id="agregar_material" class="material">
                <table style="width: 100%;" class="ui-widget-content ui-corner-bottom">
                    <thead  class="ui-widget-header">
                        <tr>
                            <td colspan="2" style="text-align: center;"><strong>AGREGAR MATERIAL BIBLIOGRÁFICO</strong></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td width="29%"><label for="input_isbn"><strong>ISBN/CODIGO:</strong></label></td>
                            <td width="71%">
                            <input name="input_isbn" placeholder="El ISBN" type="text" id="input_isbn" style="width:50%" /> <a href="<?php echo site_url('bibliografico/add_sin_isbn'); ?>">¿No tiene ISBN?</a> | <a href="<?php echo site_url('bibliografico/ebook'); ?>">¿E-BOOK?</a></td>
                        </tr>
                    </tbody>                    
                </table>
                <form id="agregar" onsubmit="return agrega_material()" style="margin-top: 15px;">
                    <table class="ui-widget-content ui-corner-all" style="padding: 5px;">
                        <tr>
                            <td>
                                <label for="input_categoria"><b>CATEGORÍA</b></label>
                            </td>
                            <td>
                                <select name="input_categoria" id="input_categoria" class="input_text">
                                    <?php foreach ($categoria->result() as $value) {
                                        ?>
                                        <option value="<?php echo $value->categoria; ?>"><?php echo $value->categoria; ?></option><?php } ?>
                                </select>
                                <input type="button" name="btn_agrega_categoria" id="btn_agrega_categoria" value="+" class="input_btn" onclick="agrega_categoria()"/>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>TEMÁTICA:</strong></td>                            
                        </tr>
                        <tr>
                            <td colspan="2">
                                <select name="input_tematica" id="input_tematica" class="input_text">
                                </select>
                                <input type="button" name="btn_recarga_tematica" id="btn_recarga_tematica" value="ว" class="input_btn" onclick="recargar_tematica()"/>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>TÍTULO:</strong></td>
                            <td>
                                <input type="text" required placeholder="Título de la obra" name="input_titulo" id="input_titulo" style="width:360px;" class="input_text"/></td>
                        </tr>
                        <tr>
                            <td><strong>AUTORES:</strong></td>
                            <td>
                                <input type="text" required placeholder="Autor(es) de la obra" name="input_autores" id="input_autores" style="width:360px;" class="input_text"/></td>
                        </tr>
                        <tr>
                            <td><strong>EDICIÓN:</strong></td>
                            <td>
                                <input type="number" required name="input_edicion" id="input_edicion" style="width: 50px;" class="input_text"/></td>
                        </tr>
                        <tr>
                            <td><strong>VOLUMEN:</strong></td>
                            <td><input type="number" name="input_volumen" id="input_volumen" style="width: 50px;" class="input_text"/></td>
                        </tr>
                        <tr>
                            <td><strong>EDITORIAL/EMPRESAS:</strong></td>
                            <td><input type="text" required placeholder="Editorial de la obra" name="input_editorial" id="input_editorial" style="width:360px;" class="input_text"/></td>
                        </tr>
                        <tr>
                            <td><strong>FECHA DE PUBLICACIÓN:</strong></td>
                            <td>
                                <input name="input_fecha" required type="date" class="input_text" id="input_fecha" style="width:30%"/></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">                                                                
                                <input type="submit" name="btn_agregar" id="btn_agregar" value="AGREGAR MATERIAL" class="input_btn"/>
                                <input type="button" name="btn_limpiar" id="btn_limpiar" value="LIMPIAR" class="input_btn"/>
                                <input type="button" name="btn_salir" id="btn_salir" value="SALIR" class="input_btn"/></td>
                        </tr>
                    </table>                    
                </form>
                <?php echo form_open_multipart('subir/subir_ya', array('id' => 'subida')); ?>
                <input type="file" name="userfile" size="20" />
                <br />
                <input type="submit" value="Subir" />
                </form>
            </div>
        </div>
    </div>
</body>
</html>