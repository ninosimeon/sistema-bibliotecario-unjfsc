<?php $sesion = $this->session->userdata('logeado'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="description" content="Sistema Web Bibliotecario UNJFSC" />  
        <title>..::Sistema Bibliotecario::..</title>
        <link href="<?php echo base_url('public/css/temaBibliotecaBibliotecario.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('public/css/fresh_theme.css'); ?>" rel="stylesheet" type="text/css" />
        <link rel="icon" href="<?php echo base_url('public/img/favicon.ico'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/ui.jqgrid.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.ui.sunny.css'); ?>"/>        
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.alerts.css'); ?>"/>        
        <script src="<?php echo base_url('public/lib/jquery.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.datepicker-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.sunny.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/grid.locale-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.jqGrid.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.alerts.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.alphanumeric.js'); ?>" type="text/javascript" ></script>
        <script>           
            $().ready(function() {  
                $('button').button()
                $('.seleccion').button({
                    icons:{
                        primary: "ui-icon-arrowreturnthick-1-e"
                    }
                })      
                $('.seleccion_another').button({
                    icons:{
                        primary: "ui-icon ui-icon-circle-triangle-e"
                    }
                })              
                $('#buscador').hide()              
            });            
        </script>
    </head>
    <body>    
        <div id="contenido" class="ui-widget">
            <div id="buscador">Material: 
                <input type="text" name="usuarioBusca" id="usuarioBusca" />
                <input type="button" name="ir" id="ir" value="Ir" onclick="buscar($('#usuarioBusca').val())" /><div id="resultado" style="color: #000">
                    <p><b>No encontrado! =(</b></p>
                </div></div>
            <div id="titulo"><strong>BIENVENIDO</strong></div>
            <div id="cabezera"><img src="<?php echo base_url(); ?>public/img/bannerAdministrativo.png" width="800" height="67" alt="banner" /></div>
            <div id="menu" class="">
                <div>
                    <h4 class="ui-widget-header ui-corner-top">MATERIAL bibliográfico</h4>
                    <div class="ui-widget-content">                 
                        <?php echo anchor('bibliografico/agregar_material', "<button class='seleccion'>Agregar</button>"); ?><br>
                        <?php echo anchor('bibliografico/deshabilitar_material', "<button class='seleccion'>Deshabilitar</button>"); ?>
                    </div>
                    <h4 class="ui-widget-header">ITEM bibliográfica</h4>
                    <div class="ui-widget-content">
                        <?php echo anchor('bibliografico/agregar_item', "<button class='seleccion'>Agregar</button>"); ?><br>
                        <?php echo anchor('bibliografico/deshabilitar_item', "<button class='seleccion'>Deshabilitar</button>"); ?>
                    </div>                                  
                </div>
                <div id="otros_menu" class="" style="margin-top: 10px;">
                    <?php echo $menu; ?>                                 
                </div>
                <div id="terminal" class="ui-corner-all ui-widget-content">
                    TERMINAL:<br> 
                    <b><?php echo $sesion['nom_terminal']; ?></b>
                </div>
            </div>
            <footer id="pieDePagina" class="ui-state-default">
                <div style="float: left;">
                    Ciudad Universitaria - Av. Mercedes Indacochea N° 609<br />
                    Teléfono: 232-1338, Huacho - Perú
                </div>
                <div style="float: right">Desarrollado por: Nino D. Simeón Huaccho</div>                    
                <div style="clear: both;"></div>
            </footer>
            <div id="logeado" class="ui-widget-header">         
                <b><?php echo $sesion ['perfil_usuario']; ?>,</b> <?php echo $sesion ['apellidos_nombres']; ?> 
                <nav style="margin-right: 10px;float: right;">
                    <a href="<?php echo site_url('variado/panel'); ?>">Panel de usuario</a> | 
                    <a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar Sesión</a>
                </nav>
            </div>            
            <div id="contenido_contenido">
                <p><strong>BIENVENIDO SR. BIBLIOTECARIO</strong></p>
                <p>En esta sección usted podrá agregar tanto como<strong> MATERIAL BIBLIOGRÁFICO &amp; ITEM BIBLIOGRÁFICO</strong>, la diferencia de uno del otro se especifica a continuación:</p>
                <p><strong>MATERIAL BIBLIOGRÁFICO:</strong> Es el registro del material bibliográfico(tesis, libros, CD's, tesinas, etc.) con parámetros globales(ISBN, N° de páginas, autor) dichos parametros serían repetitivos en cuanto a cantidades hablemos.</p>
                <p><strong>ITEM BIBLIOGRÁFICO:</strong> Es el registro del material bibliográfico pero en términos ESPECÍFICOS teniendo por ejemplo los siguientes parametros<em>  signatura, terminal, ciclo, etc.</em></p>
                <p><strong>¡RECUERDA!</strong> para agregar un <strong>ITEM BIBLIOGRÁFICO</strong> primero tiene que existir como <strong>MATERIAL BIBLIOGRAFICO.</strong></p>
            </div>
        </div>
    </body>
</html>