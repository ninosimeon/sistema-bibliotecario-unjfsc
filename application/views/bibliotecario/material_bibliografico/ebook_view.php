<?php $sesion = $this->session->userdata('logeado'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="description" content="Sistema Web Bibliotecario UNJFSC" />  
        <title>..::Sistema Bibliotecario::..</title>
        <link href="<?php echo base_url('public/css/temaBibliotecaBibliotecario.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('public/css/fresh_theme.css'); ?>" rel="stylesheet" type="text/css" />
        <link rel="icon" href="<?php echo base_url('public/img/favicon.ico'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/ui.jqgrid.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.ui.sunny.css'); ?>"/>        
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.alerts.css'); ?>"/>        
        <script src="<?php echo base_url('public/lib/jquery.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.datepicker-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.sunny.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/grid.locale-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.jqGrid.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.alerts.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.alphanumeric.js'); ?>" type="text/javascript" ></script>
        <script>
            $().ready(function(){
                $('button').button()
                $('.seleccion').button({
                    icons:{
                        primary: "ui-icon-arrowreturnthick-1-e"
                    }
                })      
                $('.seleccion_another').button({
                    icons:{
                        primary: "ui-icon ui-icon-circle-triangle-e"
                    }
                })
                $('#fecha_publicacion').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'yy-mm-dd',
                    option: $.datepicker.regional['es'],
                    yearRange: '1910:2020'
                });
                $('#busca_dni').click(function(){                    
                    if($('#autor').val().length==8){                        
                        $.post('<?php echo site_url('bibliografico/ebook'); ?>',{verifica_dni: $('#autor').val()},function(r){
                            if(r == 'fail'){
                                alert('¡DNI NO ENCONTRADO!');
                            }else if(r == 'pendiente'){
                                alert('PRESTAMOS/SOLICITUDES PENDIENTES ¬¬');
                            }else{
                                $('#autor').val(r);
                                $('#autor').attr('readonly',true);
                                $('.txt_ebook').attr('disabled', false); 
                                $('#btn_registrar').attr('disabled',false);
                            }
                        },'json');                        
                    }else{
                        alert('Caracteres incorrectos!');
                    }                 
                })
                $('.txt_ebook').attr('disabled', true);  
                $('#btn_registrar').attr('disabled',true);
                $('#edicion').numeric();
                $('#volumen').numeric();
                $('#nino').click(function(){
                    $('#msg_ninosimeon').dialog('open');
                });
                $('#msg_ninosimeon').dialog({
                    autoOpen: false,
                    show: 'blind',
                    hide: 'explode',
                    title: 'Créditos',
                    width: 300
                });
            });
        </script>
    </head>
    <div id="msg_ninosimeon">
        <p>
            Desarrollador: <b>Nino David Simeón Huaccho</b><br /> <br /> <b>mail:</b>
            ninosimeon@gmail.com<br /> <b>url:</b> <a
                href="http://about.me/ninosimeon">about.me/ninosimeon</a><br />
        </p>
        <h4>E.A.P. Ing. Informática "Alan Turing"</h4>
    </div>
    <body>            
        <div id="contenido" class="ui-widget">            
            <div id="titulo"><strong>AGREGAR E-BOOK</strong></div>
            <div id="cabezera"><img src="<?php echo base_url(); ?>public/img/bannerAdministrativo.png" width="800" height="67" alt="banner" /></div>
            <div id="menu" class="">
                <div>
                    <h4 class="ui-widget-header ui-corner-top">MATERIAL bibliográfico</h4>
                    <div class="ui-widget-content">                 
                        <?php echo anchor('bibliografico/agregar_material', "<button class='seleccion'>Agregar</button>"); ?><br>
                        <?php echo anchor('bibliografico/deshabilitar_material', "<button class='seleccion'>Deshabilitar</button>"); ?>
                    </div>
                    <h4 class="ui-widget-header">ITEM bibliográfico</h4>
                    <div class="ui-widget-content">
                        <?php echo anchor('bibliografico/agregar_item', "<button class='seleccion'>Agregar</button>"); ?><br>
                        <?php echo anchor('bibliografico/deshabilitar_item', "<button class='seleccion'>Deshabilitar</button>"); ?>
                    </div>                                  
                </div>
                <div id="otros_menu" class="" style="margin-top: 10px;">
                    <?php echo $menu; ?>                                 
                </div>
                <div id="terminal" class="ui-corner-all ui-widget-content">
                    TERMINAL:<br> 
                    <b><?php echo $sesion['nom_terminal']; ?></b>
                </div>
            </div>
            <footer id="pieDePagina" class="ui-state-default">
                <div style="float: left;">
                    Ciudad Universitaria - Av. Mercedes Indacochea N° 609<br />
                    Teléfono: 232-1338, Huacho - Perú
                </div>
                <div style="float: right">Desarrollado por: Nino D. Simeón Huaccho</div>                    
                <div style="clear: both;"></div>
            </footer>
            <div id="logeado" class="ui-widget-header">         
                <b><?php echo $sesion ['perfil_usuario']; ?>,</b> <?php echo $sesion ['apellidos_nombres']; ?> 
                <nav style="margin-right: 10px;float: right;">
                    <a href="<?php echo site_url('variado/panel'); ?>">Panel de usuario</a> | 
                    <a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar Sesión</a>
                </nav>
            </div>            
            <div id="contenido_contenido">
                <?php echo form_open_multipart('bibliografico/subir_ebook'); ?>
                <table class="ui-corner-all ui-widget-content" style="padding: 5px;">
                    <tr>
                        <td><b><label for="autor">AUTOR:</label></b></td>
                        <td><input type="text" name="autor" required style="width: 343px" id="autor" placeholder="DNI DEL AUTOR" autocomplete="off"/><button type="button" id="busca_dni">Buscar</button><br /></td>
                    </tr>
                    <tr>
                        <td><b><label for="titulo">TÍTULO:</label></b></td>
                        <td><input type="text" name="titulo" required autocomplete="off" style="width: 400px" placeholder="Título de la obra" class="txt_ebook"/></td>
                    </tr>
                    <tr>
                        <td><b><label for="categoria">CATEGORIA:</label></b></td>
                        <td><select name="categoria" class="txt_ebook"><?php foreach ($categoria->result() as $value) {
                    ?>
                                    <option><?php echo $value->categoria; ?></option>
                                <?php }
                                ?></select></td>
                    </tr>
                    <tr>
                        <td colspan="2"><b><label for="tematica">TEMÁTICA:</label></b></td>                           
                    </tr>
                    <tr>
                        <td colspan="2">
                            <select name="tematica"class="txt_ebook" style="font-size: 0.98em;"><?php foreach ($tema->result() as $value) {
                                    ?>
                                    <optgroup label="<?php echo $value->descripcion; ?>">
                                        <?php
                                        $query = $this->db->get_where('tematica', array('codCategoria' => $value->codCategoria));
                                        foreach ($query->result() as $value) {
                                            ?>                                        
                                            <option value="<?php echo $value->codTematica; ?>"><?php echo $value->descripcion; ?></option>
                                        <?php }
                                        ?></optgroup><?php
                                }
                                    ?></select>
                        </td>
                    </tr>
                    <tr>
                        <td><b><label for="fecha_publicacion">FECHA DE PUBLICACIÓN:</label></b></td>
                        <td><input type="date" name="fecha_publicacion" required id="fecha_publicacion" class="txt_ebook"/></td>
                    </tr>
                    <tr>
                        <td><b><label for="edicion">EDICIÓN:</label></b></td>
                        <td><input type="number" name="edicion" id="edicion" required style="width: 50px" class="txt_ebook"/></td>
                    </tr>
                    <tr>
                        <td><b><label for="volumen">VOLUMEN:</label></b></td>
                        <td><input type="number" name="volumen" id="volumen" style="width: 50px" class="txt_ebook"/></td>
                    </tr>                        
                    <tr>
                        <td><b><label for="resolucion">RESOLUCIÓN:</label></b></td>
                        <td><input type="text" autocomplete="off" name="resolucion" style="width: 400px" class="txt_ebook" placeholder="Aca va la resolución (OPCIONAL)"/></b></td>
                    </tr>
                    <tr>
                        <td><b><label for="file_pdf">SUBIR PDF:</label></b></td>
                        <td><input type="file" name="file_pdf" required class="txt_ebook" style="width: 400px;"/></td>
                    </tr>
                    <tr>
                        <td colspan="2"><button type="submit" style="width: 100%;" id="btn_registrar">Registrar</button></td>
                    </tr>                    
                </table>
                <?php echo form_close(); ?>
            </div>
        </div>
    </body>
</html>