<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>..::Sistema Bibliotecario::..</title>        
        <link href="<?php echo base_url(); ?>public/css/temaBibliotecaBibliotecario.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<?php echo base_url(); ?>public/lib/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>public/lib/jquery.alerts.js" type="text/javascript"></script>        
        <link href="<?php echo base_url(); ?>public/css/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />      
        <script src="<?php echo base_url(); ?>public/lib/jquery.alphanumeric.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/lib/jquery.ui.datepicker-es.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/lib/jquery.ui.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/lib/jquery.zclip.js" type="text/javascript"></script>
        <link href="<?php echo base_url(); ?>public/css/jquery.ui.css" rel="stylesheet" type="text/css" media="screen" />
        <script type="text/javascript">           
            $().ready(function() {
                $('#msg_completar').dialog({
                    autoOpen: false,
                    show: "blind",
                    hide: "explode",                    
                    resizable: false,
                    modal: true
                });
                $("#nino").click(function(){
                    jAlert('Promoción "Alan Turing" E.A.P. Ing. Informática<br /><br /><a href="http://about.me/ninosimeon">+ Info</a>','Nino Simeón');
                }); 
                $('.material').hide();
                $('#agregar_material').show();
                recargar_tematica();
                $('#input_tematica').change(function(){                   
                    $.post('<?php echo site_url('bibliografico/agregar_material'); ?>',{busca_tema: $('#input_tematica').val()},function(r){
                        $('.material').hide();
                        $('#selecciona_tematica').show();                          
                        for (i = 0; r.length; i++) {
                            $('#tematica_especifico').append($('<option></option>').attr('value',r[i].valor).text(r[i].texto));
                        }                       
                        
                    },'json');
                    $('#input_tematica').empty();
                    $('#input_tematica').attr('id','tematica_definido');   
                    //jAlert('<select name="tematica_especifico" id="tematica_especifico"></select>', 'SELECCIONE EL TEMA');
                });
                $('#input_isbn').keyup(function(r){
                    if (r.keyCode == 13) {
                        $.post('<?php echo site_url('bibliografico/agregar_material'); ?>',{v_isbn: $('#input_isbn').val()},function(e){
                            if (e == 'ok') {
                                $('.input_text').attr('disabled',false);
                                $('.input_btn').attr('disabled',false);
                                $('#input_isbn').attr('readonly',true);
                            }else{
                                jAlert('ISBN EXISTENTE, pasa a ITEM BIBLIOGRAFICO ¬¬','¡WARNING!',function(ev){
                                    if (ev) {
                                        document.location.href= '<?php echo site_url('bibliografico/agregar_item'); ?>';
                                    }
                                });
                            }
                        });
                    }
                });
                $('#input_edicion').numeric();
                $('#input_volumen').numeric();
                $('#btn_limpiar').click(function(){
                    document.location.href= '<?php echo site_url('bibliografico/agregar_material'); ?>';
                });                
                $('#btn_salir').click(function(){
                    document.location.href= '<?php echo site_url('bibliografico'); ?>';
                });
                $('.input_text').attr('disabled',true);
                $('.input_btn').attr('disabled',true);                
                $('#buscador').hide();  
                $('#input_fecha').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'yy-mm-dd',
                    option: $.datepicker.regional['es'],
                    yearRange: '1950:2020'
                });
            });     
            function verifica_cantidades(){
                cantidad = 0;
                if ($('#input_autores').val().length>0) {
                    cantidad = cantidad+1;
                }
                if ($('#input_titulo').val().length>0) {
                    cantidad = cantidad+1;
                }
                if ($('#input_edicion').val().length>0) {
                    cantidad = cantidad+1;
                }
                if ($('#input_editorial').val().length>0) {
                    cantidad = cantidad+1;
                }
                if ($('#input_fecha').val().length>0) {
                    cantidad = cantidad+1;
                }
                if (cantidad == 5) {
                    return true;
                } else {
                    return false;
                }
            }           
            
            
            function agrega_material(){ 
                if (verifica_cantidades()) {                                 
                    $.post('<?php echo site_url('bibliografico/agregar_material'); ?>',{new_material: 'ok',isbn: $('#input_isbn').val(),titulo: $('#input_titulo').val(),categoria: $('#input_categoria').val(),tematica: $('#tematica_definido').val(),autores: $('#input_autores').val(),editorial: $('#input_editorial').val(), fecha: $('#input_fecha').val(),edicion: $('#input_edicion').val(), volumen: $('#input_volumen').val()},function(r){
                        if (r == 'ok') {
                            var valor_isbn = $('#input_isbn').val();
                            jConfirm('SE AGREGO CON ÉXITO, ¿Deseas agregar el ítem?<br /><h1>'+valor_isbn+'</h1>', '¡CORRECTO!', function(e){
                                if (e) {                                         
                                    document.location.href= '<?php echo site_url('bibliografico/agregar_item'); ?>';
                                }                            
                            });
                            $('.input_btn').attr('disabled',true); 
                        }else{
                            jAlert('NO SE PUDO AGREGAR','¡PROBLEMAS!');
                        }
                    });
                } else {
                    $('#msg_completar').dialog('open');  
                }                
            }
            
            function agrega_material_v1(){
                $.post('<?php echo site_url('bibliografico/agregar_material'); ?>',{new_material: 'ok',isbn: $('#input_isbn').val(),titulo: $('#input_titulo').val(),categoria: $('#input_categoria').val(),tematica: $('#tematica_definido').val(),autores: $('#input_autores').val(),editorial: $('#input_editorial').val(), fecha: $('#input_fecha').val(),edicion: $('#input_edicion').val(), volumen: $('#input_volumen').val()},function(r){
                    if (r == 'ok') {
                        jConfirm('SE AGREGO CON ÉXITO, ¿Deseas agregar el ítem?', '¡CORRECTO!', function(e){
                            if (e) {                                         
                                document.location.href= '<?php echo site_url('bibliografico/agregar_item'); ?>';
                            }
                        });
                    }else{
                        jAlert('NO SE PUDO AGREGAR','¡PROBLEMAS!');
                    }
                });
            }
            function agrega_categoria(){
                jPrompt('INGRESE NUEVA CATEGORIA: ','','CATEGORIA',function(r){
                    if (r) {
                        $.post('<?php echo site_url('bibliografico/agregar_material'); ?>',{new_categoria: r},function(e){
                            if (e == 'ok') {
                                $('#input_categoria').append($('<option></option>').attr('value',r).text(r));
                            }else{
                                jAlert('CATEGORIA DUPLICADA','¡PELIGRO!');
                            }
                        });
                    }
                });                
            }
            function definido(){                
                //jAlert($('select[name="tematica_especifico"] option:selected').text());                
                $('.material').hide();
                $('#agregar_material').show();
                $('#tematica_definido').append($('<option></option>').attr('value',$('select[name="tematica_especifico"] option:selected').val()).text($('select[name="tematica_especifico"] option:selected').text()));
            }
            function recargar_tematica(){
                $('#tematica_definido').attr('id','input_tematica');
                $('#input_tematica').empty();
                $('#tematica_especifico').empty();
                $.post('<?php echo site_url('bibliografico/agregar_material'); ?>',{recarga_tema: 'ok'},function(r){  
                    $('#input_tematica').append($('<option></option>').attr('value','XXX').text('Seleccione'));
                    for (i = 0;r.length; i++) {
                        $('#input_tematica').append($('<option></option>').attr('value',r[i].valor).text(r[i].texto));
                    }
                },'json');
            }
        </script>
    </head>
    <body>    
        <div id="msg_completar" title="VERIFICA">
            <p>Verifica los campos, </p><p><h3>PROBABLEMENTE TE FALTA UNO =)</h3></p>
        </div>
        <div id="contenido">
            <div id="buscador">Material: 
                <input type="text" name="usuarioBusca" id="usuarioBusca" />
                <input type="button" name="ir" id="ir" value="Ir" onclick="buscar($('#usuarioBusca').val())" /><div id="resultado" style="color: #000">
                    <p><b>No encontrado! =(</b></p>
                </div></div>
            <div id="titulo"><strong>AGREGAR MATERIAL BIBLIOGRÁFICO</strong></div>
            <div id="cabezera"><img src="<?php echo base_url(); ?>public/img/bannerAdministrativo.png" width="800" height="67" alt="banner" /></div>
            <div id="menu">
                <table width="100%" border="0" cellspacing="3" cellpadding="0">
                    <tr>
                        <td colspan="2" align="center" bgcolor="#3D3D3D" scope="col"><strong>MATERIAL BIBLIOGRAFICO</strong></td>
                    </tr>
                    <tr>
                        <td width="11%" align="right" valign="middle"><img src="<?php echo base_url(); ?>public/img/btn.jpg" width="14" height="17" /></td>
                        <td width="89%"><a href="<?php echo site_url('bibliografico/agregar_material'); ?>">Agregar</a></td>
                    </tr>
                    <tr>
                        <td align="right"><img src="<?php echo base_url(); ?>public/img/btn.jpg" alt="" width="14" height="17" /></td>
                        <td><a href="<?php echo site_url('bibliografico/deshabilitar_material'); ?>">Deshabilitar</a></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" bgcolor="#000000"><strong>ITEM BIBLIOGRAFICO</strong></td>
                    </tr>
                    <tr>
                        <td align="right"><img src="<?php echo base_url(); ?>public/img/btn.jpg" alt="" width="14" height="17" /></td>
                        <td><a href="<?php echo site_url('bibliografico/agregar_item'); ?>">Agregar</a></td>
                    </tr>
                    <tr>
                        <td align="right"><img src="<?php echo base_url(); ?>public/img/btn.jpg" alt="" width="14" height="17" /></td>
                        <td><a href="<?php echo site_url('bibliografico/deshabilitar_item'); ?>">Deshabilitar</a></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" bgcolor="#000000"><strong>NOVEDADES</strong></td>
                    </tr>
                    <tr>
                        <td align="right"><img src="<?php echo base_url(); ?>public/img/btn.jpg" alt="" width="14" height="17" /></td>
                        <td><a href="<?php echo site_url('bibliografico/novedades_material'); ?>">Material bibliográfico</a></td>
                    </tr>
                    <tr>
                        <td align="right"><img src="<?php echo base_url(); ?>public/img/btn.jpg" alt="" width="14" height="17" /></td>
                        <td><a href="<?php echo site_url('bibliografico/novedades_item'); ?>">Item bibliográfico</a></td>
                    </tr>
                </table>
            </div>
            <div id="pieDePagina">Desarrollado por: <strong><a href="#" id="nino">Nino D. Simeón Huaccho</a></strong><a href="#"></a><br />
                Ciudad Universitaria - Av. Mercedes Indacochea N 609<br />
                Teléfono: 232-1338, Huacho - Perú<br />
            </div>
            <div id="logeado">
                <table width="100%" border="0" cellspacing="1" cellpadding="0">
                    <tr>
                        <td width="63%" scope="col"><?php
$sesion = $this->session->userdata('logeado');
echo '<b>' . $sesion['perfil_usuario'] . '</b>, ' . $sesion['apellidos_nombres'];
?></td>
                        <td width="22%" scope="col"><a href="#">Cambiar contraseña</a></td>
                        <td width="15%" scope="col"><a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar Sesión</a></td>
                    </tr>
                </table>
            </div>
            <div id="terminal">TERMINAL:<br />
                <strong><?php echo $sesion['nom_terminal']; ?></strong></div>
            <div id="contenido_contenido">
                <div id="selecciona_tematica" class="material">
                    <h3><strong>SELECCIONE LA TEMÁTICA</strong></h3>
                    <p>
                        <select name="tematica_especifico" id="tematica_especifico">
                        </select>
                    </p>
                    <p>
                        <input type="button" name="btn_confirma_tematica" id="btn_confirma_tematica" value="OK" onclick="definido()"/>
                    </p>
                </div>
                <div id="agregar_material" class="material">
                    <table width="100%" align="center" cellspacing="5">
                        <tr>
                            <td colspan="2" align="center" bgcolor="#F0F0F0"><strong>AGREGAR MATERIAL BIBLIOGRÁFICO</strong></td>
                        </tr>
                        <tr>
                            <td width="29%"><strong>ISBN/CODIGO:</strong></td>
                            <td width="71%">
                                <input name="input_isbn" type="text" id="input_isbn" style="width:50%" /> <a href="<?php echo site_url('bibliografico/add_sin_isbn'); ?>">¿No tiene ISBN?</a></td>
                        </tr>
                        <tr>
                            <td><strong>CATEGORÍA:</strong></td>
                            <td><select name="input_categoria" id="input_categoria" class="input_text">
                                    <?php foreach ($categoria->result() as $value) {
                                        ?>
                                        <option value="<?php echo $value->categoria; ?>"><?php echo $value->categoria; ?></option><?php } ?>
                                </select>
                                <input type="button" name="btn_agrega_categoria" id="btn_agrega_categoria" value="+" class="input_btn" onclick="agrega_categoria()"/></td>
                        </tr>
                        <tr>
                            <td><strong>TEMÁTICA:</strong></td>
                            <td>
                                <select name="input_tematica" id="input_tematica" class="input_text">
                                </select>
                                <input type="button" name="btn_recarga_tematica" id="btn_recarga_tematica" value="ว" class="input_btn" onclick="recargar_tematica()"/></td>
                        </tr>
                        <tr>
                            <td><strong>AUTORES:</strong></td>
                            <td>
                                <input type="text" name="input_autores" id="input_autores" style="width:100%" class="input_text"/></td>
                        </tr>
                        <tr>
                            <td><strong>TÍTULO:</strong></td>
                            <td>
                                <input type="text" name="input_titulo" id="input_titulo" style="width:100%" class="input_text"/></td>
                        </tr>
                        <tr>
                            <td><strong>EDICIÓN:</strong></td>
                            <td>
                                <input type="text" name="input_edicion" id="input_edicion" style="width:10%" class="input_text"/></td>
                        </tr>
                        <tr>
                            <td><strong>VOLUMEN:</strong></td>
                            <td><input type="text" name="input_volumen" id="input_volumen" style="width:10%" class="input_text"/></td>
                        </tr>
                        <tr>
                            <td><strong>EDITORIAL/EMPRESAS:</strong></td>
                            <td><input type="text" name="input_editorial" id="input_editorial" style="width:100%" class="input_text"/></td>
                        </tr>
                        <tr>
                            <td><strong>FECHA DE PUBLICACIÓN:</strong></td>
                            <td>
                                <input name="input_fecha" type="text" class="input_text" id="input_fecha" style="width:50%" readonly="readonly"/></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center"><input type="button" name="btn_agregar" id="btn_agregar" value="AGREGAR MATERIAL" class="input_btn" onclick="agrega_material()"/>
                                <input type="button" name="btn_limpiar" id="btn_limpiar" value="LIMPIAR" class="input_btn"/>
                                <input type="button" name="btn_salir" id="btn_salir" value="SALIR" class="input_btn"/></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>