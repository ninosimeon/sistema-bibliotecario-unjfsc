<?php $sesion = $this->session->userdata('logeado'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="description" content="Sistema Web Bibliotecario UNJFSC" />  
        <title>..::Sistema Bibliotecario::..</title>
        <link href="<?php echo base_url('public/css/temaBibliotecaBibliotecario.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('public/css/fresh_theme.css'); ?>" rel="stylesheet" type="text/css" />
        <link rel="icon" href="<?php echo base_url('public/img/favicon.ico'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/ui.jqgrid.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.ui.sunny.css'); ?>"/>        
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.alerts.css'); ?>"/>        
        <script src="<?php echo base_url('public/lib/jquery.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.datepicker-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.sunny.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/grid.locale-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.jqGrid.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.alerts.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.alphanumeric.js'); ?>" type="text/javascript" ></script>
        <script>           
            $().ready(function() {     
                $('button').button()
                $('.seleccion').button({
                    icons:{
                        primary: "ui-icon-arrowreturnthick-1-e"
                    }
                })      
                $('.seleccion_another').button({
                    icons:{
                        primary: "ui-icon ui-icon-circle-triangle-e"
                    }
                })                       
                $('#buscador').hide();
                $('.contenido').hide();
                $('#contenido_verificar').show();
                $('#btn_inicio').click(function(){
                    document.location.href= '<?php echo site_url('bibliografico'); ?>';
                });
            });              
            function inicia_buscador(){
                document.location.href= '<?php echo site_url('bibliografico/add_sin_isbn'); ?>';
            }
            function regresar(){
                document.location.href= '<?php echo site_url('bibliografico/agregar_item'); ?>';
            }
            function agregar_item(){
                $.post('<?php echo site_url('bibliografico/agregar_item'); ?>',{isbn: $('#output_isbn').val(),soporte: $('#input_soporte').val(), prestamo: $('#input_prestamo').val(),origen: $('#input_origen').val(),observaciones: $('#input_observacion').val(), resolucion: $('#input_resolucion').val()},function(r){
                    if (r) {
                        $('#output_signatura').val(r.signatura);
                        $('#output_fecha').val(r.fechaIngreso);
                        $('#output_hora').val(r.horaIngreso);
                        $('#btn_agregar').attr('disabled',true);
                    }else{
                        jAlert('Problemas al agregar el ITEM', 'PROBLEMAS :(', function(e){
                            if (e) {
                                regresar();
                            }
                        });
                    }
                },'json');
            }
            function busca_isbn(){
                jPrompt('TIPEA EL ISBN:', 'tu isbn aqui', 'BUSCADOR ISBN', function(r){
                    if (r) {                        
                        $.post('<?php echo site_url('bibliografico/agregar_item'); ?>',{busca_isbn: r}, function(e){
                            if (e) {
                                //VERIFICAR SI ES LO QUE BUSCASTE
                                jConfirm('AUTOR: '+e.autor+'<br />TITULO: '+e.titulo+'<br />N° EDICION: '+e.edicion,'VERIFICA',function(res){
                                    if (res) {
                                        //aca salta a la vista de agregar
                                        $('#output_isbn').val(r);
                                        $('.contenido').hide();
                                        $('#contenido_agregar').show();
                                    }else{
                                        busca_isbn();
                                    }
                                });                                
                            } else {                                
                                jAlert('ISBN NO EXISTENTE, INTENTA DE NUEVO','¡WARNING!',function(ev){
                                    if (ev) {
                                        busca_isbn();
                                    }
                                });
                            }
                        },'json');
                    }
                });
            }
        </script>
    </head>
    <body>    
        <div id="contenido" class="ui-widget">
            <div id="buscador">Material: 
                <input type="text" name="usuarioBusca" id="usuarioBusca" />
                <input type="button" name="ir" id="ir" value="Ir" onclick="buscar($('#usuarioBusca').val())" /><div id="resultado" style="color: #000">
                    <p><b>No encontrado! =(</b></p>
                </div></div>
            <div id="titulo"><strong>AGREGAR ITEM BIBLIOGRÁFICO</strong></div>
            <div id="cabezera"><img src="<?php echo base_url(); ?>public/img/bannerAdministrativo.png" width="800" height="67" alt="banner" /></div>
            <div id="menu" class="">
                <div>
                    <h4 class="ui-widget-header ui-corner-top">MATERIAL bibliográfico</h4>
                    <div class="ui-widget-content">                 
                        <?php echo anchor('bibliografico/agregar_material', "<button class='seleccion'>Agregar</button>"); ?><br>
                        <?php echo anchor('bibliografico/deshabilitar_material', "<button class='seleccion'>Deshabilitar</button>"); ?>
                    </div>
                    <h4 class="ui-widget-header">ITEM bibliográfico</h4>
                    <div class="ui-widget-content ui-corner-bottom">
                        <?php echo anchor('bibliografico/agregar_item', "<button class='seleccion'>Agregar</button>"); ?><br>
                        <?php echo anchor('bibliografico/deshabilitar_item', "<button class='seleccion'>Deshabilitar</button>"); ?>
                    </div>                                  
                </div>
                <div id="otros_menu" class="" style="margin-top: 10px;">
                    <?php echo $menu; ?>                                 
                </div>
                <div id="terminal" class="ui-corner-all ui-widget-content">
                    TERMINAL:<br> 
                    <b><?php echo $sesion['nom_terminal']; ?></b>
                </div>
            </div>
            <footer id="pieDePagina" class="ui-state-default">
                <div style="float: left;">
                    Ciudad Universitaria - Av. Mercedes Indacochea N° 609<br />
                    Teléfono: 232-1338, Huacho - Perú
                </div>
                <div style="float: right">Desarrollado por: Nino D. Simeón Huaccho</div>                    
                <div style="clear: both;"></div>
            </footer>
            <div id="logeado" class="ui-widget-header">         
                <b><?php echo $sesion ['perfil_usuario']; ?>,</b> <?php echo $sesion ['apellidos_nombres']; ?> 
                <nav style="margin-right: 10px;float: right;">
                    <a href="<?php echo site_url('variado/panel'); ?>">Panel de usuario</a> | 
                    <a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar Sesión</a>
                </nav>
            </div>
            <div id="terminal">TERMINAL:<br />
                <strong><?php echo $sesion['nom_terminal']; ?></strong></div>
            <div id="contenido_contenido"><div id="contenido_verificar" class="contenido"><br />
                    <table class="ui-widget-content ui-corner-bottom" align="center">
                        <thead class="ui-widget-header">
                            <tr>
                                <td colspan="2" style="text-align: center;"><h1><strong>VERIFICAR EXISTENCIA</strong></h1></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td width="54%" align="center"><input type="button" name="btn_isbn" id="btn_isbn" value="ISBN" onclick="busca_isbn()" style="width:80%;height:100px"/></td>
                                <td width="46%" align="center"><input type="button" name="btn_buscador" id="btn_buscador" value="BUSCADOR" onclick="inicia_buscador()" style="width:80%;height:100px"/></td>
                            </tr>
                        </tbody>                        
                    </table>
                </div><div id="contenido_busqueda" class="contenido"></div>
                <div id="contenido_agregar" class="contenido">
                    <table width="80%" align="center" class="ui-widget-content" style="padding: 5px;">
                        <thead class="ui-widget-header">
                            <tr>
                                <td colspan="2" align="center"><h2><b>AGREGAR ITEM</b></h2></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td width="38%"><strong>ISBN:</strong></td>
                                <td width="62%"><input name="output_isbn" type="text" id="output_isbn" readonly="readonly" style="width:50%"/></td>
                            </tr>
                            <tr>
                                <td><strong>SIGNATURA:</strong></td>
                                <td><input name="output_signatura" type="text" id="output_signatura" readonly="readonly" style="width:80%"/></td>
                            </tr>
                            <tr>
                                <td><strong>RESOLUCIÓN: </strong></td>
                                <td><input type="text" name="input_resolucion" id="input_resolucion" style="width:80%"/></td></tr>
                            <tr>
                                <td><strong>SOPORTE:</strong></td>
                                <td><select name="input_soporte" id="input_soporte">
                                        <option value="PAPEL">PAPEL</option>
                              <!--          <?php foreach ($soporte->result() as $value) {
                                            ?>
                                            <option value="<?php echo $value->soporte; ?>"><?php echo $value->soporte; ?></option><?php } ?>
                                    --></select></td>
                            </tr>
                            <tr>
                                <td><strong>FECHA INGRESO:</strong></td>
                                <td><input name="output_fecha" type="text" id="output_fecha" readonly="readonly" style="width:50%"/></td>
                            </tr>
                            <tr>
                                <td><strong>HORA INGRESO:</strong></td>
                                <td><input name="output_hora" type="text" id="output_hora" readonly="readonly" style="width:50%"/></td>
                            </tr>
                            <tr>
                                <td><strong>MODALIDAD PRESTAMO:</strong></td>
                                <td><select name="input_prestamo" id="input_prestamo">
                                        <option value="INTERNO">PRESTAMO INTERNO</option>
                                        <!--<?php foreach ($prestamo->result() as $value) {
                                            ?>
                                            <option value="<?php echo $value->codigo; ?>"><?php echo $value->nombre; ?></option><?php } ?>
                                    --></select></td>
                            </tr>
                            <tr>
                                <td><strong>ORIGEN:</strong></td>
                                <td><select name="input_origen" id="input_origen">
                                        <?php foreach ($origen->result() as $value) {
                                            ?>
                                            <option value="<?php echo $value->origen; ?>"><?php echo $value->origen; ?></option><?php } ?>
                                    </select></td>
                            </tr>
                            <tr>
                                <td><strong>OBSERVACIONES:</strong></td>
                                <td><textarea name="input_observacion" id="input_observacion" cols="45" rows="5" style="width:98%"></textarea></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center"><input type="button" name="btn_agregar" id="btn_agregar" value="AGREGAR" onclick="agregar_item()"/>
                                    <input type="button" name="btn_regresar" id="btn_regresar" value="REGRESAR" onclick="regresar()"/>
                                    <input type="button" name="btn_inicio" id="btn_inicio" value="INICIO" /></td>
                            </tr>
                        </tbody>                        
                    </table>
                </div></div>
        </div>
    </body>
</html>