<!DOCTYPE html>
<html lang="es">
    <head>       
        <meta charset="utf-8" />
        <meta name="description" content="Sistema Web Bibliotecario UNJFSC" />  
        <title>..::Sistema Bibliotecario::..</title>
        <link href="<?php echo base_url('public/css/estilosBusqueda.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('public/css/fresh_theme.css'); ?>" rel="stylesheet" type="text/css" />
        <link rel="icon" href="<?php echo base_url('public/img/favicon.ico'); ?>"/>        
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.ui.sunny.css'); ?>"/>                
        <script src="<?php echo base_url('public/lib/jquery.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.datepicker-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.sunny.js'); ?>" type="text/javascript" ></script>        
        <script>
            $().ready(function(){             
                $('#creditos').click(function(){
                    $('#msg_ninosimeon').dialog('open');
                });                
                $('#msg_ninosimeon').dialog({
                    autoOpen: false,
                    show: 'blind',
                    hide: 'explode',
                    title: 'Créditos',
                    width: 300
                });               
            });                                 
        </script>
        <style>
            #contenedor{
                background-color: #F8F8F8;
            }
            article img{
                float: right;
                clear: both;
            }
            h1{
                font-size: 3.5em;
                text-align: center;  
                padding-top: 70px;
            }
            article p{
                margin-left: 25px;
                margin-right: 25px;
                margin-bottom: 25px;
                height: 200px;
                font-size: 1.5em;                                
            }         

        </style>
    </head>
    <div id="msg_ninosimeon">
        <p>
            Desarrollador: <b>Nino David Simeón Huaccho</b><br /> <br /> <b>mail:</b>
            ninosimeon@gmail.com<br /> <b>url:</b> <a
                href="http://about.me/ninosimeon">about.me/ninosimeon</a><br />
        </p>
        <h4>E.A.P. Ing. Informática "Alan Turing"</h4>
    </div>
    <body>
        <div id="contenedor" class="ui-widget">
            <header>
                <img
                    src="<?php echo base_url(); ?>public/img/banner_optimizado/bannerReporte_r1_c1.jpg" width="221" height="67" alt="bannerReporte_r1_c1" /><img
                    src="<?php echo base_url(); ?>public/img/banner_optimizado/bannerReporte_r1_c2.jpg" width="297" height="67" alt="bannerReporte_r1_c2" /><img
                    src="<?php echo base_url(); ?>public/img/banner_optimizado/bannerReporte_r1_c3.jpg" width="432" height="67" alt="bannerReporte_r1_c3" />
            </header>
            <section>
                <article>                    
                    <img src="<?php echo base_url(); ?>public/img/error_404.gif" />
                    <h1>¡ERROR 404!</h1>
                    <p>
                        UPSS! Página <b>NO ENCONTRADA</b>, si llegaste aquí por equivocación notificalo al administrador.                        
                    </p>                  
                                        
                </article>
                <footer id="pieDePagina" class="ui-state-default">
                    <div style="float: left;">
                        Ciudad Universitaria - Av. Mercedes Indacochea N° 609<br />
                        Teléfono: 232-1338, Huacho - Perú
                    </div>
                    <div style="float: right">Desarrollado por: Nino D. Simeón Huaccho</div>                    
                    <div style="clear: both;"></div>
                </footer>
            </section>
        </div>
    </body>
</html>
