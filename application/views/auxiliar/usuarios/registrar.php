<?php $sesion = $this->session->userdata ( 'logeado' ); ?>
<!DOCTYPE html">
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="description" content="Sistema Web Bibliotecario UNJFSC" />  
        <title>..::Sistema Bibliotecario::..</title>
        <link href="<?php echo base_url('public/css/temaBibliotecaAuxiliar.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('public/css/fresh_theme.css'); ?>" rel="stylesheet" type="text/css" />
        <link rel="icon" href="<?php echo base_url('public/img/favicon.ico'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/ui.jqgrid.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.ui.sunny.css'); ?>"/>        
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.alerts.css'); ?>"/>        
        <script src="<?php echo base_url('public/lib/jquery.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.datepicker-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.sunny.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/grid.locale-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.jqGrid.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.alerts.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.alphanumeric.js'); ?>" type="text/javascript" ></script>
        <script>
            function convoca_pass(){                                        
                jPrompt('<b>Cuenta #:</b><h2>'+$('#input_cuenta').val()+'</h2><b>APELLIDOS: </b>'+$('#input_apellidos').val()+'<p><b>NOMBRES: </b>'+$('#input_nombres').val()+'</p>','TU CLAVE AQUI','REGISTRO DE USUARIOS', function(r){
                    if (r) {
                        $('#input_clave').val(r);
                        $('#btn_nuevo').attr('onclick','inicia_registro()');
                    }
                });
            }
            function inicia_registro(){
                $.post('<?php echo site_url('usuarios/registrar'); ?>',{tipoDoc: $('#input_doc').val(),inicia_registro: $('#input_numero').val(), carneuniv: $('#input_carne').val(), apellidos: $('#input_apellidos').val(),nombres: $('#input_nombres').val(),fechaNac: $('#input_nacimiento').val(),sexo:$('#input_sexo').val(),dirFisica: $('#input_direccion').val(),email: $('#input_mail').val(),telefono: $('#input_telefono').val(),clave: $('#input_clave').val(), perfil_usuario: $('#input_tipo option:selected').val()},function(data){
                    if (data == 'ok') {
                        jAlert('Registrado correctamente','NINO SIMEON DICE:',function(r){
                            if (r) {
                                document.location.href= '<?php echo site_url('usuarios'); ?>';
                            }
                        });                        
                    }  
                    else{
                        jAlert('No se pudo registrar =(');
                    }
                });
                
                
            }
            $().ready(function() {	
                $('button').button()
                $('.seleccion').button({
                    icons:{
                            primary: "ui-icon-arrowreturnthick-1-e"
                    }
                })      
                $('.seleccion_another').button({
                    icons:{
                        primary: "ui-icon ui-icon-circle-triangle-e"
                    }
                })
                $('#btn_menu').click(function(){
                    document.location.href= '<?php echo site_url('usuarios'); ?>';
                });
                $('#input_nacimiento').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'yy-mm-dd',
                    option: $.datepicker.regional['es'],
                    yearRange: '1950:2020'
                });
                $('.input_llenable').attr('disabled',true);
                $('#btn_nuevo').attr('disabled',true);                
                $('#input_tipo').attr('disabled',true);
                $('#input_carne').numeric();
                $('#input_numero').attr('disabled',true);                 
                jAlert('Seleccione el tipo de DOC','¡ALERTA!');
                $('#input_doc').change(function(){
                    $('#input_numero').val('');
                    switch ($('#input_doc').val()) {
                        case 'DNI':
                            $('#input_numero').attr('disabled',false);
                            $('#input_numero').numeric();
                            $('#input_numero').attr('maxlength',8);
                            break;
                        default: $('#btn_nuevo').attr('disabled',true);                
                            $('.input_llenable').attr('disabled',true);
                            $('#input_tipo').attr('disabled',true);
                            break;
                    }

                });
                $('#input_numero').keyup(function(){                                    
                    $('#input_cuenta').val($('#input_numero').val());
                    $('#btn_nuevo').attr('disabled',true);                
                    $('.input_llenable').attr('disabled',true);
                    $('#input_tipo').attr('disabled',true);
                    $('#input_numero').css('background-color','#FF0000');                    
                    if ($('#input_doc').val() == 'DNI') {
                        /*$('#input_numero').numeric();
                        $('#input_numero').attr('maxlength',8);*/
                        if ($('#input_numero').val().length == 8) {
                            $.post('<?php echo site_url('usuarios/registrar'); ?>',{consulta_documento: $('#input_numero').val()},function(data){
                                if (data == 'existe') {
                                    jAlert('El DNI ya esta registrado ¬¬','¡ALERTA!');
                                }else{
                                    $('#input_numero').css('background-color','#99FF66');
                                    $('.input_llenable').attr('disabled',false);
                                    $('#btn_nuevo').attr('disabled',false);
                                    $('#input_tipo').attr('disabled', false);
                                }
                            });                          
                        }
                    } else {
                        jAlert('No es DNI -_-', 'OCURRIO PROBLEMAS');
                    }   
                });
            });
        </script>
    </head>

    <body>
        <div id="contenido" class="ui-widget">
            <div id="titulo"><strong>NUEVO USUARIO</strong></div>
            <div id="cabezera"><img src="<?php echo base_url(); ?>public/img/bannerAdministrativo.png" width="800" height="67" alt="banner" /></div>
            <div id="menu" class="">
                <div>
                    <h4 class="ui-widget-header ui-corner-top">USUARIOS</h4>
                    <div class="ui-widget-content">                 
                        <?php echo anchor('usuarios/registrar',"<button class='seleccion'>Registrar</button>"); ?><br>
                        <?php echo anchor('usuarios/actualizar',"<button class='seleccion'>Actualizar</button>"); ?>
                    </div>
                    <h4 class="ui-widget-header">TRANSACCIONES</h4>
                    <div class="ui-widget-content">
                        <?php echo anchor('prestamo_reserva/solicitud',"<button class='seleccion'>Solicitud prestamo</button>"); ?><br>
                        <?php echo anchor('prestamo_reserva/prestamo',"<button class='seleccion'>Lista prestamo</button>"); ?><br>
                        <?php echo anchor('prestamo_reserva/devolucion',"<button class='seleccion'>Lista devolución</button>"); ?>
                    </div>
                    <h4 class="ui-widget-header">TRANSACCIONES</h4>
                    <div class="ui-widget-content ui-corner-bottom">
                        <?php echo anchor('material_auxiliar/sinconfirmar',"<button class='seleccion'>Sin confirmar</button>"); ?>                        
                    </div>              
                </div>
                <div id="otros_menu" class="" style="margin-top: 10px;">
                    <?php echo $menu; ?>                                 
                </div>
                <div id="terminal" class="ui-corner-all ui-widget-content">
                    TERMINAL:<br> 
                    <b><?php echo $sesion['nom_terminal']; ?></b>
                </div>
            </div>
            <footer id="pieDePagina" class="ui-state-default">
                <div style="float: left;">
                    Ciudad Universitaria - Av. Mercedes Indacochea N° 609<br />
                    Teléfono: 232-1338, Huacho - Perú
                </div>
                <div style="float: right">Desarrollado por: Nino D. Simeón Huaccho</div>                    
                <div style="clear: both;"></div>
            </footer>
            <div id="logeado" class="ui-widget-header">         
                <b><?php echo $sesion ['perfil_usuario'] ; ?>,</b> <?php echo $sesion ['apellidos_nombres']; ?> 
                <nav style="margin-right: 10px;float: right;">
                    <a href="<?php echo site_url('variado/panel'); ?>">Panel de usuario</a> | 
                    <a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar Sesión</a>
                </nav>
            </div>            
            <div id="contenido_contenido">                
                <table border="0" cellspacing="4" cellpadding="0" style="font-size: 1.05em;">
                    <tr>
                        <td align="center" bgcolor="#F0F0F0" scope="col">
                            <strong>TIPO DE USUARIO:</strong>
                        </td>
                        <td align="center" bgcolor="#F0F0F0" scope="col">
                            <strong>DATOS PERSONALES DEL USUARIO:</strong>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" bgcolor="#99FF66">
                            <p>&nbsp;</p>
                            <p><em>*SELECCIONE UN PERFIL PARA EL USUARIO</em>:</p>
                            <p>
                                <select name="input_tipo"  size="<?php echo $tipoPerfil->num_rows(); ?>" id="input_tipo" style="width:80%">
                                    <?php foreach ($tipoPerfil->result() as $value) {
                                        ?>
                                        <option value="<?php echo $value->codigo; ?>"><?php echo $value->nombre; ?></option><?php } ?>
                                </select>
                            </p>
                            <p>&nbsp;</p></td>
                        <td>
                            <table border="0" cellspacing="0" cellpadding="2" style="font-size: 1em;">
                                <tr>
                                    <td width="43%" bgcolor="#66CCFF" scope="col"><strong>TIPO DE DOC:</strong></td>
                                    <th width="57%" align="left" bgcolor="#66CCFF" scope="col"><select name="input_doc" id="input_doc">
                                            <?php foreach ($tipoDoc->result() as $value) {
                                                ?>
                                                <option><?php echo $value->tipoDoc; ?></option><?php } ?>
                                        </select>
                                        *</th>
                                </tr>
                                <tr>
                                    <td bgcolor="#66CCFF"><strong>NÚMERO:</strong></td>
                                    <td align="left" bgcolor="#66CCFF"><input name="input_numero" type="text" id="input_numero" style="width:90%"  />
                                        *</td>
                                </tr>
                                <tr>
                                    <td bgcolor="#66CCFF"><strong>CARNÉ UNIV:</strong></td>
                                    <td align="left" bgcolor="#66CCFF"><input  name="input_carne" type="text" class="input_llenable" id="input_carne" style="width:90%" maxlength="10"/></td>
                                </tr>
                                <tr>
                                    <td bgcolor="#66CCFF"><strong>APELLIDOS:</strong></td>
                                    <td align="left" bgcolor="#66CCFF"><input name="input_apellidos" type="text" id="input_apellidos" style="width:90%" class="input_llenable"/>
                                        *</td>
                                </tr>
                                <tr>
                                    <td bgcolor="#66CCFF"><strong>NOMBRES:</strong></td>
                                    <td align="left" bgcolor="#66CCFF"><input name="input_nombres" type="text" id="input_nombres" style="width:90%" class="input_llenable"/>
                                        *</td>
                                </tr>
                                <tr>
                                    <td bgcolor="#66CCFF"><strong>FECHA NACIMIENTO:</strong></td>
                                    <td align="left" bgcolor="#66CCFF"><input name="input_nacimiento" type="text" id="input_nacimiento" style="width:90%" class="input_llenable" />
                                        *</td>
                                </tr>
                                <tr>
                                    <td>SEXO:</td>
                                    <td><input type="radio" name="input_sexo" id="input_sexo" value="M" />
                                        M
                                        <input type="radio" name="input_sexo" id="s" value="F" />
                                        F </td>
                                </tr>
                                <tr>
                                    <td>DIR. FÍSICA:</td>
                                    <td><input name="input_direccion" type="text" id="input_direccion" style="width:90%" class="input_llenable"/></td>
                                </tr>
                                <tr>
                                    <td>TELÉFONO:</td>
                                    <td><input name="input_telefono" type="text" id="input_telefono" style="width:90%" class="input_llenable"/></td>
                                </tr>
                                <tr>
                                    <td>E-MAIL:</td>
                                    <td><input name="input_mail" type="text" id="input_mail" style="width:90%" class="input_llenable"/></td>
                                </tr>
                                <tr>
                                    <td bgcolor="#FF0000"><strong>Cuenta generada:</strong></td>
                                    <td bgcolor="#FF0000"><input name="input_cuenta" type="text" disabled="disabled" id="input_cuenta" style="width:90%" readonly="readonly" class="input_llenable"/></td>
                                </tr>
                                <tr>
                                    <td bgcolor="#FF0000"><strong>Clave generada:</strong></td>
                                    <td bgcolor="#FF0000"><input name="input_clave" type="password" disabled="disabled" id="input_clave" style="width:90%" readonly="readonly" class="input_llenable"/></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><input type="submit" name="btn_menu" id="btn_menu" value="&lt;&lt; Regresar a menú principal" />
                            <input type="submit" name="btn_nuevo" id="btn_nuevo" value="&lt;REGISTRAR&gt;" style="font-family:Tahoma, Geneva, sans-serif; font-style:oblique;" onclick="convoca_pass()" /></td>
                    </tr>
                </table>
            </div>
        </div>
    </body>
</html>