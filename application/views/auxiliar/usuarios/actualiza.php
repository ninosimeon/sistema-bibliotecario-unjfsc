<?php $sesion = $this->session->userdata ( 'logeado' ); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="description" content="Sistema Web Bibliotecario UNJFSC" />  
        <title>..::Sistema Bibliotecario::..</title>
        <link href="<?php echo base_url('public/css/temaBibliotecaAuxiliar.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('public/css/fresh_theme.css'); ?>" rel="stylesheet" type="text/css" />
        <link rel="icon" href="<?php echo base_url('public/img/favicon.ico'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/ui.jqgrid.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.ui.sunny.css'); ?>"/>        
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.alerts.css'); ?>"/>        
        <script src="<?php echo base_url('public/lib/jquery.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.datepicker-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.sunny.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/grid.locale-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.jqGrid.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.alerts.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.alphanumeric.js'); ?>" type="text/javascript" ></script>
        <script>           
            $().ready(function() {
                $('button').button()
                $('.seleccion').button({
                    icons:{
                            primary: "ui-icon-arrowreturnthick-1-e"
                    }
                })      
                $('.seleccion_another').button({
                    icons:{
                        primary: "ui-icon ui-icon-circle-triangle-e"
                    }
                })
                $("#nino").click(function(){
                    jAlert('Promoción "Alan Turing" E.A.P. Ing. Informática<br /><br /><a href="http://about.me/ninosimeon">+ Info</a>','Nino Simeón');
                }); 
                $('#resultado').hide();
                $('#respuesta_contenido').hide();
                $('#input_carne').numeric();    
                $('#input_telefono').numeric();    
                $('#btn_regresa').click(function(){
                    document.location.href= '<?php echo site_url('usuarios'); ?>';
                });            
            });
            function buscar(valor){
                $.post('<?php echo site_url('usuarios/actualizar'); ?>',{consulta: valor},function(r){
                    if(r.estado == 'fail'){
                        $('#respuesta_contenido').hide();
                        $('#resultado').show();
                    }else{
                        $('#resultado').hide();
                        $('#respuesta_contenido').show();
                        $('#output_dni').val(r.dni);
                        $('#output_apellidos').val(r.apellidos);
                        $('#output_nombres').val(r.nombres);
                        $('#input_carne').val(r.carne);
                        $('#input_sexo').val(r.sexo).attr('checked','checked');
                        $('#input_direccion').val(r.direccion);
                        $('#input_mail').val(r.mail);
                        $('#input_telefono').val(r.telefono);
                        $('#output_perfil').val(r.perfil);
                        $('#input_terminal').val(r.codTerminal);                        
                    }
                },'json');
            } 
            function update(){
                $.post('<?php echo site_url('usuarios/actualizar'); ?>',{update:$('#output_dni').val(),carne: $('#input_carne').val(),sexo: $('#input_sexo').val(),direccion: $('#input_direccion').val(),mail: $('#input_mail').val(),telefono: $('#input_telefono').val(),terminal:$('#input_terminal').val(),clave: $('#input_clave').val()},function(r){
                    if(r=='fail'){
                        jAlert('Problemas','UPS!');                        
                    }else{
                        jAlert('Actualizado correctamente','FELICITACIONES',function(r){if(r){document.location.href= '<?php echo site_url('usuarios/actualizar'); ?>';}});
                        
                    }
                });
            }
        </script>
    </head>
    <body>    
        <div id="contenido" class="ui-widget">
            <div id="buscador" class="ui-widget-content ui-corner-all">Usuario: 
                <input type="text" name="usuarioBusca" id="usuarioBusca" placeholder="45454545" />
                <button type="button" name="ir" id="ir" onclick="buscar($('#usuarioBusca').val())">Ir</button>
                <div id="resultado" style="color: #000">
                    <p><b>No encontrado! =(</b></p>
                </div></div>
            <div id="titulo"><strong>ACTUALIZAR USUARIO</strong></div>
            <div id="cabezera"><img src="<?php echo base_url(); ?>public/img/bannerAdministrativo.png" width="800" height="67" alt="banner" /></div>
            <div id="menu" class="">
                <div>
                    <h4 class="ui-widget-header ui-corner-top">USUARIOS</h4>
                    <div class="ui-widget-content">                 
                        <?php echo anchor('usuarios/registrar',"<button class='seleccion'>Registrar</button>"); ?><br>
                        <?php echo anchor('usuarios/actualizar',"<button class='seleccion'>Actualizar</button>"); ?>
                    </div>
                    <h4 class="ui-widget-header">TRANSACCIONES</h4>
                    <div class="ui-widget-content">
                        <?php echo anchor('prestamo_reserva/solicitud',"<button class='seleccion'>Solicitud prestamo</button>"); ?><br>
                        <?php echo anchor('prestamo_reserva/prestamo',"<button class='seleccion'>Lista prestamo</button>"); ?><br>
                        <?php echo anchor('prestamo_reserva/devolucion',"<button class='seleccion'>Lista devolución</button>"); ?>
                    </div>
                    <h4 class="ui-widget-header">TRANSACCIONES</h4>
                    <div class="ui-widget-content ui-corner-bottom">
                        <?php echo anchor('material_auxiliar/sinconfirmar',"<button class='seleccion'>Sin confirmar</button>"); ?>                        
                    </div>              
                </div>
                <div id="otros_menu" class="" style="margin-top: 10px;">
                    <?php echo $menu; ?>                                 
                </div>
                <div id="terminal" class="ui-corner-all ui-widget-content">
                    TERMINAL:<br> 
                    <b><?php echo $sesion['nom_terminal']; ?></b>
                </div>
            </div>
            <footer id="pieDePagina" class="ui-state-default">
                <div style="float: left;">
                    Ciudad Universitaria - Av. Mercedes Indacochea N° 609<br />
                    Teléfono: 232-1338, Huacho - Perú
                </div>
                <div style="float: right">Desarrollado por: Nino D. Simeón Huaccho</div>                    
                <div style="clear: both;"></div>
            </footer>
            <div id="logeado" class="ui-widget-header">         
                <b><?php echo $sesion ['perfil_usuario'] ; ?>,</b> <?php echo $sesion ['apellidos_nombres']; ?> 
                <nav style="margin-right: 10px;float: right;">
                    <a href="<?php echo site_url('variado/panel'); ?>">Panel de usuario</a> | 
                    <a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar Sesión</a>
                </nav>
            </div>
            <div id="terminal">TERMINAL:<br />
                <strong><?php echo $sesion['nom_terminal']; ?></strong></div>
            <div id="contenido_contenido"><div id="respuesta_contenido">
                    <table width="75%" border="0" align="center" cellpadding="5" cellspacing="0">
                        <tr>
                            <td width="36%" bgcolor="#FF3333" scope="col"><strong>DNI:</strong></td>
                            <td width="64%" bgcolor="#FF3333" scope="col"><input name="output_dni" style="width:95%" type="text" id="output_dni" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <td bgcolor="#FF3333"><strong>APELLIDOS:</strong></td>
                            <td bgcolor="#FF3333"><input name="output_apellidos" type="text" style="width:95%" id="output_apellidos" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <td bgcolor="#FF3333"><strong>NOMBRES:</strong></td>
                            <td bgcolor="#FF3333"><input name="output_nombres" type="text" style="width:95%" id="output_nombres" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <td bgcolor="#FF3333"><strong>PERFIL:</strong></td>
                            <td bgcolor="#FF3333"><input name="output_perfil" type="text" style="width:95%" id="output_perfil" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <td bgcolor="#66FF00"><strong>CARNE UNIV:</strong></td>
                            <td bgcolor="#66FF00"><input name="input_carne" type="text" id="input_carne" style="width:95%" maxlength="10" /></td>
                        </tr>
                        <tr>
                            <td bgcolor="#66FF00"><strong>SEXO:</strong></td>
                            <td bgcolor="#66FF00"><input name="input_sexo" type="radio" id="input_sexo" value="M" />
                                M
                                <input name="input_sexo" type="radio" id="input_sexo" value="F" />
                                F</td>
                        </tr>
                        <tr>
                            <td bgcolor="#66FF00"><strong>DIR. FÍSICA:</strong></td>
                            <td bgcolor="#66FF00"><input type="text" name="input_direccion" style="width:95%" id="input_direccion" /></td>
                        </tr>
                        <tr>
                            <td bgcolor="#66FF00"><strong>E-MAIL:</strong></td>
                            <td bgcolor="#66FF00"><input type="text" name="input_mail" style="width:95%" id="input_mail" /></td>
                        </tr>
                        <tr>
                            <td bgcolor="#66FF00"><strong>TELÉFONO:</strong></td>
                            <td bgcolor="#66FF00"><input type="text" name="input_telefono"  style="width:95%" id="input_telefono" /></td>
                        </tr>
                        <tr>
                            <td bgcolor="#66FF00"><strong>TERMINAL:</strong></td>
                            <td bgcolor="#66FF00"><select name="input_terminal" style="width:95%" id="input_terminal">
                                    <?php foreach ($terminal->result() as $value) {
                                        ?>
                                        <option value="<?php echo $value->codTerminal; ?>"><?php echo $value->nomTerminal; ?></option><?php } ?></select></td>
                        </tr>
                        <tr>
                            <td bgcolor="#6666CC"><strong>NUEVA CLAVE:</strong></td>
                            <td bgcolor="#6666CC"><input type="password" name="input_clave" style="width:95%" id="input_clave" /></td>
                        </tr>
                        <tr>
                            <td><input type="button" name="btn_regresa" style="width:100%" id="btn_regresa" value="&lt;&lt;REGRESA MENÚ" /></td>
                            <td><input type="button" name="btn_actualiza" style="width:100%" id="btn_actualiza" value="ACTUALIZAR" onclick="update()"/></td>
                        </tr>
                    </table></div>
            </div>
        </div>
    </body>
</html>