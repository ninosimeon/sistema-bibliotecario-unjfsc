<?php $sesion = $this->session->userdata ( 'logeado' ); ?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="description" content="Sistema Web Bibliotecario UNJFSC" />	
	<title>..::Sistema Bibliotecario::..</title>
	<link 	href="<?php echo base_url('public/css/temaBibliotecaAuxiliar.css'); ?>" rel="stylesheet" type="text/css" />
  	<link href="<?php echo base_url('public/css/fresh_theme.css'); ?>" rel="stylesheet" type="text/css" />
    <link rel="icon" href="<?php echo base_url('public/img/favicon.ico'); ?>"/>
    <link rel="stylesheet" href="<?php echo base_url('public/css/ui.jqgrid.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.ui.sunny.css'); ?>"/>        
    <script src="<?php echo base_url('public/lib/jquery.min.js'); ?>" type="text/javascript" ></script>
    <script src="<?php echo base_url('public/lib/jquery.ui.datepicker-es.js'); ?>" type="text/javascript" ></script>
    <script src="<?php echo base_url('public/lib/jquery.ui.sunny.js'); ?>" type="text/javascript" ></script>
    <script src="<?php echo base_url('public/lib/grid.locale-es.js'); ?>" type="text/javascript" ></script>
    <script src="<?php echo base_url('public/lib/jquery.jqGrid.min.js'); ?>" type="text/javascript" ></script>
	<script>
	$().ready(function() {
		$('button').button()
		$('.seleccion').button({
			icons:{
                  	primary: "ui-icon-arrowreturnthick-1-e"
           	}
		})		
		$('.seleccion_another').button({
			icons:{
				primary: "ui-icon ui-icon-circle-triangle-e"
			}
		})
	})
	</script>
</head>

<body>
	<div id="contenido" class="ui-widget">
		<div id="titulo">
			<strong>MENÚ</strong>
		</div>
		<div id="cabezera">
			<img src="<?php echo base_url('public/img/bannerAdministrativo.png'); ?>" width="800" height="67" alt="banner" />
		</div>
		<div id="menu" class="">
			<div>
				<h4 class="ui-widget-header ui-corner-top">USUARIOS</h4>
				<div class="ui-widget-content">					
					<?php echo anchor('usuarios/registrar',"<button class='seleccion'>Registrar</button>"); ?><br>
					<?php echo anchor('usuarios/actualizar',"<button class='seleccion'>Actualizar</button>"); ?>
				</div>
				<h4 class="ui-widget-header">TRANSACCIONES</h4>
				<div class="ui-widget-content">
					<?php echo anchor('prestamo_reserva/solicitud',"<button class='seleccion'>Solicitud prestamo</button>"); ?><br>
					<?php echo anchor('prestamo_reserva/prestamo',"<button class='seleccion'>Lista prestamo</button>"); ?><br>
					<?php echo anchor('prestamo_reserva/devolucion',"<button class='seleccion'>Lista devolución</button>"); ?>
				</div>
				<h4 class="ui-widget-header">TRANSACCIONES</h4>
				<div class="ui-widget-content ui-corner-bottom">
					<?php echo anchor('material_auxiliar/sinconfirmar',"<button class='seleccion'>Sin confirmar</button>"); ?>					
				</div>				
			</div>
			<div id="otros_menu" class="" style="margin-top: 10px;">
				<?php echo $menu; ?>				
			</div>
			<div id="terminal" class="ui-corner-all ui-widget-content">
				TERMINAL:<br> 
				<b><?php echo $sesion['nom_terminal']; ?></b>
			</div>
		</div>
		<footer id="pieDePagina" class="ui-state-default">
        	<div style="float: left;">
				Ciudad Universitaria - Av. Mercedes Indacochea N° 609<br />
				Teléfono: 232-1338, Huacho - Perú
          	</div>
			<div style="float: right">Desarrollado por: Nino D. Simeón Huaccho</div>                    
			<div style="clear: both;"></div>
		</footer>
		<div id="logeado" class="ui-widget-header">			
			<b><?php echo $sesion ['perfil_usuario'] ; ?>,</b> <?php echo $sesion ['apellidos_nombres']; ?> 
			<nav style="margin-right: 10px;float: right;">
				<a href="<?php echo site_url('variado/panel'); ?>">Panel de usuario</a> | 
				<a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar Sesión</a>
        	</nav>
		</div>  				
		<div id="contenido_contenido">
			<blockquote>
				<p>
					<strong>BIENVENIDOS AL SISTEMA DE INFORMACIÓN BIBLIOTECARIO</strong>
				</p>
				<p>
					En el menú ubicado en la parte superior izquierda puede realizar
					las operaciones designadas al <strong>AUXILIAR BIBLIOTECARIO</strong>.
				</p>
				<p>
					<strong>USUARIOS</strong>
				</p>
				<blockquote>
					<p>
						<strong><em>Registrar</em></strong> <em><strong>-&gt;</strong></em>Permite
						registrar a los nuevos usuarios de la facultad en el cual esta
						encargado.
					</p>
					<p>
						<strong><em>Actualizar-&gt;</em></strong>Permite cambiar datos del
						usuario así como tambien cambiar su contraseña.
					</p>
				</blockquote>
				<p>
					<strong>TRANSACCIONES</strong>
				</p>
				<blockquote>
					<p>
						<strong><em>Solicitud prestamo-&gt;</em></strong>En este módulo
						usted podra solicitar libros en su propio establecimiento, siendo
						el camino alterno a la solicitud echa por el mismo lector.
					</p>
					<p>
						<strong><em>Lista prestamo-&gt;</em></strong>En este módulo usted
						visualizara los prestamos echos hacia su terminal, puede
						confirmarlo o anularlo.
					</p>
					<p>
						<em><strong>Lista devolución-&gt;</strong></em>En este módulo
						usted visualizara la relación de préstamos sin devolver.
					</p>
				</blockquote>
				<p>
					<strong>MATERIAL BIBLIOGRÁFICO</strong>
				</p>
				<blockquote>
					<p>
						<strong><em>Sin confirmar-&gt;</em></strong>Son los item's
						ingresados por el BIBLIOTECARIO tiene que ser validado por el
						AUXILIAR para poder estar disponible a los lectores.
					</p>
					<p>
						<strong><em>Novedades terminal-&gt;</em></strong>Una vista global
						de los 10 materiales ingresados recientemente.
					</p>
				</blockquote>
			</blockquote>
		</div>
	</div>
</body>
</html>