<?php $sesion = $this->session->userdata('logeado'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="description" content="Sistema Web Bibliotecario UNJFSC" />  
        <title>..::Sistema Bibliotecario::..</title>
        <link href="<?php echo base_url('public/css/temaBibliotecaAuxiliar.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('public/css/fresh_theme.css'); ?>" rel="stylesheet" type="text/css" />
        <link rel="icon" href="<?php echo base_url('public/img/favicon.ico'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/ui.jqgrid.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.ui.sunny.css'); ?>"/>        
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.alerts.css'); ?>"/>        
        <script src="<?php echo base_url('public/lib/jquery.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.datepicker-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.sunny.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/grid.locale-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.jqGrid.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.alerts.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.alphanumeric.js'); ?>" type="text/javascript" ></script>
        <script>             
            $().ready(function() {       
                $('button').button()
                $('.seleccion').button({
                    icons:{
                        primary: "ui-icon-arrowreturnthick-1-e"
                    }
                })      
                $('.seleccion_another').button({
                    icons:{
                        primary: "ui-icon ui-icon-circle-triangle-e"
                    }
                })         
                $("#nino").click(function(){
                    jAlert('Promoción "Alan Turing" E.A.P. Ing. Informática<br /><br /><a href="http://about.me/ninosimeon">+ Info</a>','Nino Simeón');
                });      
                $('#buscador').hide();
                $('#listado_material tr').mouseover(function(){
                    $(this).css('background-color','#66CCFF');
                });
                $('#listado_material tr').mouseout(function(){
                    $(this).css('background-color','');
                });
            });
            function confirmar(data){
                $.post('<?php echo site_url('material_auxiliar/sinconfirmar'); ?>',{confirma: data},function(){
                    jAlert('Fue confirmado exitosamente;<br />'+data, 'CORRECTO', function(r){
                        if (r) {
                            document.location.href= '<?php echo site_url('material_auxiliar/sinconfirmar'); ?>';
                        }
                    });
                });
            }            
        </script>
    </head>
    <body>    
        <div id="contenido" class="ui-widget">
            <div id="buscador">Usuario: 
                <input type="text" name="usuarioBusca" id="usuarioBusca" />
                <input type="button" name="ir" id="ir" value="Ir" onclick="buscar($('#usuarioBusca').val())" /><div id="resultado" style="color: #000">
                    <p><b>No encontrado! =(</b></p>
                </div></div>
            <div id="titulo"><strong>SIN CONFIRMAR</strong></div>
            <div id="cabezera"><img src="<?php echo base_url(); ?>public/img/bannerAdministrativo.png" width="800" height="67" alt="banner" /></div>
            <div id="menu" class="">
                <div>
                    <h4 class="ui-widget-header ui-corner-top">USUARIOS</h4>
                    <div class="ui-widget-content">                 
                        <?php echo anchor('usuarios/registrar', "<button class='seleccion'>Registrar</button>"); ?><br>
                        <?php echo anchor('usuarios/actualizar', "<button class='seleccion'>Actualizar</button>"); ?>
                    </div>
                    <h4 class="ui-widget-header">TRANSACCIONES</h4>
                    <div class="ui-widget-content">
                        <?php echo anchor('prestamo_reserva/solicitud', "<button class='seleccion'>Solicitud prestamo</button>"); ?><br>
                        <?php echo anchor('prestamo_reserva/prestamo', "<button class='seleccion'>Lista prestamo</button>"); ?><br>
                        <?php echo anchor('prestamo_reserva/devolucion', "<button class='seleccion'>Lista devolución</button>"); ?>
                    </div>
                    <h4 class="ui-widget-header">TRANSACCIONES</h4>
                    <div class="ui-widget-content ui-corner-bottom">
                        <?php echo anchor('material_auxiliar/sinconfirmar', "<button class='seleccion'>Sin confirmar</button>"); ?>                        
                    </div>              
                </div>
                <div id="otros_menu" class="" style="margin-top: 10px;">
                    <?php echo $menu; ?>                                 
                </div>
                <div id="terminal" class="ui-corner-all ui-widget-content">
                    TERMINAL:<br> 
                    <b><?php echo $sesion['nom_terminal']; ?></b>
                </div>
            </div>
            <footer id="pieDePagina" class="ui-state-default">
                <div style="float: left;">
                    Ciudad Universitaria - Av. Mercedes Indacochea N° 609<br />
                    Teléfono: 232-1338, Huacho - Perú
                </div>
                <div style="float: right">Desarrollado por: Nino D. Simeón Huaccho</div>                    
                <div style="clear: both;"></div>
            </footer>
            <div id="logeado" class="ui-widget-header">         
                <b><?php echo $sesion ['perfil_usuario']; ?>,</b> <?php echo $sesion ['apellidos_nombres']; ?> 
                <nav style="margin-right: 10px;float: right;">
                    <a href="<?php echo site_url('variado/panel'); ?>">Panel de usuario</a> | 
                    <a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar Sesión</a>
                </nav>
            </div>
            <div id="terminal">TERMINAL:<br />
                <strong><?php echo $sesion['nom_terminal']; ?></strong></div>
            <div id="contenido_contenido">
                <?php if ($lista) {
                    ?>
                    <table width="100%" border="0" cellpadding="3" cellspacing="0" id="listado_material">
                        <tr>
                            <th width="19%" bgcolor="#F0F0F0" scope="col">SIGNATURA</th>
                            <th width="25%" bgcolor="#F0F0F0" scope="col">ISBN</th>
                            <th width="23%" bgcolor="#F0F0F0" scope="col">AUTOR</th>
                            <th width="26%" bgcolor="#F0F0F0" scope="col">TITULO</th>
                            <th width="7%" bgcolor="#F0F0F0" scope="col">&nbsp;</th>
                        </tr>
                        <?php foreach ($lista->result() as $value) {
                            ?>
                            <tr>
                                <td><?php echo $value->signatura; ?></td>
                                <td><?php echo $value->isbn; ?></td>
                                <td><?php echo $value->autores; ?></td>
                                <td><?php echo $value->titulo; ?></td>
                                <td><input type="button" name="btn_aprobar" id="btn_aprobar" value="√" onclick="confirmar('<?php echo $value->signatura; ?>')"/></td>
                            </tr><?php } ?>
                    </table><?php } ?>
            </div>
        </div>
    </body>
</html>