<?php $sesion = $this->session->userdata ( 'logeado' ); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="description" content="Sistema Web Bibliotecario UNJFSC" />  
        <title>..::Sistema Bibliotecario::..</title>
        <link href="<?php echo base_url('public/css/temaBibliotecaAuxiliar.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('public/css/fresh_theme.css'); ?>" rel="stylesheet" type="text/css" />
        <link rel="icon" href="<?php echo base_url('public/img/favicon.ico'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/ui.jqgrid.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.ui.sunny.css'); ?>"/>        
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.alerts.css'); ?>"/>        
        <script src="<?php echo base_url('public/lib/jquery.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.datepicker-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.sunny.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/grid.locale-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.jqGrid.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.alerts.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.alphanumeric.js'); ?>" type="text/javascript" ></script>
        <script>                
            $().ready(function() {
                $('button').button()
                $('.seleccion').button({
                    icons:{
                            primary: "ui-icon-arrowreturnthick-1-e"
                    }
                })      
                $('.seleccion_another').button({
                    icons:{
                        primary: "ui-icon ui-icon-circle-triangle-e"
                    }
                })
                $('#resultado').hide();
                $('#consumar_transaccion').hide();
                $('.tbl_prestamo tr').mouseover(function(){
                    $(this).css('background-color','#FEEEBD');
                });
                $('.tbl_prestamo tr').mouseout(function(){
                    $(this).css('background-color','');
                });
                $("#nino").click(function(){
                    jAlert('Promoción "Alan Turing" E.A.P. Ing. Informática<br /><br /><a href="http://about.me/ninosimeon">+ Info</a>','Nino Simeón');
                }); 
                $('#output_signatura').click(function(){
                    $.post('<?php echo site_url('variado/ver_item'); ?>',{item: $('#output_signatura').val()},function(r){
                        if (r.isbn) {
                            jAlert('<b>ISBN: </b>'+r.isbn+'<br /><b>TITULO: </b>'+r.titulo+'<br /><b>AUTOR: </b>'+r.autor+'<br /><b>EDITORIAL: </b>'+r.editorial+'<br /><b>TEMATICA: </b>'+r.tematica+'<br /><b>FECHA: </b>'+r.fecha+'<br /><b>TERMINAL: </b>'+r.terminal+'<br />', 'INFORMACION BIBLIOGRÁFICA');
                        }
                    },'json');
                });
            });
            function buscar(valor){
                jAlert('En progreso','DEVELOPING');
            }
            function carga_info(data){
                $.post('<?php echo site_url('prestamo_reserva/prestamo'); ?>',{click_busca: data},function(r){
                    $('#output_usuario').val(r.identificacion);
                    $('#output_signatura').val(r.isbn);
                    $('#output_estado').val(r.estado);
                },'json');                
            }          
            
            function confirmar(data){                
                $.post('<?php echo site_url('prestamo_reserva/prestamo'); ?>',{click_confirma: data},function(r){
                    if (r.estado == 'fail') {
                        jAlert('El item esta <b>¡NO DISPONIBLE!</b>', 'Are you kidding me? FFS');
                    } else {
                        $('#consumar_transaccion').show();
                        $('#descripcion').hide();
                        $('.tbl_prestamo').hide();
                        /*                         
                         */
                        $('.btn_consumar').hide();
                        $('#btn_cconfirmar').show().attr('onclick','confirmar_final('+data+')');
                        $('#btn_celiminar').show().attr('onclick','eliminar('+data+')');
                        $('#output_pusuario').val(r.usuario);
                        $('#output_pitem').val(r.item);
                        $('#output_ptipo').val(r.prestamo);
                        $('#output_pencargado').val(r.responsable);
                    }                    
                },'json');              
            }
            
            function confirmar_final(data){
                $.post('<?php echo site_url('prestamo_reserva/prestamo'); ?>',{confirmado: data},function(r){
                    if (r) {
                        $('#output_nprestamo').val(r.nprestamo);
                        $('#output_pfechini').val(r.finicio);
                        $('#output_phoraini').val(r.hinicio);
                        $('#output_pfechfin').val(r.ffin);
                        $('#output_phorafin').val(r.hfin);
                        $('.btn_consumar').hide();
                        $('#btn_caceptar').show();
                    }                   
                    else{
                        jAlert('Problemas con la transaccion =(','Try again!');
                    }
                },'json');
            }
            
            function aceptar_confirma(){
                //ESTA FUNCION SOLO ES FORMALIDAD ^^
                jAlert('¡Confirmado!','Correcto :D',function(r){
                    if (r) {
                        document.location.href= '<?php echo site_url('prestamo_reserva/prestamo'); ?>';
                    }
                });
            }
            
            function eliminar(data){
                $.post('<?php echo site_url('prestamo_reserva/prestamo'); ?>',{click_elimina: data},function(r){
                    if (r == 'ok') {
                        jAlert('Eliminado correctamente', '=)',function(c){
                            if (c) {
                                document.location.href= '<?php echo site_url('prestamo_reserva/prestamo'); ?>';
                            }
                        });
                        
                    } else {
                        jAlert('No pudo eliminarse :S','¡ERROR!');
                    }
                });                
            }
            
        </script>
        <style type="text/css">
            #descripcion {
                border: 1px dashed #000;
                /*height: 12%;*/
                padding-top: 3px;
                padding-right: 1px;
                padding-bottom: 1px;
                padding-left: 15px;
                /*background-color: #6CF;*/
            }
        </style>
    </head>
    <body>    
        <div id="contenido" class="ui-widget">
            <div id="buscador" class="ui-widget-content ui-corner-all">Usuario: 
                <input type="text" name="usuarioBusca" id="usuarioBusca" />
                <button type="button" name="ir" id="ir" onclick="buscar($('#usuarioBusca').val())">Ir</button>
                <div id="resultado" style="color: #000">
                    <p><b>No encontrado! =(</b></p>
                </div>
            </div>
            <div id="titulo"><strong>LISTA PRESTAMO</strong></div>
            <div id="cabezera"><img src="<?php echo base_url(); ?>public/img/bannerAdministrativo.png" width="800" height="67" alt="banner" /></div>
            <div id="menu" class="">
                <div>
                    <h4 class="ui-widget-header ui-corner-top">USUARIOS</h4>
                    <div class="ui-widget-content">                 
                        <?php echo anchor('usuarios/registrar',"<button class='seleccion'>Registrar</button>"); ?><br>
                        <?php echo anchor('usuarios/actualizar',"<button class='seleccion'>Actualizar</button>"); ?>
                    </div>
                    <h4 class="ui-widget-header">TRANSACCIONES</h4>
                    <div class="ui-widget-content">
                        <?php echo anchor('prestamo_reserva/solicitud',"<button class='seleccion'>Solicitud prestamo</button>"); ?><br>
                        <?php echo anchor('prestamo_reserva/prestamo',"<button class='seleccion'>Lista prestamo</button>"); ?><br>
                        <?php echo anchor('prestamo_reserva/devolucion',"<button class='seleccion'>Lista devolución</button>"); ?>
                    </div>
                    <h4 class="ui-widget-header">TRANSACCIONES</h4>
                    <div class="ui-widget-content ui-corner-bottom">
                        <?php echo anchor('material_auxiliar/sinconfirmar',"<button class='seleccion'>Sin confirmar</button>"); ?>                        
                    </div>              
                </div>
                <div id="otros_menu" class="" style="margin-top: 10px;">
                    <?php echo $menu; ?>                                 
                </div>
                <div id="terminal" class="ui-corner-all ui-widget-content">
                    TERMINAL:<br> 
                    <b><?php echo $sesion['nom_terminal']; ?></b>
                </div>
            </div>
            <footer id="pieDePagina" class="ui-state-default">
                <div style="float: left;">
                    Ciudad Universitaria - Av. Mercedes Indacochea N° 609<br />
                    Teléfono: 232-1338, Huacho - Perú
                </div>
                <div style="float: right">Desarrollado por: Nino D. Simeón Huaccho</div>                    
                <div style="clear: both;"></div>
            </footer>
            <div id="logeado" class="ui-widget-header">         
                <b><?php echo $sesion ['perfil_usuario'] ; ?>,</b> <?php echo $sesion ['apellidos_nombres']; ?> 
                <nav style="margin-right: 10px;float: right;">
                    <a href="<?php echo site_url('variado/panel'); ?>">Panel de usuario</a> | 
                    <a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar Sesión</a>
                </nav>
            </div>
            <div id="terminal">TERMINAL:<br />
                <strong><?php echo $sesion['nom_terminal']; ?></strong></div>
            <div id="contenido_contenido"> 
                <div id="descripcion" class="ui-widget-content">
                    <b>Usuario:</b>
                    <input name="output_usuario" type="text" id="output_usuario" style="width: 475px;" readonly="readonly"/>
                    <br />
                    <table width="94%" border="0" cellspacing="2" cellpadding="3">
                        <tr>
                            <th width="7%" scope="col">ITEM</th>
                            <th width="45%" scope="col"><input name="output_signatura" type="text" id="output_signatura" style="width: 90%" readonly="readonly" /></th>
                            <th width="16%" scope="col">SITUACIÓN</th>
                            <th width="32%" scope="col"><input name="output_estado" type="text" id="output_estado"  style="width: 80%" readonly="readonly"/></th>
                        </tr>
                    </table>
                </div><br />
                <div id="consumar_transaccion">
                    <table width="75%" border="0" align="center" cellpadding="2" cellspacing="0">
                        <tr>
                            <th colspan="2" bgcolor="#F0F0F0" scope="col">CONSUMAR TRANSACCIÓN:</th>
                        </tr>
                        <tr>
                            <td width="28%">N° préstamo:</td>
                            <td width="72%"><input name="output_nprestamo" type="text" id="output_nprestamo" style="width:20%" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <td>Usuario préstamo:</td>
                            <td><input name="output_pusuario" type="text" id="output_pusuario" style="width:90%" readonly="readonly"/></td>
                        </tr>
                        <tr>
                            <td>Item a transacción:</td>
                            <td><input name="output_pitem" type="text" id="output_pitem" style="width:60%" readonly="readonly"/></td>
                        </tr>
                        <tr>
                            <td>Tipo de préstamo:</td>
                            <td><input name="output_ptipo" type="text" id="output_ptipo" style="width:60%" readonly="readonly"/></td>
                        </tr>
                        <tr>
                            <td bgcolor="#CCFFCC">Fecha de inicio:</td>
                            <td bgcolor="#CCFFCC"><input name="output_pfechini" type="text" id="output_pfechini" style="width:50%" readonly="readonly"/></td>
                        </tr>
                        <tr>
                            <td bgcolor="#CCFFCC">Hora de inicio:</td>
                            <td bgcolor="#CCFFCC"><input name="output_phoraini" type="text" id="output_phoraini" style="width:50%" readonly="readonly"/></td>
                        </tr>
                        <tr>
                            <td bgcolor="#CCFFCC">Fecha de fin:</td>
                            <td bgcolor="#CCFFCC"><input name="output_pfechfin" type="text" id="output_pfechfin" style="width:50%" readonly="readonly"/></td>
                        </tr>
                        <tr>
                            <td bgcolor="#CCFFCC">Hora de fin:</td>
                            <td bgcolor="#CCFFCC"><input name="output_phorafin" type="text" id="output_phorafin" style="width:50%" readonly="readonly"/></td>
                        </tr>
                        <tr>
                            <td>Encargado</td>
                            <td><input name="output_pencargado" type="text" id="output_pencargado" style="width:90%" readonly="readonly"/></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td><input type="button" name="btn_cconfirmar" id="btn_cconfirmar" class="btn_consumar" value="Confirmar" />
                                <input type="button" name="btn_celiminar" id="btn_celiminar" class="btn_consumar" value="Eliminar" />
                                <input type="button" name="btn_caceptar" id="btn_caceptar" class="btn_consumar" value="Aceptar" onclick="aceptar_confirma()"/></td>
                        </tr>
                    </table>
                </div>
                <table width="100%" border="0" cellspacing="0" cellpadding="3" class="tbl_prestamo" id="tbl_lista">
                    <tr>
                        <th width="13%" bgcolor="#F0F0F0" scope="col">N°</th>
                        <th width="33%" bgcolor="#F0F0F0" scope="col">ITEM</th>
                        <th width="15%" bgcolor="#F0F0F0" scope="col">USUARIO</th>
                        <th width="15%" bgcolor="#F0F0F0" scope="col">FECHA</th>
                        <th width="12%" bgcolor="#F0F0F0" scope="col">HORA</th>
                        <th width="6%" bgcolor="#F0F0F0" scope="col">&nbsp;</th>
                        <th width="6%" bgcolor="#F0F0F0" scope="col">&nbsp;</th>
                    </tr>
                    <?php
                    if ($listado) {
                        foreach ($listado->result() as $value) {
                            ?>
                            <tr onclick="carga_info(<?php echo $value->nevento; ?>)">
                                <td align="center"><?php echo substr($value->tipoSolicitud, 0, 3) . '-' . $value->nevento; ?></td>
                                <td align="center"><?php echo $value->idItem; ?></td>
                                <td align="center"><?php echo $value->usuario; ?></td>
                                <td align="center"><?php echo $value->fecha; ?></td>
                                <td align="center"><?php echo $value->hora; ?></td>
                                <td align="center"><input type="button" name="btn_confirmar" id="btn_confirmar" value="✓" onclick="confirmar(<?php echo $value->nevento; ?>)"/></td>
                                <td align="center"><input type="button" name="btn_eliminar" id="btn_eliminar" value="✗" onclick="eliminar(<?php echo $value->nevento; ?>)"/></td>
                            </tr><?php }
            } ?>                    
                </table>
            </div>
        </div>
    </body>
</html>