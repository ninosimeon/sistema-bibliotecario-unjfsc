<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>..::Sistema Bibliotecario::..</title>
        <link href="<?php echo base_url(); ?>public/css/reporte.css" rel="stylesheet" type="text/css" /> 
        <link href="<?php echo base_url(); ?>public/css/jquery.ui.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>public/css/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <script src="<?php echo base_url(); ?>public/lib/jquery.min.js" type="text/javascript"></script>
        <script src="../../../../public/lib/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/lib/jquery.ui.datepicker-es.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/lib/jquery.ui.js" type="text/javascript"></script>
        <script src="../../../../public/lib/jquery.ui.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/lib/highcharts.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/lib/grid.locale-es.js" type="text/javascript"></script>
        <script src="../../../../public/lib/jquery.jqGrid.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/lib/jquery.jqGrid.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/lib/exporting.js" type="text/javascript"></script>                      
        <link href="../../../../public/css/reporte.css" rel="stylesheet" type="text/css" />      
        <script type="text/javascript">   
            var chart_publicacion;
            $().ready(function(){                
                /* INICIO MENÚ JQUERY UI
                 **/
                $('#menu_inicio').click(function(){                    
                    document.location.href= '<?= site_url('reporte') ?>';
                });
                $('#menu_ingreso').click(function(){
                    document.location.href= '<?= site_url('reporte/item_ingreso') ?>';
                });
                $('#menu_operaciones').click(function(){
                    document.location.href= '<?= site_url('reporte/operaciones') ?>';
                });
                $('#menu_publicacion').click(function(){
                    document.location.href= '<?= site_url('reporte/item_publicacion') ?>';
                });
                $('#menu_sancion').click(function(){
                    document.location.href= '<?= site_url('reporte/sancion') ?>';
                });
                $('#menu_resumen').click(function(){
                    document.location.href= '<?= site_url('reporte/resumen') ?>';
                });
                /* FIN MENÚ JQUERY UI
                 **/                     
                $('button[id=btn_ir]').button({
                    icons:{
                        primary: "ui-icon-circle-arrow-e"
                    },
                    text: true
                }).click(function(){                    
                    $.post('<?php echo site_url('reporte/jqgrid_publicacion'); ?>',{jqgrid_terminal: $('#select_terminal').val()},function(){
                        $('#tabla_jqgrid').jqGrid({
                            url:'<?php echo site_url('reporte/jqgrid_publicacion'); ?>',
                            datatype: "json",
                            mtype: "POST",
                            colNames:['Autores','Título', 'Signatura','ISBN', 'Fecha'],
                            colModel:[
                                {name:'autores',index:'autores', width: 200},
                                {name:'titulo',index:'titulo', width: 370},
                                {name:'signatura',index:'signatura', sortable: false},
                                {name: 'isbn', index: 'isbn', sortable: false, width: 115},
                                {name: 'publicacion', index: 'anio', sortable: true, width: 40}                            	
                            ],  
                            height: 400,
                            pager: '#pie_jqgrid',
                            rowNum: 20,
                            rowList: [10,20,30],
                            sortname: 'anio',
                            viewrecords: true,
                            sortorder: "asc",
                            gridview: true,
                            caption:"Relación items fecha de publicación"
                        }); 
                        $("#tabla_jqgrid").jqGrid('navGrid','#pie_jqgrid',{search: false, edit: false, del: false, add: false}).trigger("reloadGrid");
                        
                    },'json');
                    chart_publicacion = new Highcharts.Chart({
                        chart: {
                            renderTo: 'contenedor_publicacion',                        
                            type: 'column',
                            events: {
                                load: carga_publicacion()
                            }
                        },
                        title: {
                            text: ''
                        },legend: {
                            enabled: false  
                        },
                        tooltip: {
                            formatter: function() {
                                return '<b>Año '+ this.x +'</b>: '+ this.y+' items';
                            }
                        },xAxis:{
                            categories: [],
                            title: {
                                text: null
                            }
                        },yAxis:{
                            title:{
                                text: null
                            }
                        },
                        series: [{                                      
                                data: []
                            }]
                    }); 
                                               
                });
                $('button[id=btn_descarga_pdf]').button({
                    icons:{
                        primary: "ui-icon-disk"
                    }
                }).click(function(){
                    document.location.href= '<?= site_url('reporte/pdf_publicacion') ?>';
                });
                $('#menu_reporte ul').menu();
                $('#buscador_libros').hide();                
                $('#msg_nino').dialog({
                    autoOpen: false,
                    show: 'blind',
                    hide: 'explode',
                    title: 'Información',
                    width: 300,
                    modal: true
                });                
                $('#nino').click(function(){					
                    $('#msg_nino').dialog('open');
                });                      
            });
            function carga_publicacion(){
                $.post('<?php echo site_url('reporte/item_publicacion'); ?>',{grafico_terminal: $('#select_terminal').val()},function(r){
                    chart_publicacion.xAxis[0].setCategories(r.anio);                    
                    for (i = 0;r.cantidad.length; i++) {                          
                        chart_publicacion.series[0].addPoint(r.cantidad[i]);                         
                    }                                                              
                },'json');
            }
                       
        
        </script>
    </head>
    <body>        
        <div id="msg_nino"><p>Nino Simeón, promoción &quot;Alan Turing&quot;, E.A.P. Ing. Informática 2010 - II</p><p>e-mail: ninosimeon@gmail.com<br />
                web: <a href="http://about.me/ninosimeon">about.me/ninosimeon</a></p></div>
        <div id="contenedor">            
            <div id="bannerTOP"><img src="<?php echo base_url(); ?>public/img/banner_reporte/bannerReporte_mostrar_r1_c1.jpg" width="216" height="67" /><img src="<?php echo base_url(); ?>public/img/banner_reporte/bannerReporte_mostrar_r1_c2.jpg" width="193" height="67" /><img src="<?php echo base_url(); ?>public/img/banner_reporte/bannerReporte_mostrar_r1_c3.jpg" width="290" height="67" /><img src="<?php echo base_url(); ?>public/img/banner_reporte/bannerReporte_mostrar_r1_c4.jpg" width="201" height="67" /></div>
            <div id="superior">
                <div id="menu_reporte">
                    <ul>
                        <b>MENÚ</b>
                        <li id="menu_inicio"><a href="#">Inicio</a></li>
                        <li id="menu_ingreso"><a href="#">Items ingreso</a></li>
                        <li id="menu_operaciones"><a href="#">Operaciones</a></li>
                        <li id="menu_publicacion"><a href="#">Items publicación</a></li>
                        <li id="menu_sancion"><a href="#">Sanción</a></li>
                        <li id="menu_resumen"><a href="#">Resumen</a></li>
                    </ul>
                </div>            
                <div id="descripcion_usuario">
                    <div id="descripcion_usuario_perfil"><?= $sesion; ?></div>
                    <div id="descripcion_usuario_operacion"><a href="<?= site_url('variado/cerrar_sesion') ?>">Cerrar sesión</a></div>
                </div>
                <div id="tipo_reporte">REPORTE ITEMS PUBLICACIÓN</div>
                <div id="buscador_libros">ISBN:
                    <input type="text" name="input_isbn" id="input_isbn" style="width:140px"/>
                </div>
                <div id="descripcion_reporte">
                    <div id="saludo_reporte"><strong>Sr. bibliotecario</strong>, a continuación se muestra por terminal los items fecha de publicación. 
                        <button id="btn_descarga_pdf">Descargar PDF</button>
                    </div><div id="detalle_reporte">Si desea ver el reporte completo <b>descarge el PDF</b>, caso contrario dirigase en el menú de navegación y seleccione el terminal a investigar :)</div></div>
            </div>
            <div id="contenido_elemento">
                <div id="contenedor_publicacion" style="width:600px;float: right;height: 140px;clear: both;">
                </div>
                <div id="form_selector">TERMINAL: 
                    <select name="select_terminal" id="select_terminal">                        
                        <?php foreach ($relacion_terminal->result() as $value) { ?>
                            <option value="<?php echo $value->codTerminal; ?>"><?php echo $value->nomTerminal; ?></option>
                        <?php } ?>
                    </select>
                    <button id="btn_ir">Iniciar reporte</button>
                    <br />
                </div>
                <div id="contenido_jqgrid"><br />
                    <table id="tabla_jqgrid"></table>
                    <div id="pie_jqgrid"></div>
                </div>
            </div>
            <div id="footer">Desarrollado por: <a href="#" id="nino">Nino D. Simeón Huaccho</a><br />Ciudad Universitaria - Av. Mercedes Indacochea N 609<br />Teléfono: 232-1338, Huacho - Perú</div>
        </div>
    </body>
</html>