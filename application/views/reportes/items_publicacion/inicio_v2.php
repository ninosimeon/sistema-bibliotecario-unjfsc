<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="description" content="Sistema Web Bibliotecario UNJFSC" />
        <title>..::Sistema Bibliotecario::..</title>
        <link href="<?php echo base_url('public/css/reporte.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('public/css/fresh_theme.css'); ?>" rel="stylesheet" type="text/css" />
        <link rel="icon" href="<?php echo base_url('public/img/favicon.ico'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/ui.jqgrid.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.ui.sunny.css'); ?>"/>        
        <script src="<?php echo base_url('public/lib/jquery.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.datepicker-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.sunny.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/highcharts.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('public/lib/grid.locale-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.jqGrid.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/exporting.js'); ?>" type="text/javascript"></script>  
        <script>   
            var chart_publicacion;
            $().ready(function(){ 
                $('button').button()
                $('.seleccion').button({
                    icons:{
                        primary: "ui-icon-arrowreturnthick-1-e"
                    }
                })      
                $('.seleccion_another').button({
                    icons:{
                        primary: "ui-icon ui-icon-circle-triangle-e"
                    }
                })                      
                $('button[id=btn_ir]').button({
                    icons:{
                        primary: "ui-icon-circle-arrow-e"
                    },
                    text: true
                }).click(function(){                    
                    $.post('<?php echo site_url('reporte/jqgrid_publicacion'); ?>',{jqgrid_terminal: $('#select_terminal').val()},function(){
                        $('#tabla_jqgrid').jqGrid({
                            url:'<?php echo site_url('reporte/jqgrid_publicacion'); ?>',
                            datatype: "json",
                            mtype: "POST",
                            colNames:['Autores','Título', 'Signatura','ISBN', 'Fecha'],
                            colModel:[
                                {name:'autores',index:'autores', width: 200},
                                {name:'titulo',index:'titulo', width: 370},
                                {name:'signatura',index:'signatura', sortable: false},
                                {name: 'isbn', index: 'isbn', sortable: false, width: 115},
                                {name: 'publicacion', index: 'anio', sortable: true, width: 40}                            	
                            ],  
                            height: 400,
                            pager: '#pie_jqgrid',
                            rowNum: 20,
                            rowList: [10,20,30],
                            sortname: 'anio',
                            viewrecords: true,
                            sortorder: "asc",
                            gridview: true,
                            caption:"Relación items fecha de publicación"
                        }); 
                        $("#tabla_jqgrid").jqGrid('navGrid','#pie_jqgrid',{search: false, edit: false, del: false, add: false}).trigger("reloadGrid");
                        
                    },'json');
                    chart_publicacion = new Highcharts.Chart({
                        chart: {
                            renderTo: 'contenedor_publicacion',                        
                            type: 'bar',
                            events: {
                                load: carga_publicacion()
                            }
                        },
                        title: {
                            text: ''
                        },legend: {
                            enabled: false  
                        },
                        tooltip: {
                            formatter: function() {
                                return '<b>Año '+ this.x +'</b>: '+ this.y+' items';
                            }
                        },xAxis:{
                            categories: [],
                            title: {
                                text: null
                            }
                        },yAxis:{
                            title:{
                                text: null
                            }
                        },
                        series: [{                                      
                                data: []
                            }]
                    }); 
                                               
                });
                $('button[id=btn_descarga_pdf]').button({
                    icons:{
                        primary: "ui-icon-disk"
                    }
                }).click(function(){
                    document.location.href= '<?= site_url('reporte/pdf_publicacion') ?>';
                });
                $('#menu_reporte ul').menu();
                $('#buscador_libros').hide();                
                $('#msg_nino').dialog({
                    autoOpen: false,
                    show: 'blind',
                    hide: 'explode',
                    title: 'Información',
                    width: 300,
                    modal: true
                });                
                $('#nino').click(function(){					
                    $('#msg_nino').dialog('open');
                });                      
            });
            function carga_publicacion(){
                $.post('<?php echo site_url('reporte/item_publicacion'); ?>',{grafico_terminal: $('#select_terminal').val()},function(r){
                    chart_publicacion.xAxis[0].setCategories(r.anio);                    
                    for (i = 0;r.cantidad.length; i++) {                          
                        chart_publicacion.series[0].addPoint(r.cantidad[i]);                         
                    }                                                              
                },'json');
            }
                       
        
        </script>
    </head>
    <body>        
        <div id="msg_nino"><p>Nino Simeón, promoción &quot;Alan Turing&quot;, E.A.P. Ing. Informática 2010 - II</p><p>e-mail: ninosimeon@gmail.com<br />
                web: <a href="http://about.me/ninosimeon">about.me/ninosimeon</a></p></div>
        <div id="contenedors" class="ui-widget">            
            <div id="bannerTOP"><img src="<?php echo base_url(); ?>public/img/banner_reporte/bannerReporte_mostrar_r1_c1.jpg" width="216" height="67" /><img src="<?php echo base_url(); ?>public/img/banner_reporte/bannerReporte_mostrar_r1_c2.jpg" width="193" height="67" /><img src="<?php echo base_url(); ?>public/img/banner_reporte/bannerReporte_mostrar_r1_c3.jpg" width="290" height="67" /><img src="<?php echo base_url(); ?>public/img/banner_reporte/bannerReporte_mostrar_r1_c4.jpg" width="201" height="67" /></div>
            <div id="superior">
                <div id="menu_reporte" class="">
                    <div>
                        <h4 class="ui-widget-header ui-corner-top">MENÚ</h4>
                        <div class="ui-widget-content ui-corner-bottom">                 
                            <?php echo anchor('reporte', "<button class='seleccion'>Inicio</button>"); ?><br>
                            <?php echo anchor('reporte/item_ingreso', "<button class='seleccion'>Items ingreso</button>"); ?><br>
                            <?php echo anchor('reporte/operaciones', "<button class='seleccion'>Operaciones</button>"); ?><br>
                            <?php echo anchor('reporte/item_publicacion', "<button class='seleccion'>Items publicación</button>"); ?><br>
                            <?php echo anchor('reporte/sancion', "<button class='seleccion'>Sanción</button>"); ?><br>
                            <?php echo anchor('reporte/resumen', "<button class='seleccion'>Resumen</button>"); ?>
                        </div>                                                         
                    </div>
                    <div id="otros_menu" class="" style="margin-top: 10px;">
                        <?php echo $menu; ?>                                 
                    </div>                    
                </div>           
                <div id="descripcion_usuario" class="ui-widget-header" style="width: 685px;">
                    <?= $sesion; ?> <nav style="margin-right: 10px;">
                        <a href="<?php echo site_url('variado/panel'); ?>">
                            Panel de usuario</a>
                        | <a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar
                            Sesión</a>
                    </nav>
                </div> 
                <div id="tipo_reporte">REPORTE ITEMS PUBLICACIÓN</div>
                <div id="buscador_libros">ISBN:
                    <input type="text" name="input_isbn" id="input_isbn" style="width:140px"/>
                </div>
                <div id="descripcion_reporte">
                    <div id="saludo_reporte"><strong>Sr. bibliotecario</strong>, a continuación se muestra por terminal los items fecha de publicación. 
                        <button id="btn_descarga_pdf">Descargar PDF</button>
                    </div>
                    <div id="detalle_reporte">Si desea ver el reporte completo <b>descarge el PDF</b>, caso contrario dirigase en el menú de navegación y seleccione el terminal a investigar :)
                    </div>
                    <div id="form_selector" class="ui-widget-content ui-corner-all">TERMINAL: 
                        <select name="select_terminal" id="select_terminal">                        
                            <?php foreach ($relacion_terminal->result() as $value) { ?>
                                <option value="<?php echo $value->codTerminal; ?>"><?php echo $value->nomTerminal; ?></option>
                            <?php } ?>
                        </select>
                        <button id="btn_ir">Iniciar reporte</button>                        
                    </div> 
                </div>                                       
            </div>
            <div id="contenido_elemento">           
                <div id="contenedor_publicacion" style="width:100%;height: 700px;clear: both;">
                </div>
                <div id="contenido_jqgrid"><br />
                    <table id="tabla_jqgrid"></table>
                    <div id="pie_jqgrid"></div>
                </div>
            </div>
            <footer id="footer" class="ui-state-default">
                <div style="float: left;">
                    Ciudad Universitaria - Av. Mercedes Indacochea N° 609<br />
                    Teléfono: 232-1338, Huacho - Perú
                </div>
                <div style="float: right">Desarrollado por: Nino D. Simeón Huaccho</div>                    
                <div style="clear: both;"></div>
            </footer>            
        </div>
    </body>
</html>