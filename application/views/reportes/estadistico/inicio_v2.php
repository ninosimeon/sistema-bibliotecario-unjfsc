<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>..::Sistema Bibliotecario::..</title>
        <link href="<?php echo base_url(); ?>public/css/ui.jqgrid.css" rel="stylesheet" type="text/css" media="screen"/>
        <link href="<?php echo base_url(); ?>public/css/jquery.ui.css" rel="stylesheet" type="text/css" media="screen"/>
        <script type="text/javascript" src="<?php echo base_url(); ?>public/lib/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>public/lib/jquery.alerts.js" type="text/javascript"></script>        
        <link href="<?php echo base_url(); ?>public/css/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />      
        <script src="<?php echo base_url(); ?>public/lib/jquery.alphanumeric.js" type="text/javascript"></script>        
        <script src="<?php echo base_url(); ?>public/lib/highcharts.js" type="text/javascript"></script>          
        <script src="../../../../public/lib/highcharts.js" type="text/javascript"></script>        
        <link href="../../../../public/css/temaBibliotecaAuxiliar.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>public/css/temaBibliotecaAuxiliar.css" rel="stylesheet" type="text/css" />
        <script src="<?php echo base_url(); ?>public/lib/grid.locale-es.js" type="text/javascript"></script>  
        <script src="<?php echo base_url(); ?>public/lib/jquery.jqGrid.min.js" type="text/javascript"></script>  
        <script type="text/javascript">               
            var chart, chart_publicacion, chart_ingreso;            
            $(document).ready(function(){   
                $('#select_terminal_ingreso').change(function(){
                    //jAlert($('#select_terminal_ingreso').val());
                    $('#relacion_ingreso').hide();
                    $.post('<?php echo site_url('reporte/estadistico'); ?>',{terminal_ingreso_column: $('#select_terminal_ingreso').val()},function(){
                        chart_ingreso = new Highcharts.Chart({
                            chart: {
                                renderTo: 'contenedor_ingreso',                        
                                type: 'pie',
                                events: {
                                    load: carga_ingreso() // i know it
                                }
                            },
                            title: {
                                text: 'Reporte por fecha de ingreso'
                            },legend: {
                                enabled: false  
                            },
                            tooltip: {
                                formatter: function() {
                                    return '<b>Mes '+ this.x +'</b>: '+ this.y+' items';
                                }
                            },xAxis:{
                                categories: [],
                                title: {
                                    text: null
                                }
                            },yAxis:{
                                title:{
                                    text: null
                                }
                            },
                            series: [{                                      
                                    data: []
                                }]
                        });    
                    },'json');                    
                });
                $('#select_terminal').change(function(){
                    //jAlert($('#select_terminal').val());
                    $('.grafico_especifico_fpublicacion').hide();
                    $('#tbl_fpublicacion').show();                     
                    $.post('<?php echo site_url('reporte/estadistico'); ?>',{jqgrid: true, jqtipo: 'fecha_publicacion', jqparametro: $('#select_terminal').val()},function(){
                        $('#jqgrid_fpublicacion').jqGrid({
                            url:'<?php echo site_url('reporte/fecha_publicacion'); ?>',
                            datatype: "json",
                            mtype: "POST",
                            colNames:['Autores','Título', 'Signatura','ISBN'],
                            colModel:[
                                {name:'autores',index:'autores asc, titulo', width:150},
                                {name:'titulo',index:'autores', width:205},
                                {name:'signatura',index:'signatura', width:120},
                                {name: 'isbn', index: 'isbn', width: 100, sortable: false}                            	
                            ],  
                            height: 230,                            
                            pager: '#paginacion_fpublicacion',
                            rowNum: 10,
                            rowList: [10,20,30],
                            sortname: 'autores',
                            viewrecords: true,
                            sortorder: "desc",
                            gridview: true,
                            caption:"Item bibliografico"
                        }); 
                        $("#jqgrid_fpublicacion").jqGrid('navGrid','#paginacion_fpublicacion',{search: false, edit: false, del: false, add: false});
                    },'json');                                  
                });
                $('.muestra_container').hide();
                $('#inicio').show();
                $('.mostrar').hide();
                $('#contenedor_publicacion').show();                
                $('input[name=tipo_reporte]:radio').click(function(){                    
                    //jAlert($('input[name=tipo_reporte]:checked').val());
                    if ($('input[name=tipo_reporte]:checked').val() == 'item') {
                        $('.mostrar').hide();
                        $('#publicacion').show();
                        $('.grafico_especifico_fpublicacion').hide();
                        $('#contenedor_publicacion').show();
                    } else {
                        jAlert('operaciones');
                    }
                });
                $('input[name=item_f]:radio').click(function(){
                    if ($('input[name=item_f]:checked').val() == 'a_publicacion') {                        
                        $('.muestra_container').hide();
                        $('#fecha_publicacion').show();
                        chart_publicacion = new Highcharts.Chart({
                            chart: {
                                renderTo: 'contenedor_publicacion',                        
                                type: 'column',
                                events: {
                                    load: carga_publicacion() // i know it
                                }
                            },
                            title: {
                                text: 'Reporte por fecha de publicación'
                            },legend: {
                                enabled: false  
                            },
                            tooltip: {
                                formatter: function() {
                                    return '<b>Año '+ this.x +'</b>: '+ this.y+' items';
                                }
                            },xAxis:{
                                categories: [],
                                title: {
                                    text: null
                                }
                            },yAxis:{
                                title:{
                                    text: null
                                }
                            },
                            series: [{                                      
                                    data: []
                                }]
                        });
                    } else if($('input[name=item_f]:checked').val() == 'a_ingreso'){
                        $('.muestra_container').hide();
                        $('#anio_ingreso').show();     
                        $.post('<?php echo site_url('reporte/estadistico') ?>',{terminal_ingreso: true},function(r){
                            $('#relacion_ingreso').empty();
                            $('#relacion_ingreso').append('<b>Año</b>   Cantidad<br />');
                            $('#select_terminal_ingreso').append($('<option></option>').attr('value','idk').text('Seleccione'));
                            for (i = 0; i < r.length; i++) {
                                $('#relacion_ingreso').append(r[i][0]+' '+r[i][1]);
                                $('#select_terminal_ingreso').append($('<option></option>').attr('value',r[i][0]).text(r[i][0]));
                            }                            
                        },'json');
                    }
                });
                $('#selecciona').show();
                $('#buscador').hide();                
                $("#nino").click(function(){
                    jAlert('Promoción "Alan Turing" E.A.P. Ing. Informática<br /><br /><a href="http://about.me/ninosimeon">+ Info</a>','Nino Simeón');
                });      
                $('#input_terminal').change(function(){
                    $.post('<?php echo site_url('reporte/estadistico'); ?>',{terminal : $('#input_terminal').val()},function(r){
                        $('.mostrar').hide();
                        $('#tipo_reporte').show();  
                        $('#fecha_publicacion p:first').append(' <b>'+r.terminal+'</b>');
                        $('#anio_ingreso p:first').append(' <b>'+r.terminal+'</b>');
                    },'json');    
                    
                });
                chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'contenedor_graphic',                        
                        type: 'pie',
                        events: {
                            load: cargaterminal() // i know it
                        }
                    },
                    title: {
                        text: 'Relación de items'
                    },
                    tooltip: {
                        formatter: function() {
                            return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                formatter: function() {
                                    return '<b>'+ this.point.name +'</b>: '+ this.y ;
                                }
                            }
                        }
                    },
                    series: [{                          
                            data: []
                        }]
                });                
            });
            function detecta_fpublicacion(){
                jAlert('Hola');
            }
            function cargaterminal(){
                $.post('<?php echo site_url('reporte/estadistico'); ?>',{terminales_item: true},function(r){                      
                    for (i = 0;r.length; i++) { //En este caso tambien pudo ser -> i < r.length pero emite una absurdo error (que se ignora)
                        chart.series[0].addPoint(r[i]);
                    }
                },'json');
            }     
            function recarga(){
                document.location.href= '<?php echo site_url('reporte/estadistico'); ?>';
                /*$('.mostrar').hide();
                        $('#selecciona').show();*/
            }
            function carga_ingreso(){
                $.post('<?php echo site_url('reporte/estadistico'); ?>',{terminal_ingre: true},function(r){
                    for (i = 0;r.length; i++) {
                        chart_ingreso.series[0].addPoint(r[i]);
                    }
                },'json');
            }
            function carga_publicacion(){
                $.post('<?php echo site_url('reporte/estadistico'); ?>',{terminal_pub: true},function(r){
                    //jAlert(r[0][0]);
                    $('#select_terminal').append($('<option></option>').attr('value','idk').text('Seleccione'));
                    for (i = 0; r.length; i++) {                                 
                        chart_publicacion.series[0].addPoint(r[i]);                         
                        $('#select_terminal').append($('<option></option>').attr('value',r[i][0]).text(r[i][0]));
                    }      
                    
                },'json');
            }                
            
        </script>        
        <style type="text/css"></style>
    </head>
    <body>    
        <div id="contenido">
            <div id="reporte_container"><div id="anio_ingreso" class="muestra_container">
                    <p>Seleccione el año: <select name="select_terminal_ingreso" id="select_terminal_ingreso" ></select></p>
                    <div id="relacion_ingreso"></div>
                    <div id="tbl_fingreso" class="grafico_ingreso">
                        <table id="jqgrid_fingreso"></table>
                        <div id="paginacion_fingreso"></div>
                    </div>
                    <div id="contenedor_ingreso" class="grafico_ingreso"></div>
                </div>
                <div id="fecha_publicacion" class="muestra_container">
                    <p>Seleccione el año: <select name="select_terminal" id="select_terminal" ></select></p>
                    <div id="tbl_fpublicacion" class="grafico_especifico_fpublicacion"> 
                        <table id="jqgrid_fpublicacion"></table>                   
                        <div id="paginacion_fpublicacion"></div>
                    </div>
                    <div id="contenedor_publicacion" class="grafico_especifico_fpublicacion"></div>
                </div><div id="inicio" class="muestra_container">
                    <p><div id="selecciona" class="mostrar">Por favor seleccione el terminal a investigar:                
                            <select name="input_terminal" id="input_terminal">
                                <option value="idk">Seleccione</option>
                                <?php foreach ($terminal->result() as $value) { ?>
                                    <option value="<?php echo $value->codTerminal; ?>"><?php echo $value->nomTerminal; ?></option>
                                <?php } ?>
                            </select></div><div id="tipo_reporte" class="mostrar">Ahora el tipo de <strong>reporte estadístico</strong>:
                            <input type="radio" name="tipo_reporte" id="select_item" value="item" />
                            ITEM 
                            <input type="radio" name="tipo_reporte" id="select_operaciones" value="operaciones" />
                            OPERACIONES 
                            <input type="button" name="btn_otro_term" id="btn_otro_term" value="¿Otro terminal?" onclick="recarga()"/>
                        </div>
                        <div id="publicacion" class="mostrar">Finalmente deseas verlo por: 
                            <input type="radio" name="item_f" id="a_publicacion" value="a_publicacion" />
                            Fecha de publicacón
                            <strong>ó</strong>
                            <input type="radio" name="item_f" id="a_ingreso" value="a_ingreso" />
                            Fecha de ingreso</div>
                    </p>
                    <div id="contenedor_graphic" style="width: 550px; height: 350px;"></div>

                </div></div>
            <div id="buscador">Usuario: 
                <input type="text" name="usuarioBusca" id="usuarioBusca" />
                <input type="button" name="ir" id="ir" value="Ir" onclick="buscar($('#usuarioBusca').val())" /><div id="resultado" style="color: #000">
                    <p><b>No encontrado! =(</b></p>
                </div></div>
            <div id="titulo"><strong>ESTADÍSTICO</strong></div>
            <div id="cabezera"><img src="<?php echo base_url(); ?>public/img/bannerAdministrativo.png" width="800" height="67" alt="banner" />
                <div id="reportemenu"><table width="100%" border="0" cellspacing="3" cellpadding="0">
                        <tr>
                            <td colspan="2" align="center" bgcolor="#3D3D3D" scope="col"><strong>REPORTES</strong></td>
                        </tr>
                        <tr>
                            <td width="11%" align="right" valign="middle"><img src="<?php echo base_url(); ?>public/img/btn.jpg" width="14" height="17" /></td>
                            <td width="89%"><a href="<?php echo site_url('reporte'); ?>">Inicio</a></td>
                        </tr>
                        <tr>
                            <td align="right"><img src="<?php echo base_url(); ?>public/img/btn.jpg" alt="" width="14" height="17" /></td>
                            <td><a href="<?php echo site_url('reporte/operaciones'); ?>">Operaciones</a></td>
                        </tr>
                        <tr>
                            <td align="right"><img src="<?php echo base_url(); ?>public/img/btn.jpg" alt="" width="14" height="17" /></td>
                            <td><a href="<?php echo site_url('reporte/estadistico'); ?>">Estadístico</a></td>
                        </tr>
                        <tr>
                            <td align="right"><img src="<?php echo base_url(); ?>public/img/btn.jpg" alt="" width="14" height="17" /></td>
                            <td><a href="<?php echo site_url('reporte/resumen'); ?>">Resumen</a></td>
                        </tr>
                    </table></div>
            </div>
            <div id="pieDePagina">Desarrollado por: <strong><a href="#" id="nino">Nino D. Simeón Huaccho</a></strong><a href="#"></a><br />
                Ciudad Universitaria - Av. Mercedes Indacochea N 609<br />
                Teléfono: 232-1338, Huacho - Perú<br />
            </div>
            <div id="logeado">
                <table width="100%" border="0" cellspacing="1" cellpadding="0">
                    <tr>
                        <td width="63%" scope="col"><?php
                                $sesion = $this->session->userdata('logeado');
                                echo '<b>' . $sesion['perfil_usuario'] . '</b>, ' . $sesion['apellidos_nombres'];
                                ?></td>
                        <td width="22%" scope="col"><a href="#">Cambiar contraseña</a></td>
                        <td width="15%" scope="col"><a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar Sesión</a></td>
                    </tr>
                </table>
            </div>
        </div>
    </body>
</html>