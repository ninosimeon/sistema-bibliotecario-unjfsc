<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="description" content="Sistema Web Bibliotecario UNJFSC" />
        <title>..::Sistema Bibliotecario::..</title>
        <link href="<?php echo base_url(); ?>public/css/estilosBusqueda.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>public/css/jquery.ui.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>public/css/ui.jqgrid.css" rel="stylesheet" type="text/css" />                      
        <script type="text/javascript" src="<?php echo base_url(); ?>public/lib/modernizr.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>public/lib/jquery.min.js"></script>      
        <script src="<?php echo base_url(); ?>public/lib/jquery.ui.datepicker-es.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/lib/jquery.ui.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/lib/grid.locale-es.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/lib/jquery.jqGrid.min.js" type="text/javascript"></script>
        <script>
            $().ready(function(){       
                
                
                /* INICIO MENÚ JQUERY UI
                 **/
                $('#menu_inicio').click(function(){                    
                    document.location.href= '<?= site_url('reporte') ?>';
                });
                $('#menu_ingreso').click(function(){
                    document.location.href= '<?= site_url('reporte/item_ingreso') ?>';
                });
                $('#menu_operaciones').click(function(){
                    document.location.href= '<?= site_url('reporte/operaciones') ?>';
                });
                $('#menu_publicacion').click(function(){
                    document.location.href= '<?= site_url('reporte/item_publicacion') ?>';
                });
                $('#menu_sancion').click(function(){
                    document.location.href= '<?= site_url('reporte/sancion') ?>';
                });
                $('#menu_resumen').click(function(){
                    document.location.href= '<?= site_url('reporte/resumen') ?>';
                });
                /* FIN MENÚ JQUERY UI
                 **/ 
                $('#menu ul').menu();
                $('#creditos').click(function(){
                    $('#msg_ninosimeon').dialog('open');
                });                
                $('#msg_ninosimeon').dialog({
                    autoOpen: false,
                    show: 'blind',
                    hide: 'explode',
                    title: 'Créditos',
                    width: 300
                });    
                $('.resultado_busqueda').hide()                
                $('#envia_isbn').submit(function(){
                    $.post('<?php echo site_url('reporte/material_detalle'); ?>',{relacion_first: true, isbn: $('#buscar').val()},function(r){                        
                        $('#resp_terminal').text('')
                        $('.resultado_busqueda').show()
                        $('#relacion_descripcion').hide()
                        $('#relacion_signatura').hide()                        
                        for(i=0;i<r.length;i++){
                            $('#resp_terminal').append('<a href=# onclick=terminal_detalle("'+r[i].cod_terminal+'")><b>'+r[i].nom_terminal+':</b></a> '+r[i].cantidad+'<br />')    
                        }                        
                    },'json');
                    return false;
                })
                $('.resultado_busqueda').hide();
                $('#descripcion_signatura').hide();                
            });            
            function terminal_detalle(terminal){
                $.post('<?php echo site_url('reporte/material_detalle'); ?>',{relacion_item: true, isbn: $('#buscar').val(),cod_terminal: terminal},function(r){
                    $('#relacion_descripcion').show()
                    $('#relacion_signatura').show()
                    $('#relacion_signatura ul').text('')                    
                    $('#autor').text(r[0].autor)
                    $('#publicacion').text(r[0].publicacion)
                    $('#titulo').text(r[0].titulo)
                    $('#edicion').text(r[0].edicion)
                    $('#volumen').text(r[0].volumen)
                    $('#soporte').text(r[0].soporte)
                    for(i=0;i<r.length;i++){
                        $('#relacion_signatura ul').append("<li>"+r[i].signatura+'</li>')
                    } 
                    /*
                     ** INICIO JQGRID
                     */             
                    $('#jqgrid_signatura').jqGrid({
                        url: '<?php echo site_url('reporte/jqgrid_material'); ?>',
                        datatype: "json",
                        mtype: "post",
                        colNames: ['SIGNATURA','FECHA INGRESO','HORA INGRESO'],
                        colModel: [
                            {name: 'signatura', index: 'signatura', width: 350, align: 'center'},
                            {name: 'fecha_ingreso', index: 'fecha_ingreso', width: 200, align: 'center'},
                            {name: 'hora_ingreso', index: 'hora_ingreso',width: 200, align: 'center'}
                        ],
                        height: 300,
                        pager: '#pie_signatura',
                        rowNum: 10,
                        rowList: [10,20,30],
                        sortname: 'signatura',
                        viewrecords: true,
                        sortorder: 'asc',   
                        multiselect: false,
                        gridview: true,                            
                        subGrid: true,
                        caption: 'Relación de ítems bibliográficos',
                        subGridUrl: '<?php echo site_url('reporte/jqgrid_evento_material'); ?>',
                        subGridModel: [
                            {name:['DNI','NOMBRES','FECHA','HORA','FACULTAD','EVENTO'],
                                width: [100,300,50,70,100,70]}]                                                                   
                    });
                    $('#jqgrid_signatura').jqGrid('navGrid','#pie_signatura',{search:true, edit: false, del: false, add:false}).trigger("reloadGrid");
                    /*
                     ** FIN JQGRID
                     */
                },'json')
                $('#relacion_descripcion h2').text(terminal)                
            }            
        </script>
        <style>
            #resp_terminal a{
                text-decoration: blink;
                color: #000000;
                font-weight: normal;
                text-shadow: none;
            }
            #menu a{
                text-decoration: none;
                color: #000000;
                font-weight: normal;
                text-shadow: none;
            }
            #menu{
                margin: 10px;
                width: auto; 
            }
            .derecha{
                margin-left: 150px;
                width: 785px;
            }
            #resultados{
                margin-top: 55px;

            }
            #relacion_descripcion h2{
                color: black;
                -webkit-text-fill-color: white; /* Will override color (regardless of order) */
                -webkit-text-stroke-width: 1px;
                -webkit-text-stroke-color: black;
                font-size: 2em;
            }
            #relacion_descripcion{
                margin-left: 100px;
                border: 1px black solid; 
                padding-left: 25px;
                padding-bottom: 10px; 
                width: 660px;
                border-radius: 15px;
            }
            .relacion_desc{
                font-size: 1.5em;
            }
            .contenido_jqgrid{
                margin-top: 25px;
            }
        </style>
    </head>
    <div id="msg_ninosimeon">
        <p>
            Desarrollador: <b>Nino David Simeón Huaccho</b><br /> <br /> <b>mail:</b>
            ninosimeon@gmail.com<br /> <b>url:</b> <a
                href="http://about.me/ninosimeon">about.me/ninosimeon</a><br />
        </p>
        <h4>E.A.P. Ing. Informática "Alan Turing"</h4>
    </div>
    <body>
        <div id="contenedor">
            <header>
                <img
                    src="<?php echo base_url(); ?>public/img/banner_optimizado/bannerReporte_r1_c1.jpg"
                    width="221" height="67" alt="bannerReporte_r1_c1" /><img
                    src="<?php echo base_url(); ?>public/img/banner_optimizado/bannerReporte_r1_c2.jpg"
                    width="297" height="67" alt="bannerReporte_r1_c2" /><img
                    src="<?php echo base_url(); ?>public/img/banner_optimizado/bannerReporte_r1_c3.jpg"
                    width="432" height="67" alt="bannerReporte_r1_c3" />
            </header>
            <section>
                <div id="menu">
                    <ul>
                        <b>MENÚ</b>
                        <li id="menu_inicio"><a href="#">Inicio</a></li>
                        <li id="menu_ingreso"><a href="#">Items ingreso</a></li>
                        <li id="menu_material"><a href="#">Material Bibliográfico</a></li>
                        <li id="menu_operaciones"><a href="#">Operaciones</a></li>
                        <li id="menu_publicacion"><a href="#">Items publicación</a></li>
                        <li id="menu_alumnos"><a href="#">Alumnos</a></li>
                        <li id="menu_sancion"><a href="#">Sanción</a></li>                        
                    </ul>                    
                </div>
                <div id="descripcion_usuario" class="derecha">
                    <b><?php echo $persona['cargo']; ?>,</b> <?php echo $persona['nombres']; ?> <nav>
                        <a href="<?php echo site_url('variado/panel'); ?>">
                            Panel de usuario</a>
                        | <a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar
                            Sesión</a>
                    </nav>
                </div>                
                <div id="titulo_descriptivo" class="derecha">
                    <h1>REPORTEADOR DE MATERIAL BIBLIOGRÁFICO</h1>
                </div>
                <article>
                    <div id="buscador" class="derecha">                       
                        <form id="envia_isbn">
                            <label for="buscar"><b>ISBN:</b> </label>
                            <input id="buscar" name="simple" type="search" required
                                   placeholder="Digita el ISBN posteriormente ENTER"
                                   style="width: 720px; font-size: 1.5em;" />
                        </form>
                    </div>
                    <div id="resultados">
                        <div id="relacion_terminal" class="resultado_busqueda">
                            <p>
                                <b>RELACIÓN DE ITEMS POR TERMINAL:</b>
                            </p>
                            <p>
                                <span id="resp_terminal"></span>
                            </p>
                        </div>
                        <div id="relacion_descripcion" class="resultado_busqueda">
                            <h2></h2>
                            <p>    
                                <b>AUTOR:</b> <span id="autor" class="relacion_desc"></span> <b>FECHA DE PUBLICACIÓN:</b> <span id="publicacion" class="relacion_desc"></span><br/>
                                <b>TÍTULO:</b> <span id="titulo" class="relacion_desc"></span><br/>
                                <b>EDICIÓN:</b> <span id="edicion" class="relacion_desc"></span> <b>VOLUMEN:</b> <span id="volumen" class="relacion_desc"></span> <b>SOPORTE:</b> <span id="soporte" class="relacion_desc"></span>
                            </p>                            
                        </div>  
                        <div class="contenido_jqgrid">
                            <table id="jqgrid_signatura">                                
                            </table>
                            <div id="pie_signatura"></div>                            
                        </div>
                    </div>
                </article>                
                <footer>
                    Desarrollado por: <a href="#" id="creditos"><b>Nino Simeón Huaccho</b></a><br />
                    Ciudad Universitaria, Av. Mercedes Indacochea N° 609 <br />
                    Teléfono: 232-1338. Huacho - Perú
                </footer>
            </section>
        </div>
    </body>
</html>
