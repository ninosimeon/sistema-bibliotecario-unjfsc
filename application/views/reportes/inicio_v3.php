<!DOCTYPE html>
<html lang="es">
    <head>       
        <meta charset="utf-8" />
        <meta name="description" content="Sistema Web Bibliotecario UNJFSC" />
        <title>..::Sistema Bibliotecario::..</title>
        <link href="<?php echo base_url('public/css/reporte.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('public/css/fresh_theme.css'); ?>" rel="stylesheet" type="text/css" />
        <link rel="icon" href="<?php echo base_url('public/img/favicon.ico'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/ui.jqgrid.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.ui.sunny.css'); ?>"/>        
        <script src="<?php echo base_url('public/lib/jquery.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.datepicker-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.sunny.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/highcharts.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('public/lib/grid.locale-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.jqGrid.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/exporting.js'); ?>" type="text/javascript"></script>                      
        <script>          
            var chart;
            $().ready(function(){
                $('button').button()
                $('.seleccion').button({
                    icons:{
                        primary: "ui-icon-arrowreturnthick-1-e"
                    }
                })      
                $('.seleccion_another').button({
                    icons:{
                        primary: "ui-icon ui-icon-circle-triangle-e"
                    }
                }) 
                $('#menu_reporte ul').menu();
                $('#buscador_libros').hide();                
                $('#msg_nino').dialog({
                    autoOpen: false,
                    show: 'blind',
                    hide: 'explode',
                    title: 'Información',
                    width: 300,
                    modal: true
                });                
                $('#nino').click(function(){					
                    $('#msg_nino').dialog('open');
                });
                chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'grafico_terminales',                        
                        type: 'pie',
                        events: {
                            load: cargaterminal() 
                        }
                    },
                    title: {
                        text: 'Relación de items'
                    },
                    tooltip: {
                        formatter: function() {
                            return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                formatter: function() {
                                    return '<b>'+ this.point.name +'</b>: '+ this.y ;
                                }
                            }
                        }
                    },
                    series: [{                          
                            data: []
                        }]
                });      
            });
            function cargaterminal(){
                $.post('<?php echo site_url('reporte'); ?>',{relacion_items: true},function(r){                      
                    for (i = 0;r.length; i++) { 
                        chart.series[0].addPoint(r[i]);
                    }
                },'json');
            }
        </script>
    </head>
    <body>      
        <div id="contenedors" class="ui-widget">            
            <div id="bannerTOP"><img src="<?php echo base_url(); ?>public/img/banner_reporte/bannerReporte_mostrar_r1_c1.jpg" width="216" height="67" /><img src="<?php echo base_url(); ?>public/img/banner_reporte/bannerReporte_mostrar_r1_c2.jpg" width="193" height="67" /><img src="<?php echo base_url(); ?>public/img/banner_reporte/bannerReporte_mostrar_r1_c3.jpg" width="290" height="67" /><img src="<?php echo base_url(); ?>public/img/banner_reporte/bannerReporte_mostrar_r1_c4.jpg" width="201" height="67" /></div>
            <div id="superior">
                <div id="descripcion_usuario" class="ui-widget-header" style="width: 685px;">
                    <?= $sesion; ?> <nav style="margin-right: 10px;">
                        <a href="<?php echo site_url('variado/panel'); ?>">
                            Panel de usuario</a>
                        | <a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar
                            Sesión</a>
                    </nav>
                </div>  
                <div id="menu_reporte" class="">
                    <div>
                        <h4 class="ui-widget-header ui-corner-top">MENÚ</h4>
                        <div class="ui-widget-content ui-corner-bottom">                 
                            <?php echo anchor('reporte', "<button class='seleccion'>Inicio</button>"); ?><br>
                            <?php echo anchor('reporte/item_ingreso', "<button class='seleccion'>Items ingreso</button>"); ?><br>
                            <?php echo anchor('reporte/operaciones', "<button class='seleccion'>Operaciones</button>"); ?><br>
                            <?php echo anchor('reporte/item_publicacion', "<button class='seleccion'>Items publicación</button>"); ?><br>
                            <?php echo anchor('reporte/sancion', "<button class='seleccion'>Sanción</button>"); ?><br>
                            <?php echo anchor('reporte/resumen', "<button class='seleccion'>Resumen</button>"); ?>
                        </div>                                                         
                    </div>
                    <div id="otros_menu" class="" style="margin-top: 10px;">
                        <?php echo $menu; ?>                                 
                    </div>                    
                </div>                                                          
                <div id="tipo_reporte">REPORTE </div>                
                <div id="descripcion_reporte">
                    <p><b>Sr. bibliotecario</b>, el módulo de reportes permite obtener datos relevantes obtenidos gracias al Sistema de Información bibliotecario.</p>
                    <p>El menú ubicado en la parte superior izquierda indica los tipos de reporte a realizar, por favor eliga uno para su evaluación ;).</p>
                </div>
                <div id="contenido_elemento">                    
                    <div id="grafico_terminales" style="float: right; margin-top: 90px; margin-right: 100px;">graficando</div>
                </div>                        
            </div>                
            <footer id="footer" class="ui-state-default">
                <div style="float: left;">
                    Ciudad Universitaria - Av. Mercedes Indacochea N° 609<br />
                    Teléfono: 232-1338, Huacho - Perú
                </div>
                <div style="float: right">Desarrollado por: Nino D. Simeón Huaccho</div>                    
                <div style="clear: both;"></div>
            </footer>            
        </div>
    </body>
</html>