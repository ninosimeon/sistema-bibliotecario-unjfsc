<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="description" content="Sistema Web Bibliotecario UNJFSC" />
        <title>..::Sistema Bibliotecario::..</title>
        <link href="<?php echo base_url('public/css/estilosBusqueda.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('public/css/fresh_theme.css'); ?>" rel="stylesheet" type="text/css" />
        <link rel="icon" href="<?php echo base_url('public/img/favicon.ico'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/ui.jqgrid.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.ui.sunny.css'); ?>"/>        
        <script src="<?php echo base_url('public/lib/jquery.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.datepicker-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.sunny.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/grid.locale-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.jqGrid.min.js'); ?>" type="text/javascript" ></script>
        <script>
            $().ready(function(){                          
                $('#busqueda_avanzada').hide();                                                      
                $('#msg_indice').dialog({
                    autoOpen: false,
                    show: 'slide',
                    hide: 'explode',
                    title: 'Índice',
                    width: 500
                });
                $('.btn_lanzamiento').hide();
                $('#resultados form').mouseover(function(){                   
                    $(this).find('button[id="btn_go"]:not(:visible)').show();                   
                });
                $('#resultados form').mouseout(function(){
                    $(this).find('button[id="btn_go"]:visible').hide();                                      
                });
            });            
            function avanzado(){
                $('#busqueda_simple').hide('slow',function(){
                    $('#busqueda_avanzada').show('slow');
                });                
            }
            function busca_simple(){
                $('#busqueda_avanzada').hide('slow',function(){
                    $('#busqueda_simple').show('slow');
                }); 
            }                        
            function lanza_indice(isbn){
                $.post('<?php echo site_url('visualizarcatalogo/consulta_isbn'); ?>',{isbn_consulta: isbn},function(r){
                    if(r == 'fail'){
                        alert('No disponible, por el momento');
                    }else{
                        $('#msg_indice p').html(r);
                        $('#msg_indice').dialog('open');
                    }
                },'json');
            }
        </script>
    </head>
    <div id="msg_indice">
        <p>

        </p>
    </div>    
    <body>
        <div id="contenedor" class="ui-widget">
            <header>
                <img
                    src="<?php echo base_url('public/img/banner_optimizado/bannerReporte_r1_c1.jpg'); ?>"
                    width="221" height="67" alt="bannerReporte_r1_c1" /><img
                    src="<?php echo base_url('public/img/banner_optimizado/bannerReporte_r1_c2.jpg'); ?>"
                    width="297" height="67" alt="bannerReporte_r1_c2" /><img
                    src="<?php echo base_url('public/img/banner_optimizado/bannerReporte_r1_c3.jpg'); ?>"
                    width="432" height="67" alt="bannerReporte_r1_c3" />
            </header>
            <section>
                <div id="imagen">
                    <img src="<?php echo base_url(); ?>public/img/unjfsc_interior.jpg"
                         width="220" height="165" alt="UNJFSC INTERIOR" />
                </div>
                <div id="descripcion_usuario" class="derecha ui-widget-header" style="width: 685px;">
                    <b><?php echo $persona['cargo']; ?>,</b> <?php echo $persona['nombres']; ?> <nav style="margin-right: 10px;">
                        <a href="<?php echo site_url('variado/panel'); ?>">
                            Panel de usuario</a>
                        | <a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar
                            Sesión</a>
                    </nav>
                </div>                
                <div id="titulo_descriptivo" class="derecha ui-corner-all ui-widget-content">
                    <h1>BUSCADOR DE MATERIAL BIBLIOGRÁFICO</h1>
                </div>
                <article>
                    <div id="buscador" class="derecha ui-widget-content ui-corner-tr ui-corner-bl">
                        <form id="busqueda_simple" class="busqueda" action="<?php echo site_url('visualizarcatalogo/busqueda_simple'); ?>" method="POST" accept-charset="utf-8">
                            <label for="buscar"><b>BUSCAR:</b> </label><input id="buscar" name="simple" type="search" required autofocus placeholder="Escribe palabras a buscar" style="width: 80%; font-size: 1.5em;" /> 
                            <a href="#" onclick="avanzado()"> 
                                <i style="">¿Avanzado?</i>
                            </a>
                        </form>
                        <?php echo form_open('visualizarcatalogo/busqueda_avanzada', array('id' => 'busqueda_avanzada', 'class' => 'busqueda')); ?>                        
                        <label for="input_autor">AUTOR:</label><input type="search" name="input_autor" placeholder="Francisco Ceballos Javier" style="width: 61%;" />
                        <label for="input_isbn">ISBN:</label><input placeholder="12-550-1225-44" type="search" name="input_isbn" style="width: 22%;" /> 
                        <br /> 
                        <label for="input_titulo">TÍTULO:</label><input placeholder="Java 2" type="search" name="input_titulo" style="width: 73%;" />
                        <button type="submit" style="margin-left: 12px;">Buscar</button>
                        <button type="button" onclick="busca_simple()">๑</button>
                        <br /> <label for="input_contenido">CONTENIDO:</label><input placeholder="Programación orientada a objetos" type="search" style="width: 86%" name="input_contenido" />
                        <?php echo form_close(); ?>
                    </div>
                    <div id="resultados">
                        <br />
                        <?php
                        if ($resultados == 'fail') {
                            ?><form>
                                <h3>No encontre coincidencias =(</h3>
                            </form><?php } else if ($resultados != '') { ?>
                            <?php
                            foreach ($resultados as $value) {
                                ?>
                                <?php echo form_open('visualizarcatalogo/procesa_solicitud') ?>
                                <b>TITULO:</b> <?php
                        echo $value->titulo;
                        $sesion = $this->session->userdata('logeado');
                        if ($sesion['perfil_usuario'] != 'INVITADO') {
                            if (preg_match('/EBOOK/', $value->ISBN)) {
                                echo form_button(array('onclick' => "document.location.href='" . base_url("public/pdf/" . $value->ISBN . ".pdf") . "'", 'type' => 'button', 'id' => 'btn_go', 'class' => 'btn_lanzamiento', 'content' => '>>'));
                            } else {
                                echo form_button(array('id' => 'btn_go', 'type' => 'submit', 'class' => 'btn_lanzamiento', 'content' => '>>'));
                            }
                        }
                                ?>
                                <br /> <b>AUTOR:</b> <?php echo $value->autores; ?>
                                <br /> <b>PUBLICACIÓN:</b> <?php echo substr($value->fecPublicacion, 0, 4) ?> <b>EDICIÓN:</b> <?php
                        echo $value->edicion;
                        if ($value->volumen) {
                            echo ' <b>VOLUMEN:</b> ' . $value->volumen;
                        }
                                ?>
                                <br /> <b>UBICACIÓN:</b> <?php echo $value->nomTerminal; ?> <input
                                    type="hidden" name="valor_isbn"
                                    value="<?php echo $value->ISBN; ?>" /><input type="hidden"
                                    name="ubicacion" value="<?php echo $value->codTerminal; ?>" />
                                <button onclick="lanza_indice('<?php echo $value->ISBN; ?>')" type="button" id="b_indice">Índice</button>
                                <?php echo form_close(); ?><br />                                
                                <?php
                            }
                        }
                        ?>

                        <nav>
                            <?php echo $this->pagination->create_links(); ?>
                        </nav>
                    </div>
                </article>
                <footer class="ui-state-default">
                    <div style="float: left;">
                        Ciudad Universitaria - Av. Mercedes Indacochea N° 609<br />
                        Teléfono: 232-1338, Huacho - Perú
                    </div>
                    <div style="float: right">Desarrollado por: Nino D. Simeón Huaccho</div>                    
                    <div style="clear: both;"></div>
                </footer>
            </section>
        </div>
    </body>
</html>
