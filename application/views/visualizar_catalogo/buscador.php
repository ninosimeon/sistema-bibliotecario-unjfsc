<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>..::Sistema Bibliotecario::..</title>
        <link href="<?php echo base_url(); ?>public/css/temaBibliotecaRMK.css" rel="stylesheet" type="text/css" />        
        <script type="text/javascript" src="<?php echo base_url(); ?>public/lib/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>public/lib/jquery.alerts.js" type="text/javascript"></script>        
        <link href="<?php echo base_url(); ?>public/css/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />      
        <script type="text/javascript">
            $().ready(function() {
                $("#nino").click(function(){
                    jAlert('Promoción "Alan Turing" E.A.P. Ing. Informática<br /><br /><a href="http://about.me/ninosimeon">+ Info</a>','Nino Simeón');
                });                 
                $('#resultado_busqueda tr').mouseover(function(){
                    $(this).css('background-color','#66CCFF');
                });
                $('#resultado_busqueda tr').mouseout(function(){
                    $(this).css('background-color','');
                });
                $('input[name=solicita_item]:radio').click(function(){                    
                    $.post('<?php echo site_url('visualizarcatalogo/solicitud_reserva'); ?>',{averigua:$('input[name=solicita_item]:checked').val()},function(data){
                        if (data.estado=='DISPONIBLE') {
                            $('.btn_enviar').hide();
                            $('#detalles_reserva').hide();
                            $('#btn_enviar_solicitud').show();
                        }else{
                            $('.btn_enviar').hide();
                            $('#detalles_reserva').show();
                            $('#btn_enviar_reserva').show();                            
                            $('#reserva_fecha').val(data.fecha);
                            $('#reserva_hora').val(data.hora); 
                            $('#lista_reservas').empty();                            
                            for (i = 0; data['dni'].length; i++) {
                                $('#lista_reservas').append($('<option></option>').attr('value',data['dni'][i]).text(data['dni'][i]));
                            }                            
                        }
                    },'json');
                });
                $('.btn_enviar').hide();
                $('.detalles').hide();                         
                
                $('#btn_limpiar').click(function(){
                    $('.input_busqueda').val('');
                });    
                $('.resultado').hide();
                $('#resultado_<?php echo $div_determinante; ?>').show();
<?php echo $div_procesamiento; ?>
    });
    function habilita(data){                             
        if($('#act_'+data)){
            var nuevo = "deshabilita('"+data+"')";
            $('#act_'+data).attr('onclick',nuevo);                    
            $('#input_'+data).attr('disabled',false);                                        
        }
    }			
    function deshabilita(data){
        if($('#act_'+data)){
            var antiguo = "habilita('"+data+"')";
            $('#act_'+data).attr('onclick',antiguo);                    
            $('#input_'+data).attr('disabled',true);                                     
        }
    }
    function enviar_solicitud(){
        $.post('<?php echo site_url('visualizarcatalogo/solicitud_reserva'); ?>',{solicitud :$('input[name=solicita_item]:checked').val(), tipo_solicitud: 'SOLICITUD' },function(data){
            if (data == 'pendiente') {
                $('.detalles').hide();
                $('#detalles_fail').show();
            } else {
                $('.detalles').hide();
                $('#detalles_solicitado').show();
            }
        });        
    }
    function enviar_reserva(){
        $.post('<?php echo site_url('visualizarcatalogo/solicitud_reserva'); ?>',{solicitud :$('input[name=solicita_item]:checked').val(), tipo_solicitud: 'RESERVA' },function(data){
            if (data == 'pendiente') {
                $('.detalles').hide();
                $('#detalles_fail').show();
            } else {
                $('.detalles').hide();
                $('#detalles_reservado').show();
            }
        });        
    }
    
        </script>
        <style type="text/css">
            #titulo_principal {
                position:absolute;
                width:610px;
                height:59px;
                z-index:8;
                left: 181px;
                top: 127px;
                text-align: center;
                background-color: #FFFF66;
                border-top-width: 1px;
                border-right-width: 1px;
                border-bottom-width: 1px;
                border-left-width: 1px;
                border-top-style: solid;
                border-right-style: solid;
                border-bottom-style: solid;
                border-left-style: solid;
            }
            #nuevas_adquisiciones {
                position:absolute;
                width:173px;
                height:12px;
                z-index:2;
                left: 423px;
                top: 38px;
                padding: 3px;
                background-color: #CCCC99;
                border-top-width: 1px;
                border-right-width: 1px;
                border-bottom-width: 1px;
                border-left-width: 1px;
                border-top-style: dashed;
                border-right-style: dashed;
                border-bottom-style: dashed;
                border-left-style: dashed;
            }
            #procesa_busqueda {
                border-top-width: 1px;
                border-right-width: 1px;
                border-bottom-width: 1px;
                border-left-width: 1px;
                border-top-style: dashed;
                border-right-style: dashed;
                border-bottom-style: dashed;
                border-left-style: dashed;
                width: 100%;
            }
            #botones_direccionamiento {
                margin-left: 56.5%;
                margin-top: 1%;
            }
        </style>
    </head>

    <body>

        <div id="contenido">
            <div id="cabezera"><img src="<?php echo base_url(); ?>public/img/bannerAdministrativo.png" width="800" height="67" alt="banner" /></div>

            <div id="logeado">
                <table width="100%" border="0" cellspacing="1" cellpadding="0">
                    <tr>
                        <td width="63%" scope="col"><?php $sesion = $this->session->userdata('logeado');
echo '<b>' . $sesion['perfil_usuario'] . '</b>, ' . $sesion['apellidos_nombres']; ?></td>
                        <td width="22%" scope="col"><a href="#">Cambiar contraseña</a></td> 
                        <td width="15%" scope="col"><a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar Sesión</a></td>
                    </tr>
                </table>
            </div>
            <div id="terminal">TERMINAL:<br />
                <strong><?php echo $sesion['nom_terminal']; ?></strong></div>
            <div id="contenido_contenido"><div id="procesa_busqueda" class="procesa" style=""><?php echo form_open('visualizarcatalogo/busquedacatalogo'); ?>
                    <table width="100%" border="0" cellspacing="0" cellpadding="2">
                        <tr>
                            <td width="19%" nowrap="nowrap" bgcolor="#99CC66" scope="col">Terminal:</td>
                            <td width="17%" bgcolor="#99CC66" scope="col"><input type="radio" name="terminal" id="terminal2" value="TODOS" />
                                En todos</td>
                            <td colspan="2" bgcolor="#99CC66" scope="col"><input name="terminal" type="radio" id="terminal3" value="<?php echo $sesion['cod_terminal']; ?>" checked="checked" />
                                Solo en este</td>
                        </tr>
                        <tr>
                            <td align="right" nowrap="nowrap"><input type="submit" name="btn_buscar" id="btn_buscar" value="Buscar" style="width:100%"/></td>
                            <td><input type="button" name="btn_limpiar" id="btn_limpiar" value="Limpiar" style="width:100%"/></td>
                            <td width="22%"><input name="act_tit" type="checkbox" id="act_tit" onclick="habilita('tit')"/>
                                Título:</td>
                            <td width="42%" nowrap="nowrap"><input name="input_tit" class="input_busqueda" type="text" disabled="disabled" id="input_tit" style="width:230px"/></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap"><input type="checkbox" name="act_isb" id="act_isb" onclick="habilita('isb')"/>
                                ISBN-ISSN-Codigo:</td>
                            <td><input name="input_isb" type="text" disabled="disabled" id="input_isb" class="input_busqueda" style="width:100px" /></td>
                            <td><input name="act_aut" type="checkbox" id="act_aut" onclick="habilita('aut')"/>
                                Autor:</td>
                            <td nowrap="nowrap"><input name="input_aut" type="text" class="input_busqueda" disabled="disabled" id="input_aut" style="width:230px"/></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap"><input name="act_cat"  type="checkbox" id="act_cat" onclick="habilita('cat')"/>
                                Categoría:</td>
                            <td><select name="input_cat" id="input_cat"  disabled="disabled">
                                    <?php foreach ($categoria->result() as $value) { ?>
                                        <option><?php echo $value->categoria; ?></option><?php } ?>                                  
                                </select></td>
                            <td><input name="act_edi" type="checkbox" id="act_edi" onclick="habilita('edi')"/>
                                Editorial:</td>
                            <td nowrap="nowrap"><input name="input_edi" class="input_busqueda" type="text" disabled="disabled" id="input_edi" style="width:230px" /></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap"><input name="act_tem" type="checkbox" id="act_tem" onclick="habilita('tem')"/>
                                Temática:</td>
                            <td colspan="3"><select name="input_tem" id="input_tem" disabled="disabled">
                                    <?php foreach ($tematica->result() as $value) {
                                        ?>
                                        <option value="<?php echo $value->codCategoria; ?>"><?php echo $value->descripcion; ?></option><?php } ?>
                                </select></td>
                        </tr>
                    </table><?php echo form_close(); ?>
                </div>
                <div id="contenido_resultado"><br />
                    <?php
                    if ($resultadoBusqueda == 'fail') {
                        echo '<b>¡NO SE ENCONTRO COINCIDENCIAS! =(</b>';
                    } else if ($resultadoBusqueda == '') {
                        echo '<b>INGRESE SU CONSULTA =)</b>';
                    } else {
                        ?>
                        <div id="resultado_novedades" class="resultado"><?php echo form_open('visualizarcatalogo/novedades'); ?>Seleccione terminal: 
                            <select name="select_term" id="select_term">
                                <?php foreach ($all_terminales->result() as $value) {
                                    ?>
                                    <option value="<?php echo $value->codTerminal; ?>"><?php echo $value->nomTerminal; ?></option><?php } ?>
                            </select>
                            <input type="submit" name="btn_novedad" id="btn_novedad" value="&gt;&gt; Go" /><?php echo form_close(); ?>
                            <table width="100%" border="0" cellspacing="0" cellpadding="1">
                                <tr>
                                    <th width="38%" bgcolor="#99CC66" scope="col">TÍTULO</th>
                                    <th width="22%" bgcolor="#99CC66" scope="col">AUTOR(ES)</th>
                                    <th width="10%" bgcolor="#99CC66" scope="col">AÑO</th>
                                    <th width="16%" bgcolor="#99CC66" scope="col">TIPO DE ITEM</th>
                                    <th width="14%" bgcolor="#99CC66" scope="col">SOPORTE</th>
                                </tr>
                                <?php
                                if ($resultadoNovedades == '') {
                                    echo '<tr><td><b>SELECCIONE EL TERMINAL =)</b></td></tr>';
                                } else {
                                    foreach ($resultadoNovedades->result() as $value) {
                                        ?>
                                        <tr>
                                            <td><?php echo $value->titulo; ?></td>
                                            <td><?php echo $value->autores; ?></td>
                                            <td><?php echo substr($value->fecPublicacion, 0, 4); ?></td>
                                            <td><?php echo $value->categoria; ?></td>
                                            <td><?php echo $value->soporte; ?></td>
                                        </tr>       <?php }
                    } ?>                     
                            </table>
                            <?php echo form_open('visualizarcatalogo/regresaCatalogo'); ?><input type="submit" name="btn_regresa_catalogo" id="btn_regresa_catalogo" value="&lt;&lt; Regresar catálogo" /><?php echo form_close(); ?>
                        </div>
                        <div id="resultado_solicitud" class="resultado" style="">
                            <table width="100%" border="0" cellspacing="0" cellpadding="2">
                                <tr>
                                    <td colspan="2" align="center" bgcolor="#99CC66" scope="col"><strong>SITUACIÓN ACTUAL DE ITEMS:</strong></td>
                                    <td width="56%" rowspan="4" valign="top" scope="col"><strong>PASOS PARA ENVÍO DE SOLICITUD:</strong><br />
                                        1. Consulte la relación de items.<br />
                                        2. Elija de preferencia uno que posea estado disponible.<br />
                                        3. Para ítems con estado ocupado consultar detalles adicionales de reserva.<br />
                                        4. Enviar transacción.</td>
                                </tr>
                                <tr>
                                    <td width="20%" height="26">Items disponibles:</td>
                                    <td width="24%"><input name="item_disponible" type="text" id="item_disponible" style="width:30%" value="<?php echo $cant_disponible; ?>" readonly="readonly"/></td>
                                </tr>
                                <tr>
                                    <td height="26">Items ocupados:</td>
                                    <td><input name="item_ocupado" type="text" id="item_ocupado" style="width:30%" value="<?php echo $cant_nodisponible; ?>" readonly="readonly"/></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td><td>&nbsp;</td></tr>
                            </table><br />
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="55%" align="left" valign="top" scope="col"><table width="96%" border="1" align="left" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <th width="22%" bgcolor="#F0F0F0" scope="col"><strong>ITEM</strong></th>
                                                <th width="25%" bgcolor="#F0F0F0" scope="col"><strong>MOD. PREST</strong></th>
                                                <th width="22%" bgcolor="#F0F0F0" scope="col"><strong>SOPORTE</strong></th>
                                                <th width="26%" bgcolor="#F0F0F0" scope="col"><strong>ESTADO</strong></th>
                                                <th width="5%" bgcolor="#F0F0F0" scope="col">°</th>
                                            </tr><?php foreach ($resultadoBusqueda as $value) { ?>
                                                <tr>
                                                    <td><?php echo $value->signatura; ?></td>
                                                    <td><?php echo $value->modPrestamo; ?></td>
                                                    <td><?php echo $value->soporte; ?></td>
                                                    <td bgcolor="<?php
                            if ($value->estado == 'DISPONIBLE') {
                                echo '#99CC66';
                            } else {
                                echo '#FF0000';
                            }
                                ?>"><?php echo $value->estado; ?></td>
                                                    <td><input type="radio" name="solicita_item"  value="<?php echo $value->signatura; ?>" /></td>
                                                </tr> <?php } ?>                                           
                                        </table></td>
                                    <td width="42%" align="left" valign="top" bgcolor="#99CC66" scope="col"><div id="detalles_fail" class="detalles"><strong>¡NO SE PUEDE PROCESAR SOLICITUD!</strong>
                                            <p>No se puede procesar más de una solicitud por usuario, culmine la solicitud anterior para poder solicitar nuevamente.</p></div><div id="detalles_reservado" class="detalles" style=""><strong>¡RESERVA ENVIADA EXITOSAMENTE! </strong>
                                            <p>Por favor, vaya puntualmente a recoger el material.<br />
                                                El sistema envia las reservas mas no asegura su exclusividad. <br />
                                                LA <strong>DEVOLUCIÓN DE MATERIAL PUNTUAL</strong> ES IMPORTANTE, <strong>EVITE SANCIONES.</strong></p></div>
                                        <div id="detalles_solicitado" class="detalles" style=""><strong>¡SOLICITUD ENVIADA EXITOSAMENTE!</strong>
                                            <p>Por favor, recoja el item lo más antes posible para evitar solicitudes morosas.<br />
                                                El sistema envia las solicitudes mas no asegura su exclusividad. <br />
                                                LA <strong>DEVOLUCIÓN DE MATERIAL PUNTUAL</strong> ES IMPORTANTE, <strong>EVITE SANCIONES.</strong></p> 
                                        </div><div id="detalles_reserva" class="detalles" style=""><strong>Detalles adicionales de reservas:</strong>
                                            <table width="100%" border="1" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <th bgcolor="#FFFFFF" scope="col">SOLICITANTES</th>
                                                    <th bgcolor="#FFFFFF" scope="col">FECHA DEVOLUCION</th>
                                                    <th bgcolor="#FFFFFF" scope="col">HORA DEVOLUCIÓN</th>
                                                </tr>
                                                <tr>
                                                    <td rowspan="2" valign="top" bgcolor="#FFFFFF"><select name="lista_reservas" size="4" id="lista_reservas" style="width:100%">
                                                        </select></td>
                                                    <td align="center" valign="top" bgcolor="#FFFFFF"><input name="reserva_fecha" type="text" id="reserva_fecha" style="width:60px"  readonly="readonly"  /></td>
                                                    <td align="center" valign="top" bgcolor="#FFFFFF"><input name="reserva_hora" type="text" id="reserva_hora" style="width:78px" readonly="readonly" /></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" bgcolor="#FFFFFF">&nbsp;</td>
                                                </tr>
                                            </table></div>
                                    </td>
                                </tr>
                            </table><div id="botones_direccionamiento"><input type="button" name="btn_enviar_solicitud" id="btn_enviar_solicitud" class="btn_enviar" onclick="enviar_solicitud()" value="&gt;&gt; Enviar solicitud" />
                                <input type="button" name="btn_enviar_reserva" onclick="enviar_reserva()" class="btn_enviar" id="btn_enviar_reserva" value="&gt;&gt; Enviar reserva" />
                                <br /><?php echo form_open('visualizarcatalogo/regresacatalogo'); ?>
                                <input type="submit" name="regresar_catalogo" id="regresar_catalogo" value="&lt;&lt; Regresar catálogo" onclick=""/><?php echo form_close(); ?></div>
                        </div>
                        <div id="resultado_busqueda" class="resultado" style=""><?php echo form_open('visualizarcatalogo/procesasolicitud'); ?>
                            <table width="100%" border="0" cellspacing="0" cellpadding="2">
                                <tr>
                                    <th width="17%" bgcolor="#F0F0F0" scope="col">TERMINAL</th>
                                    <th width="10%" bgcolor="#F0F0F0" scope="col">ISBN</th>
                                    <th width="16%" bgcolor="#F0F0F0" scope="col">SIGNATURA</th>
                                    <th width="25%" bgcolor="#F0F0F0" scope="col">TITULO</th>
                                    <th width="15%" bgcolor="#F0F0F0" scope="col">AUTOR</th>
                                    <th width="14%" bgcolor="#F0F0F0" scope="col">EDITORIAL</th>
                                    <th width="3%" bgcolor="#F0F0F0" scope="col">°</th>
                                </tr>
                                <?php
                                foreach ($resultadoBusqueda as $value) {
                                    ?>
                                    <tr>
                                        <td><?php echo $value->nomTerminal; ?></td>
                                        <td><?php echo $value->ISBN; ?></td>
                                        <td><?php echo $value->signatura; ?></td>
                                        <td><?php echo $value->titulo; ?></td>
                                        <td><?php echo $value->autores; ?></td>
                                        <td><?php echo $value->editorial; ?></td>
                                        <td><input type="radio" name="item" id="selecciona" value="<?php echo $value->signatura; ?>" /></td>
                                    </tr>                        <?php } ?>
                                <tr>
                                    <td colspan="7" align="right" bgcolor="#FFFFFF"><?php if ($sesion['perfil_usuario'] != 'INVITADO') {
                                    ?><input type="submit" name="btn_solicitar" id="btn_solicitar" value="Consultar items" /><?php } ?></td>
                                </tr>
                            </table><?php echo form_close(); ?></div><?php echo $this->pagination->create_links();
                                } ?>
                </div><br />
                <div id="pieDePagina">Desarrollado por: <strong><a href="#" id="nino">Nino D. Simeón Huaccho</a></strong><a href="#"></a><br />
                    Ciudad Universitaria - Av. Mercedes Indacochea N 609<br />
                    Teléfono: 232-1338, Huacho - Perú<br />
                </div>
            </div>

            <div id="titulo_principal">
                <h2><u>VISUALIZACIÓN DE CATÁLOGO</u>:</h2>
                <div id="nuevas_adquisiciones"><strong><a href="<?php echo site_url('visualizarcatalogo/novedades'); ?>">NUEVAS ADQUISICIONES</a></strong></div>
            </div>
            <div id="menu"><img src="<?php echo base_url(); ?>public/img/unjfsc_interior.jpg" width="161" height="105" /></div>
        </div>

    </body>
</html>