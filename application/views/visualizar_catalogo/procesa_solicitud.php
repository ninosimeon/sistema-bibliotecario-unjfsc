<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="description" content="Sistema Web Bibliotecario UNJFSC" />
        <title>..::Sistema Bibliotecario::..</title>
        <link href="<?php echo base_url('public/css/estilosBusqueda.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('public/css/fresh_theme.css'); ?>" rel="stylesheet" type="text/css" />
        <link rel="icon" href="<?php echo base_url('public/img/favicon.ico'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/ui.jqgrid.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.ui.sunny.css'); ?>"/>        
        <script src="<?php echo base_url('public/lib/jquery.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.datepicker-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.sunny.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/grid.locale-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.jqGrid.min.js'); ?>" type="text/javascript" ></script>
        <script type="text/javascript">
            $().ready(function(){
                $('#btn_interno').click(function(){
                    $('#tipo_prestamo').val('INTERNO');                    
                    $('#msg_confirma').dialog('open');
                });
                $('#btn_externo').click(function(){
                    $('#tipo_prestamo').val('EXTERNO');                    
                    $('#msg_confirma').dialog('open');
                });
                $('.sol').hide();
                $('#sol_descriptivo').show();
                $('#creditos').click(function(){
                    $('#msg_ninosimeon').dialog('open');
                });                  
                $('#msg_confirma').dialog({
                    autoOpen: false,
                    show: "blind",
                    hide: "explode",
                    title: "Confirmación",                                        
                    buttons:{
                        "Si =)":function(){
                            $(this).dialog('close'); 
                            $.post('<?php echo site_url('visualizarcatalogo/solicitud_reserva'); ?>',{solicitud_reserva: true, tipo_solicitud: $('#tipo_prestamo').val(),isbn: $('#isbn').val(),terminal: $('#ubicacion').val()},function(r){                            	
                                if (r.estado == 'pendiente') {
                                    $('.sol').slideUp('slow', function() {
                                        $('#sol_fail').slideDown('slow');
                                    });
                                } else if(r.estado == 'sancion'){
                                    alert('TIENES UNA SANCION PENDIENTE ¬¬');
                                }else if(r.estado == 'INTERNO' || r.estado == 'EXTERNO'){									                           
                                    $('#sol_explicativo').slideUp('slow', function() {
                                        $('#sol_success').slideDown('slow');
                                    });
                                }else if(r.estado == 'RESERVA'){
                                    $('#res_hora').text(r.hora);
                                    $('#res_fecha').text(r.fecha);
                                    $('#sol_descriptivo').slideUp('slow', function() {
                                        $('#res_success').slideDown('slow');
                                    });
                                }
                            },'json');                            
                        },
                        "No =(":function(){
                            $(this).dialog('close');
                            $('#sol_explicativo').slideUp('slow', function() {
                                $('#sol_descriptivo').slideDown('slow');
                            });
                        }
                    }
                });
                $('#msg_ninosimeon').dialog({
                    autoOpen: false,
                    show: 'blind',
                    hide: 'explode',
                    title: 'Créditos',
                    width: 300                    
                });            
            }); 
            function regresa_buscador(){
                document.location.href= '<?php echo site_url('visualizarcatalogo'); ?>';
            }
            function inicia_solicitud(){
                $('#sol_descriptivo').slideUp('slow', function() {
                    $('#sol_explicativo').slideDown('slow');
                });
            }
            function inicia_reserva(){
                $('#tipo_prestamo').val('RESERVA');                    
                $('#msg_confirma').dialog('open');
            }
        </script>
    </head>
    <div id="msg_ninosimeon">
        <p>
            Desarrollador: <b>Nino David Simeón Huaccho</b><br /> <br /> <b>mail:</b>
            ninosimeon@gmail.com<br /> <b>url:</b> <a
                href="http://about.me/ninosimeon">about.me/ninosimeon</a><br />
        </p>
        <h4>E.A.P. Ing. Informática "Alan Turing"</h4>
    </div>
    <div id="msg_confirma">
        <h2>¿Estas seguro de proceder?</h2>
        <p>
            1 solicitud por usuario es el <b>LIMITE</b>
        </p>
    </div>
    <body>
        <div id="contenedor" class="ui-widget">
            <header>
                <img
                    src="<?php echo base_url('public/img/banner_optimizado/bannerReporte_r1_c1.jpg'); ?>"
                    width="221" height="67" alt="bannerReporte_r1_c1" /><img
                    src="<?php echo base_url('public/img/banner_optimizado/bannerReporte_r1_c2.jpg'); ?>"
                    width="297" height="67" alt="bannerReporte_r1_c2" /><img
                    src="<?php echo base_url('public/img/banner_optimizado/bannerReporte_r1_c3.jpg'); ?>"
                    width="432" height="67" alt="bannerReporte_r1_c3" />
            </header>
            <section>
                <div id="imagen">
                    <img src="<?php echo base_url(); ?>public/img/unjfsc_interior.jpg"
                         width="220" height="165" alt="UNJFSC INTERIOR" />
                </div>
                <div id="descripcion_usuario" class="derecha ui-widget-header" style="width: 685px;">
                    <b><?php echo $persona['cargo']; ?>,</b> <?php echo $persona['nombres']; ?> <nav style="margin-right: 10px;">
                        <a href="<?php echo site_url('variado/panel'); ?>">
                            Panel de usuario</a>
                        | <a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar
                            Sesión</a>
                    </nav>
                </div>
                <div id="titulo_descriptivo" class="derecha ui-corner-all ui-widget-content">
                    <h1>MÓDULO DE PRESTAMOS/RESERVAS</h1>
                </div>
                <article>
                    <div id="descripcion_libro" class="derecha ui-widget-content">
                        <input type="hidden" value="<?php echo $isbn; ?>" id="isbn" /><input type="hidden" id="tipo_prestamo" value="hola"/><input
                            type="hidden" value="<?php echo $ubicacion; ?>" id="ubicacion" /> <b>TITULO:</b> <?php echo $libro['titulo']; ?><br />
                        <b>AUTOR:</b> <?php echo $libro['autor']; ?><br /> <b>PUBLICACION:</b> <?php echo substr($libro['publicacion'], 0, 4); ?> <b>EDICIÓN:</b> <?php
echo $libro['edicion'];
if ($libro['volumen']) {
    echo ' <b>VOLUMEN:</b> ' . $libro['volumen'];
}
?> <b>UBICACIÓN:</b> <?php echo $libro['ubicacion']; ?>
                    </div>
                    <div id="resultados">
                        <div id="sol_descriptivo" class="sol">
                            <p>
                            <nav> <?php echo $boton_disponible . $boton_nodisponible; ?><button
                                    type="button" onclick="regresa_buscador()">๑</button>
                            </nav>
                            <b>CANTIDAD DISPONIBLE:</b> <span class="sol_cifra"><?php echo $cant_disponible; ?></span>
                            <br /> <b>CANTIDAD NO DISPONIBLE:</b> <span class="sol_cifra"><?php echo $cant_nodisponible; ?></span>
                            </p>
                        </div>
                        <div id="sol_explicativo" class="sol">
                            <p>Estimado lector, para proceder a la solicitud del <B>MATERIAL BIBLIOGRAFICO</B> eliga el tipo de préstamo:
                                <br />
                            <h3>INTERNO:</h3>
                            <span style="margin-left: 1em;">El tiempo límite de entrega es al
                                cierre de la biblioteca, el <b>MATERIAL</b> no sale de la
                                BIBLIOTECA.
                            </span>
                            <h3>EXTERNO:</h3>
                            <span style="margin-left: 1em;">El tiempo límite de entrega es <b>2</b>
                                días despues de haber sido adquirido, el <b>MATERIAL</b> tiene la
                                posibilidad de salir de la BIBLIOTECA.
                            </span>
                            </p>
                            <hr />
                            <nav>
                                <p>
                                    Este libro esta disponible el tipo de prestamo:
                                    <?php
                                    echo $boton_interno;
                                    echo $boton_externo;
                                    ?>								
                                </p>
                            </nav>
                        </div>
                        <div id="res_success" class="sol">
                            <h2>¡RESERVA ENVIADA EXITOSAMENTE!</h2>
                            <p style="line-height: 2em;">
                                Por favor apersonese a la <b>BIBLIOTECA</b> lo mas antes posible
                                para hacer cumplido la solicitud, recuerda que solo puedes
                                solicitar un libro a la vez.<br /> Si no consumaste la solicitud
                                dirigete al bibliotecario para que te lo cancele y así poder
                                solicitar nuevamente. <a href="<?php echo site_url('visualizarcatalogo'); ?>">¿Volver?</a>
                                <br />
                                <b>EL ITEM ESTARA DISPONIBLE EN LA SIGUIENTE FECHA: </b><span id="res_hora"></span> <span id="res_fecha"></span>
                            </p>
                        </div>
                        <div id="sol_success" class="sol">
                            <h2>¡SOLICITUD ENVIADA EXITOSAMENTE!</h2>
                            <p style="line-height: 2em;">
                                Por favor apersonese a la <b>BIBLIOTECA</b> lo mas antes posible
                                para hacer cumplido la solicitud, recuerda que solo puedes
                                solicitar un libro a la vez.<br /> Si no consumaste la solicitud
                                dirigete al bibliotecario para que te lo cancele y así poder
                                solicitar nuevamente. <a
                                    href="<?php echo site_url('visualizarcatalogo'); ?>">¿Volver?</a>
                            </p>
                        </div>
                        <div id="sol_fail" class="sol">
                            <h2>¡NO SE PUEDE PROCESAR SOLICITUD!</h2>
                            <p style="line-height: 2em;">
                                No se puede procesar más de una solicitud por usuario, culmine la
                                solicitud anterior para poder solicitar nuevamente.<br /> Tambien
                                puedes cancelar dicha solicitud, no necesariamiente concluirlo.
                                Para ello deberas apersonarte en la <B>BIBLIOTECA</B> <a
                                    href="<?php echo site_url('visualizarcatalogo'); ?>">¿Volver?</a>
                            </p>
                        </div>
                    </div>
                </article>
                <footer class="ui-state-default">
                    <div style="float: left;">
                        Ciudad Universitaria - Av. Mercedes Indacochea N° 609<br />
                        Teléfono: 232-1338, Huacho - Perú
                    </div>
                    <div style="float: right">Desarrollado por: Nino D. Simeón Huaccho</div>                    
                    <div style="clear: both;"></div>
                </footer>
            </section>
        </div>
    </body>
</html>
