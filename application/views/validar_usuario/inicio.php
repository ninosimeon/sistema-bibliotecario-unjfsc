<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <meta name="description" content="Sistema Web Bibliotecario UNJFSC"/>
        <title>..::Sistema Bibliotecario::..</title>
        <link rel="icon" href="<?php echo base_url('public/img/favicon.ico'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/ui.jqgrid.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.ui.sunny.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/fresh_theme.css'); ?>">
        <script src="<?php echo base_url('public/lib/jquery.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.datepicker-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.sunny.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/grid.locale-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.jqGrid.min.js'); ?>" type="text/javascript" ></script>
    </head>
    <script>
        $().ready(function(){
            $('#form_sesion').submit(function(){
                $.post('<?php echo site_url('validarusuario/logear'); ?>',{user: $('#cuenta_usuario').val(),pass: $('#cuenta_clave').val()}, function(data){
                    if (data == 'error'){
                        $('#estado').show('slow');
                        $('#warning').hide();
                    }else if(data == 'sancion'){
                        $('#warning').show('slow');
                        $('#estado').hide();
                    }else{
                        document.location.href= '<?php echo site_url('validarusuario/manejausuario'); ?>';
                    }
                })
            })
            $('button').button()
            $('#btn_iniciar').button({
                icons:{
                    primary: "ui-icon-key"
                }
            })
            $('#form_sesion').submit(function(){
                return false
            })
        })
        function elige_browser(){
            window.open('https://www.google.com/chrome?hl=es')
        }
    </script>
    <body>
        <div id="contenedor" class="ui-widget">
            <div id="cabecera">
                <img src="<?php echo base_url('public/img/banner/banner_r1_c1_s1.jpg'); ?>" alt="Sistema Web Bibliotecario UNJFSC" title="Sistema Web Bibliotecario UNJFSC"><img src="<?php echo base_url('public/img/banner/banner_r1_c2_s1.jpg'); ?>" alt="Sistema Web Bibliotecario UNJFSC" title="Sistema Web Bibliotecario UNJFSC"><img src="<?php echo base_url('public/img/banner/banner_r1_c3_s1.jpg'); ?>" alt="Sistema Web Bibliotecario UNJFSC" title="Sistema Web Bibliotecario UNJFSC"><img src="<?php echo base_url('public/img/banner/banner_r1_c4_s1.jpg'); ?>" alt="Sistema Web Bibliotecario UNJFSC" title="Sistema Web Bibliotecario UNJFSC"><img src="<?php echo base_url('public/img/banner/banner_r1_c5_s1.jpg'); ?>" alt="Sistema Web Bibliotecario UNJFSC" title="Sistema Web Bibliotecario UNJFSC">
            </div>
            <div id="contenido">
                <form id="form_sesion" style="width: 220px;margin-top: 50px;margin-left: 50px;float: left;">
                    <h3 class="ui-widget-header ui-corner-top">Iniciar sesión</h3>
                    <div class="ui-corner-bottom ui-widget-content">
                        <table>
                            <tbody>
                                <tr>
                                    <td>Usuario:</td>
                                    <td><input id="cuenta_usuario" type="number" required="" placeholder="46465858" style="width: 97%;"/></td>
                                </tr>
                                <tr>
                                    <td>Contraseña:</td>
                                    <td><input id="cuenta_clave" type="password" required="" style="width: 97%;"/></td>
                                </tr>
                                <tr>
                                    <td><a href="<?php echo site_url('validarusuario/logear'); ?>">Invitado</a></td>
                                    <td><button id="btn_iniciar" type="submit">Iniciar sesión</button></td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <div id="estado" style="display:none"><b>¡Datos incorrectos! =(</b></div>
                                        <div id="warning" style="display:none"><b>¡Tienes una sanción pendiente! =(</b></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div id="texto_introducción" class="ui-widget-content ui-corner-all">
                    <div style="float: left;margin-left: 15px;margin-top: 10px;"><?php echo img(array('src' => 'public/img/logon.png', 'alt' => 'Logeate :)', 'title' => 'Logeate :)', 'width' => '120', 'height' => '120')); ?></div>                    
                    <ul style="float: right;width: 420px;">
                        <li>Desde el Sistema de Información usuarios lectores podrán realizar solicitud de materiales bibliográficos, reservarlos.</li>
                        <li>Asi como también el personal administrativo podra realizar sus operaciones a traves de este sistema.</li>
                        <li>Sres ciudadanos, asi también pueden visualizar los materiales bibliográficos sin contar con Cuenta de Usuario, para ello denle clic a 'Invitado'.</li>
                        <li>Si no tiene una cuenta de usuario, solicitelo en la biblioteca descentralizada de su Facultad.</li>
                    </ul>
                </div>
                <?php echo $browser; ?>
                <div style="clear: both;height: 15px;"></div>
                <div id='footer' class="ui-state-default">
                    <div style="float: left;">
                        Ciudad Universitaria - Av. Mercedes Indacochea N° 609<br />
                        Teléfono: 232-1338, Huacho - Perú
                    </div>
                    <div style="float: right">Desarrollado por: Nino D. Simeón Huaccho</div>                    
                    <div style="clear: both;"></div>
                </div>
            </div>
        </div>
    </body>
</html>