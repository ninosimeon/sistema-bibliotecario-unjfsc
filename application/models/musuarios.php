<?php

class Musuarios extends CI_Model {

    function obtener_dni($caracteres) {
        /* $this->db->select("CONCAT(apellidos,' ',nombres) AS nombres, numero AS dni");
          $this->db->from('persona');
          $this->db->like('apellidos', $caracteres);
          $this->db->or_like('nombres', $caracteres);
          $query = $this->db->get(); */
        $query = $this->db->query("SELECT CONCAT(apellidos,' ',nombres) AS nombres, numero AS dni
                FROM persona
                WHERE apellidos LIKE '%$caracteres%' OR nombres LIKE '%$caracteres%'");
        if ($query->num_rows() >= 1) {
            return $query;
        } else {
            return false;
        }
    }

    function obtener_tipoDoc() {
        $query = $this->db->get('tipo_doc_persona');
        return $query;
    }

    function obtener_tipoPerfil() {
        $this->db->where('codigo !=', 'ADMIN');
        $query = $this->db->get('perfil_usuario');
        return $query;
    }

    function obtener_usuario($dni) {
        $query = $this->db->get_where('view_usuario', array('cuenta' => $dni));
        return $query;
    }

    function consultaDoc($doc) {
        $query = $this->db->get_where('persona', array('numero' => $doc));
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function update_usuario($dni, $carne, $sexo, $direccion, $mail, $telefono, $terminal, $clave = NULL) {
        $upd = array();
        $upd2 = array();
        $this->db->trans_start();
        $upd['carneuniv'] = $carne;
        $upd['sexo'] = $sexo;
        $upd['dirFisica'] = $direccion;
        $upd['email'] = $mail;
        $upd['telefono'] = $telefono;
        $this->db->where('numero', $dni);
        $this->db->update('persona', $upd);
        if ($clave != NULL) {
            $upd2['clave'] = $this->encrypt->encode($clave);
        }
        $upd2['codTerminal'] = $terminal;
        $this->db->where('cuenta', $dni);
        $this->db->update('usuario', $upd2);
        $this->db->trans_complete();
        return 'ok';
    }

}

?>