<?php

class Mvisualizarcatalogo extends CI_Model {

    function describe_libros($isbn, $ubicacion) {
        $query = $this->db->get_where('view_busqueda', array('ISBN' => $isbn, 'codTerminal' => $ubicacion));
        return $query;
    }

    function determina_datos($signatura) {
        $this->db->select('item_bibliografico.signatura, evento_prestamo.fechaFin, evento_prestamo.horaFin, evento_prestamo.estado');
        $this->db->from('item_bibliografico');
        $this->db->join('evento_solicitud', 'item_bibliografico.signatura = evento_solicitud.idItem');
        $this->db->join('evento_prestamo', 'evento_solicitud.nevento = evento_prestamo.nsolicitud');
        $this->db->where('evento_prestamo.estado', 'SIN DEVOLVER');
        $query = $this->db->get();
        return $query;
    }

    function determina_persona($signatura) {
        $this->db->select('item_bibliografico.signatura, evento_solicitud.tipoSolicitud, evento_solicitud.estado, evento.usuario');
        $this->db->from('evento');
        $this->db->join('evento_solicitud', 'evento.numero = evento_solicitud.nevento');
        $this->db->join('item_bibliografico', 'item_bibliografico.signatura = evento_solicitud.idItem');
        $this->db->where('item_bibliografico.signatura', $signatura);
        $this->db->where('evento_solicitud.tipoSolicitud', 'RESERVA');
        $this->db->where('evento_solicitud.estado', 'SIN CONFIRMAR');
        $query = $this->db->get();
        return $query;
    }

    function cargatematica() {
        $query = $this->db->get('tema');
        return $query;
    }

    function todoterminal() {
        $query = $this->db->get('terminal');
        return $query;
    }

    function calculadisponibles($terminal, $isbn, $ciclo) {
        $query = $this->db->get_where('view_busqueda', array('codTerminal' => $terminal, 'ISBN' => $isbn, 'codigoCiclo' => $ciclo, 'estado' => 'DISPONIBLE'));
        return $query->num_rows();
    }

    function calculaocupados($terminal, $isbn, $ciclo) {
        $query = $this->db->get_where('view_busqueda', array('codTerminal' => $terminal, 'ISBN' => $isbn, 'codigoCiclo' => $ciclo, 'estado' => 'NO DISPONIBLE'));
        return $query->num_rows();
    }

    function listarItems($terminal, $isbn, $ciclo) {
        $query = $this->db->get_where('view_busqueda', array('codTerminal' => $terminal, 'ISBN' => $isbn, 'codigoCiclo' => $ciclo));
        return $query->result();
    }

    function determinanovedades($ciclo, $terminal) {
        $this->db->order_by('fechaIngreso', 'desc');
        $this->db->limit(10);
        $this->db->where('codTerminal', $terminal);
        $this->db->where('codigoCiclo', $ciclo);
        $query = $this->db->get('view_busqueda');
        return $query;
    }

    function cargacategoria() {
        $query = $this->db->get('categoria');
        return $query;
    }

    function determinaterminal($param) {
        $terminal = '';
        $query = $this->db->get_where('view_busqueda', array('signatura' => $param));
        foreach ($query->result() as $value) {
            $terminal = $value->codTerminal;
        }
        return $terminal;
    }

    function determinaisbn($param) {
        $isbn = '';
        $query = $this->db->get_where('view_busqueda', array('signatura' => $param));
        foreach ($query->result() as $value) {
            $isbn = $value->ISBN;
        }
        return $isbn;
    }

    function busqueda_simple($texto, $limit = NULL, $offset = NULL) {
        $this->db->like('titulo', $texto);
        $this->db->or_like('autores', $texto);
        $this->db->group_by(array('ISBN', 'codTerminal'));
        $this->db->limit($limit, $offset);
        $query = $this->db->get('view_busqueda');
        if ($query->num_rows() >= 1) {
            return $query;
        } else {
            return false;
        }
    }

    function busqueda_avanzada($autor = NULL, $isbn = NULL, $titulo = NULL, $contenido = NULL, $limit = NULL, $offset = NULL) {
        if ($autor) {
            $this->db->like('autores', $autor);
        }
        if ($isbn) {
            $this->db->like('ISBN', $isbn);
        }
        if ($titulo) {
            $this->db->like('titulo', $titulo);
        }
        $this->db->group_by(array('ISBN', 'codTerminal'));
        $this->db->limit($limit, $offset);
        $query = $this->db->get('view_busqueda');
        if ($query->num_rows() >= 1) {
            return $query;
        } else {
            return false;
        }
    }

    function busqueda($ciclo, $terminal = NULL, $isbn = NULL, $categoria = NULL, $tematica = NULL, $titulo = NULL, $autor = NULL, $editorial = NULL, $limit = NULL, $offset = NULL) {
        if ($terminal != 'TODOS') {
            $this->db->where('codTerminal', $terminal);
        }
        if ($isbn) {
            $this->db->like('ISBN', $isbn);
        }
        if ($categoria) {
            $this->db->where('categoria', $categoria);
        }
        if ($tematica) {
            $this->db->like('tematica', $tematica, 'after');
        }
        if ($titulo) {
            $this->db->like('titulo', $titulo);
        }
        if ($autor) {
            $this->db->like('autores', $autor);
        }
        if ($editorial) {
            $this->db->like('editorial', $editorial);
        }
        $this->db->limit($limit, $offset);
        $this->db->where('codigoCiclo', $ciclo);
        // Verificamos que este dado de alta por el auxiliar (emitido por el
        // bibliotecario que registro el item)
        $this->db->like('estado', 'DISPONIBLE');
        $this->db->order_by('signatura', 'random');
        // $this->db->group_by('ISBN');
        $query = $this->db->get('view_busqueda');
        if ($query->num_rows() >= 1) {
            return $query;
        } else {
            return false;
        }
    }

}

?>
