<?php

class Mreporte extends CI_Model {
    /*
     * INICIO REPORTE DETALLADO MATERIAL
     */

    function relacion_solicitantes($signatura) {
        $this->db->select('*');
        $this->db->from('view_operaciones');       
        $this->db->where('signatura', $signatura);
        $query = $this->db->get();
        return $query;
    }

    function relacion_signatura($isbn, $terminal, $sidx, $sord, $start = NULL, $limit = NULL, $primero = NULL, $segundo = NULL) {
        $this->db->select('*');
        $this->db->from('view_item');
        if ($sidx) {
            $this->db->order_by($sidx, $sord);
        }
        if ($start) {
            $this->db->limit($start, $limit);
        }
        if ($segundo) {
            $this->db->where($primero, $segundo);
        }
        $this->db->where('view_item.isbn', $isbn);
        $this->db->where('view_item.cod_terminal', $terminal);
        $query = $this->db->get();
        return $query;
    }

    function muestra_terminales($isbn) {
        $this->db->select('COUNT(*) AS cantidad, view_item.nomTerminal AS terminal, view_item.cod_terminal');
        $this->db->from('view_item');
        $this->db->group_by('view_item.cod_terminal');
        $this->db->where('isbn', $isbn);
        $query = $this->db->get();
        return $query;
    }

    function detalle_signatura($signatura) {
        $this->db->select("COUNT(*) as cantidad, (SELECT CONCAT(item_bibliografico.fechaIngreso,' ',item_bibliografico.horaIngreso) FROM item_bibliografico WHERE item_bibliografico.signatura = $signatura) AS fecha'");
        $this->db->from('view_reporte_concluido');
        $this->db->where('view_reporte_concluido.idItem', $signatura);
        $query = $this->db->get();
        return $query;
    }

    /*
     * FIN REPORTE DETALLADO MATERIAL
     */

    function consulta_sindevolver($terminal) {
        $this->db->select('*');
        $this->db->from('evento_solicitud');
        $this->db->join('evento_prestamo', 'evento_prestamo.nsolicitud = evento_solicitud.nevento');
        $this->db->join('item_bibliografico', 'evento_solicitud.idItem = item_bibliografico.signatura');
        $this->db->where('item_bibliografico.codTerminal', $terminal);
        $this->db->where('evento_prestamo.estado', 'SIN DEVOLVER');
        $query = $this->db->get();
        return $query;
    }

    function consulta_materiales($terminal) {
        $this->db->select('*');
        $this->db->from('view_reporte_operacion');
        $this->db->group_by('isbn');
        $this->db->where('terminal', $terminal);
        $query = $this->db->get();
        return $query;
    }

    function paginar_operaciones($terminal, $limite = NULL, $offset = NULL) {
        $this->db->select('*');
        $this->db->from('view_reporte_operacion');
        $this->db->where('terminal', $terminal);
        $this->db->limit($limite, $offset);
        $query = $this->db->get();
        return $query;
    }

    function paginar_inconcluso_solicitud($signatura, $limite = NULL, $offset = NULL) {
        $this->db->select('*');
        $this->db->from('view_reporte_inconcluso_solicitud');
        $this->db->where('signatura', $signatura);
        $this->db->limit($limite, $offset);
        $query = $this->db->get();
        return $query;
    }

    function paginar_inconcluso_prestamo($signatura, $limite = NULL, $offset = NULL) {
        $this->db->select('*');
        $this->db->from('view_reporte_inconcluso_prestamo');
        $this->db->where('signatura', $signatura);
        $this->db->limit($limite, $offset);
        $query = $this->db->get();
        return $query;
    }

    function paginar_concluido($signatura, $limite = NULL, $offset = NULL) {
        $this->db->select('*');
        $this->db->from('view_reporte_concluido');
        $this->db->where('idItem', $signatura);
        $this->db->limit($limite, $offset);
        $query = $this->db->get();
        return $query;
    }

    function fechapublicacion($terminal) {
        $query = $this->db->query("SELECT YEAR(material_bibliografico.fecPublicacion) AS anio, Count(*) AS cantidad FROM material_bibliografico INNER JOIN item_bibliografico ON item_bibliografico.`ISBN-ISSN-codigo` = material_bibliografico.`ISBN-ISSN-codigo` WHERE item_bibliografico.codTerminal = '$terminal' GROUP BY YEAR(material_bibliografico.fecPublicacion)");
        return $query;
    }

    function fechaingreso($terminal) {
        $query = $this->db->query("SELECT YEAR(item_bibliografico.fechaIngreso) AS anio , COUNT(*) AS cantidad FROM item_bibliografico INNER JOIN material_bibliografico ON item_bibliografico.`ISBN-ISSN-codigo` = material_bibliografico.`ISBN-ISSN-codigo` WHERE item_bibliografico.codTerminal = '$terminal' GROUP BY YEAR (item_bibliografico.fechaIngreso)");
        return $query;
    }

    //QUERY PARA EL REPORTE GRAFICO MENSUAL FECHA DE INGRESO (NO TE CONFUNDAS CON EL DE ARRIBA, QUE ES DEL TOTAL)

    function fecha_ingreso($terminal, $anio) {
        $query = $this->db->query("SELECT MONTH(item_bibliografico.fechaIngreso) AS mes,COUNT(*) AS cantidad FROM item_bibliografico INNER JOIN material_bibliografico ON item_bibliografico.`ISBN-ISSN-codigo` = material_bibliografico.`ISBN-ISSN-codigo` WHERE item_bibliografico.codTerminal = '$terminal' AND YEAR(item_bibliografico.fechaIngreso) = $anio GROUP BY MONTH(item_bibliografico.fechaIngreso)");
        return $query;
    }

    /*
     * ZONA DE ACCIONES REFERENTES A JQGRID, GENERALMENTE ES UN PAR POR CADA ACTIVIDAD EN LA PRIMERA MIDE LA CANTIDAD Y LA SEGUNDA EL LÍMITE. ENJOY IT! :D
     */

    function jqcant_fechapublicacion($terminal, $anio) {
        $query = $this->db->query("SELECT COUNT(*) AS cantidad FROM material_bibliografico INNER JOIN item_bibliografico ON item_bibliografico.`ISBN-ISSN-codigo` = material_bibliografico.`ISBN-ISSN-codigo` WHERE YEAR(material_bibliografico.fecPublicacion) = $anio AND item_bibliografico.codTerminal = '$terminal'");
        return $query;
    }

    function jquery_fechapublicacion($terminal, $anio, $sidx, $sord, $start, $limit) {
        $query = $this->db->query("SELECT material_bibliografico.autores, material_bibliografico.titulo, item_bibliografico.signatura, material_bibliografico.`ISBN-ISSN-codigo` AS isbn FROM material_bibliografico INNER JOIN item_bibliografico ON item_bibliografico.`ISBN-ISSN-codigo` = material_bibliografico.`ISBN-ISSN-codigo` WHERE YEAR(material_bibliografico.fecPublicacion) = $anio AND item_bibliografico.codTerminal = '$terminal' ORDER BY $sidx $sord LIMIT $start , $limit");
        return $query;
    }

    // VERSION REHECHA REPORTES 'ITEM INGRESO'

    function items_ingreso() {
        $query = $this->db->get('view_anio_ingreso');
        $texto = 'Actualmente del año: ';
        $i = 1;
        foreach ($query->result() as $value) {
            if ($i % 3 == 0) {
                $texto .= '<br />';
            }
            if ($i > 1) {
                $texto.=', ';
            }
            $texto .= '<b>' . $value->anio . '</b> cuenta con ' . $value->cantidad . ' items';
            $i = $i + 1;
        }
        return $texto;
    }

    //GRAFICO
    function items_ingreso_terminal($anio) {
        $query = $this->db->query("SELECT COUNT(*) AS cantidad, terminal.nomTerminal, terminal.codTerminal FROM material_bibliografico INNER JOIN item_bibliografico ON item_bibliografico.`ISBN-ISSN-codigo` = material_bibliografico.`ISBN-ISSN-codigo` INNER JOIN terminal ON terminal.codTerminal = item_bibliografico.codTerminal WHERE YEAR(item_bibliografico.fechaIngreso) = $anio GROUP BY terminal.nomTerminal");
        return $query;
    }

    function semi_detalle() {
        $query = $this->db->query("SELECT YEAR(item_bibliografico.fechaIngreso) AS anio, terminal.nomTerminal, item_bibliografico.codTerminal AS terminal FROM item_bibliografico INNER JOIN material_bibliografico ON item_bibliografico.`ISBN-ISSN-codigo` = material_bibliografico.`ISBN-ISSN-codigo` INNER JOIN terminal ON terminal.codTerminal = item_bibliografico.codTerminal GROUP BY item_bibliografico.codTerminal");
        return $query;
    }

    function ingreso_superdetallado($anio, $terminal) {
        $query = $this->db->query("SELECT material_bibliografico.autores, material_bibliografico.titulo, item_bibliografico.signatura, item_bibliografico.`ISBN-ISSN-codigo` AS isbn FROM item_bibliografico INNER JOIN material_bibliografico ON item_bibliografico.`ISBN-ISSN-codigo` = material_bibliografico.`ISBN-ISSN-codigo` INNER JOIN terminal ON terminal.codTerminal = item_bibliografico.codTerminal WHERE item_bibliografico.codTerminal = '$terminal' AND YEAR(item_bibliografico.fechaIngreso) = $anio");
        return $query;
    }

    function jqingreso_superdetallado($anio, $terminal, $sidx, $sord, $start, $limit) {
        $query = $this->db->query("SELECT material_bibliografico.autores, item_bibliografico.fechaIngreso AS ingreso, material_bibliografico.titulo, item_bibliografico.signatura, item_bibliografico.`ISBN-ISSN-codigo` AS isbn FROM item_bibliografico INNER JOIN material_bibliografico ON item_bibliografico.`ISBN-ISSN-codigo` = material_bibliografico.`ISBN-ISSN-codigo` INNER JOIN terminal ON terminal.codTerminal = item_bibliografico.codTerminal WHERE item_bibliografico.codTerminal = '$terminal' AND YEAR(item_bibliografico.fechaIngreso) = $anio ORDER BY $sidx $sord LIMIT $start , $limit");
        return $query;
    }

    // VERSION REHECHA REPORTES 'ITEM PUBLICACION'

    /* GRAFICO */

    function items_publicacion_terminal($terminal) {
        $query = $this->db->query("SELECT COUNT(*) AS cantidad, YEAR(material_bibliografico.fecPublicacion) AS anio FROM material_bibliografico INNER JOIN item_bibliografico ON item_bibliografico.`ISBN-ISSN-codigo` = material_bibliografico.`ISBN-ISSN-codigo` INNER JOIN terminal ON terminal.codTerminal = item_bibliografico.codTerminal WHERE item_bibliografico.codTerminal = '$terminal' GROUP BY YEAR(material_bibliografico.fecPublicacion)");
        return $query;
    }

    function jqpublicacion_superdetallado($terminal, $sidx, $sord, $start, $limit) {
        $query = $this->db->query("SELECT material_bibliografico.autores, material_bibliografico.titulo, item_bibliografico.signatura, item_bibliografico.`ISBN-ISSN-codigo` AS isbn, YEAR(material_bibliografico.fecPublicacion) AS anio FROM material_bibliografico INNER JOIN item_bibliografico ON item_bibliografico.`ISBN-ISSN-codigo` = material_bibliografico.`ISBN-ISSN-codigo` INNER JOIN terminal ON terminal.codTerminal = item_bibliografico.codTerminal WHERE item_bibliografico.codTerminal = '$terminal' ORDER BY $sidx $sord LIMIT $start , $limit");
        return $query;
    }

    /* REPORTEADO EN PDF */

    function pdf_semidetallado() {
        $query = $this->db->query("select `terminal`.`nomTerminal` AS `terminal`,year(`material_bibliografico`.`fecPublicacion`) AS `anio`,count(0) AS `cantidad` from ((`material_bibliografico` join `item_bibliografico` on((`item_bibliografico`.`ISBN-ISSN-codigo` = `material_bibliografico`.`ISBN-ISSN-codigo`))) join `terminal` on((`terminal`.`codTerminal` = `item_bibliografico`.`codTerminal`))) group by year(`material_bibliografico`.`fecPublicacion`),`terminal`.`nomTerminal` order by `terminal`.`nomTerminal`,year(`material_bibliografico`.`fecPublicacion`)");
        return $query;
    }

    function pdf_detallado($terminal) {
        $query = $this->db->query("SELECT material_bibliografico.autores, material_bibliografico.titulo, item_bibliografico.signatura, item_bibliografico.`ISBN-ISSN-codigo` AS isbn, YEAR(material_bibliografico.fecPublicacion) AS anio FROM material_bibliografico INNER JOIN item_bibliografico ON item_bibliografico.`ISBN-ISSN-codigo` = material_bibliografico.`ISBN-ISSN-codigo` INNER JOIN terminal ON terminal.codTerminal = item_bibliografico.codTerminal WHERE item_bibliografico.codTerminal = '$terminal' ORDER BY YEAR(material_bibliografico.fecPublicacion)");
        return $query;
    }

    // VERSION REHECHA REPORTES 'OPERACIONES CONCLUIDAS'

    /* Tablas JQGRID */

    function cantidad_operaciones($anio) {
        $query = $this->db->query("SELECT COUNT(*) AS cantidad
            FROM evento
            INNER JOIN evento_solicitud ON evento.numero = evento_solicitud.nevento
            INNER JOIN evento_prestamo ON evento_prestamo.nsolicitud = evento_solicitud.nevento
            INNER JOIN evento_devolucion ON evento_devolucion.idPrestamo = evento_prestamo.nevento
            INNER JOIN usuario ON evento.usuario = usuario.cuenta
            INNER JOIN persona ON usuario.cuenta = persona.numero
            INNER JOIN terminal ON terminal.codTerminal = usuario.codTerminal
            WHERE YEAR(evento.fecha) = $anio");
        return $query;
    }

    function detallado_operaciones_primero($fecha_1, $fecha_2, $terminal, $libro, $persona, $sidx = NULL, $sord = NULL, $start = NULL, $limit = NULL, $primero = NULL, $segundo = NULL) {
        $this->db->select('*');
        $this->db->from('view_operaciones');
        if ($sidx) {
            $this->db->order_by($sidx, $sord);
        }
        if ($start) {
            $this->db->limit($start, $limit);
        }
        if ($segundo) {
            $this->db->where($primero, $segundo);
        }
        if ($terminal) {
            $this->db->where('FACULTAD', $terminal);
        }
        if ($libro) {
            $this->db->where('isbn', $libro);
        }
        if ($persona) {
            $this->db->where('DNI', $persona);
        }
        if ($fecha_2) {
            $this->db->where('fecha >=', $fecha_1)->where('fecha <=', $fecha_2);
        }
        else if($fecha_1){
            $this->db->where('fecha', $fecha_1);
        }        
        $query = $this->db->get();

        /* $query = $this->db->query("SELECT persona.numero AS DNI, CONCAT(persona.apellidos,' ',persona.nombres) AS NOMBRES, evento.fecha, evento.hora, terminal.nomTerminal AS FACULTAD, evento.numero 
          FROM evento
          INNER JOIN evento_solicitud ON evento.numero = evento_solicitud.nevento
          INNER JOIN evento_prestamo ON evento_prestamo.nsolicitud = evento_solicitud.nevento
          INNER JOIN evento_devolucion ON evento_devolucion.idPrestamo = evento_prestamo.nevento
          INNER JOIN usuario ON evento.usuario = usuario.cuenta
          INNER JOIN persona ON usuario.cuenta = persona.numero
          INNER JOIN terminal ON terminal.codTerminal = usuario.codTerminal
          WHERE YEAR(evento.fecha) = $anio
          ORDER BY $sidx $sord
          LIMIT $start, $limit"); */
        return $query;
    }

    function detallado_operaciones_externo($evento) {
        $query = $this->db->query("SELECT material_bibliografico.autores AS autor, material_bibliografico.titulo, item_bibliografico.modPrestamo AS prestamo, terminal.nomTerminal AS terminal, item_bibliografico.signatura, item_bibliografico.`ISBN-ISSN-codigo` AS isbn
            FROM evento
            INNER JOIN evento_solicitud ON evento_solicitud.nevento = evento.numero
            INNER JOIN item_bibliografico ON evento_solicitud.idItem = item_bibliografico.signatura
            INNER JOIN material_bibliografico ON item_bibliografico.`ISBN-ISSN-codigo` = material_bibliografico.`ISBN-ISSN-codigo`
            INNER JOIN terminal ON terminal.codTerminal = item_bibliografico.codTerminal
            WHERE evento.numero = $evento");
        return $query;
    }

    /*function detallado_operaciones_externo($evento, $sidx, $sord, $start, $limit) {
        $query = $this->db->query("SELECT material_bibliografico.autores AS autor, material_bibliografico.titulo, item_bibliografico.modPrestamo AS prestamo, terminal.nomTerminal AS terminal, item_bibliografico.signatura, item_bibliografico.`ISBN-ISSN-codigo` AS isbn
            FROM evento
            INNER JOIN evento_solicitud ON evento_solicitud.nevento = evento.numero
            INNER JOIN item_bibliografico ON evento_solicitud.idItem = item_bibliografico.signatura
            INNER JOIN material_bibliografico ON item_bibliografico.`ISBN-ISSN-codigo` = material_bibliografico.`ISBN-ISSN-codigo`
            INNER JOIN terminal ON terminal.codTerminal = item_bibliografico.codTerminal
            WHERE evento.numero = $evento
            ORDER BY $sidx $sord
            LIMIT $start, $limit");
        return $query;
    }*/

    function operaciones_subgrid_prestamo($nevento) {
        $query = $this->db->query("SELECT persona.numero AS dni, CONCAT(persona.apellidos,' ',persona.nombres) AS nombres, evento.fecha, evento.hora, evento.numero 
            FROM evento_solicitud
            INNER JOIN evento_prestamo ON evento_prestamo.nsolicitud = evento_solicitud.nevento
            INNER JOIN evento_devolucion ON evento_devolucion.idPrestamo = evento_prestamo.nevento
            INNER JOIN evento ON evento.numero = evento_prestamo.nevento
            INNER JOIN usuario ON evento.usuario = usuario.cuenta
            INNER JOIN persona ON usuario.cuenta = persona.numero
            WHERE evento_solicitud.nevento = $nevento");
        return $query;
    }

    function operaciones_subgrid_devolucion($nevento) {
        $query = $this->db->query("SELECT persona.numero AS dni, CONCAT(persona.apellidos,' ',persona.nombres) AS nombres, evento.fecha, evento.hora, evento.numero
            FROM evento_solicitud
            INNER JOIN evento_prestamo ON evento_prestamo.nsolicitud = evento_solicitud.nevento
            INNER JOIN evento_devolucion ON evento_devolucion.idPrestamo = evento_prestamo.nevento
            INNER JOIN evento ON evento_devolucion.nevento = evento.numero
            INNER JOIN usuario ON evento.usuario = usuario.cuenta
            INNER JOIN persona ON usuario.cuenta = persona.numero
            WHERE evento_solicitud.nevento = $nevento");
        return $query;
    }

    function grafico_operaciones($anio) {
        $query = $this->db->query("SELECT COUNT(*) AS cantidad, terminal.nomTerminal AS terminal
            FROM evento
            INNER JOIN evento_solicitud ON evento.numero = evento_solicitud.nevento
            INNER JOIN evento_prestamo ON evento_prestamo.nsolicitud = evento_solicitud.nevento
            INNER JOIN evento_devolucion ON evento_devolucion.idPrestamo = evento_prestamo.nevento
            INNER JOIN usuario ON evento.usuario = usuario.cuenta
            INNER JOIN persona ON usuario.cuenta = persona.numero
            INNER JOIN terminal ON terminal.codTerminal = usuario.codTerminal
            WHERE YEAR(evento.fecha) = $anio
            GROUP BY terminal.nomTerminal
            ORDER BY evento.fecha");
        return $query;
    }

    function listado_anio() {
        $query = $this->db->query("SELECT YEAR(evento.fecha) AS anio
            FROM evento
            INNER JOIN evento_solicitud ON evento.numero = evento_solicitud.nevento
            INNER JOIN evento_prestamo ON evento_prestamo.nsolicitud = evento_solicitud.nevento
            INNER JOIN evento_devolucion ON evento_devolucion.idPrestamo = evento_prestamo.nevento
            INNER JOIN usuario ON evento.usuario = usuario.cuenta
            INNER JOIN persona ON usuario.cuenta = persona.numero
            INNER JOIN terminal ON terminal.codTerminal = usuario.codTerminal            
            GROUP BY YEAR(evento.fecha)
            ORDER BY evento.fecha");

        return $query;
    }

    // VERSION REHECHA SANCIONADOS 

    /* Tablas JQGRID */

    function grafica_sancionados() {
        $query = $this->db->query("SELECT COUNT(*) AS cantidad, YEAR(evento.fecha) AS anio
            FROM evento_solicitud
            INNER JOIN evento_prestamo ON evento_prestamo.nsolicitud = evento_solicitud.nevento
            INNER JOIN evento_devolucion ON evento_devolucion.idPrestamo = evento_prestamo.nevento
            INNER JOIN evento_sancion ON evento_sancion.nevento = evento_devolucion.nsancion
            INNER JOIN evento ON evento_solicitud.nevento = evento.numero
            INNER JOIN usuario ON evento.usuario = usuario.cuenta
            INNER JOIN persona ON usuario.cuenta = persona.numero
            INNER JOIN especificacion_castigo ON evento_sancion.tipoCastigo = especificacion_castigo.codigo
            INNER JOIN terminal ON usuario.codTerminal = terminal.codTerminal
            GROUP BY YEAR(evento.fecha)");
        return $query;
    }

    function detallado_sancionados($sidx, $sord, $start, $limit) {
        $query = $this->db->query("SELECT persona.numero AS dni, CONCAT(persona.apellidos,' ', persona.nombres) AS nombres, (SELECT evento.fecha FROM evento WHERE evento_sancion.nevento = evento.numero) AS fechaInicio, (SELECT evento.hora FROM evento WHERE evento_sancion.nevento = evento.numero) AS horaInicio, evento_sancion.fecFinCastigo AS fechaFin, evento_sancion.horFinCastigo AS horaFin, terminal.nomTerminal AS terminal, especificacion_castigo.descripcion, evento.numero AS nevento 
            FROM evento_solicitud
            INNER JOIN evento_prestamo ON evento_prestamo.nsolicitud = evento_solicitud.nevento
            INNER JOIN evento_devolucion ON evento_devolucion.idPrestamo = evento_prestamo.nevento
            INNER JOIN evento_sancion ON evento_sancion.nevento = evento_devolucion.nsancion
            INNER JOIN evento ON evento_solicitud.nevento = evento.numero
            INNER JOIN usuario ON evento.usuario = usuario.cuenta
            INNER JOIN persona ON usuario.cuenta = persona.numero
            INNER JOIN especificacion_castigo ON evento_sancion.tipoCastigo = especificacion_castigo.codigo
            INNER JOIN terminal ON usuario.codTerminal = terminal.codTerminal            
            ORDER BY $sidx $sord
            LIMIT $start, $limit");
        return $query;
    }

    function detallado_sancionados_view($sidx, $sord, $start, $limit, $primero = NULL, $segundo = NULL) {
        $this->db->select('*');
        $this->db->from('view_sancion_final');
        if ($sidx) {
            $this->db->order_by($sidx, $sord);
        }
        if ($start) {
            $this->db->limit($start, $limit);
        }
        if ($segundo) {
            $this->db->where($primero, $segundo);
        }
        $query = $this->db->get();
        return $query;
    }

}

/* Fin del archivo mreporte.php */
