# PROYECTO: SISTEMA DE BIBLIOTECA #

**AUTOR:** Simeón Huaccho, Nino David

**DESCRIPCIÓN:** 
Este Sistema se realizo en base a la tesis formulada por mi persona, dejo en libertad el código para su mejora y uso con fines ACADÉMICOS. Queda totalmente prohibido dar uso COMERCIAL a este sistema sin previa autorización por parte de el autor de este proyecto.

**PROMOCIÓN "ALAN TURING" 2010 - II
INGENIERO INFORMÁTICO. NINO DAVID SIMEÓN HUACCHO.**

**DESCRIPCIÓN TÉCNICA:**
* PHP Codeigniter
* Apache Web Server
* MySQL